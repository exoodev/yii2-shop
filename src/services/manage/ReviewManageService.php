<?php

namespace exoo\shop\services\manage;

use exoo\shop\models\backend\forms\Product\ReviewEditForm;
use exoo\shop\repositories\ProductRepository;

class ReviewManageService
{
    private $products;

    public function __construct(ProductRepository $products)
    {
        $this->products = $products;
    }

    public function edit($id, $reviewId, ReviewEditForm $form): void
    {
        $product = $this->products->get($id);
        $product->editReview(
            $reviewId,
            $form->vote,
            $form->text
        );
        $this->products->save($product);
    }

    public function activate($id, $reviewId): void
    {
        $product = $this->products->get($id);
        $product->activateReview($reviewId);
        $this->products->save($product);
    }

    public function moderation($id, $reviewId): void
    {
        $product = $this->products->get($id);
        $product->moderationReview($reviewId);
        $this->products->save($product);
    }

    public function remove($id, $reviewId): void
    {
        $product = $this->products->get($id);
        $product->removeReview($reviewId);
        $this->products->save($product);
    }
}
