<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\PaymentMethod;
use exoo\shop\models\backend\forms\PaymentMethodForm;
use exoo\shop\repositories\PaymentMethodRepository;

class PaymentMethodManageService
{
    private $methods;

    public function __construct(PaymentMethodRepository $methods)
    {
        $this->methods = $methods;
    }

    public function create(PaymentMethodForm $form): PaymentMethod
    {
        $method = PaymentMethod::create(
            $form->name,
            $form->icon,
            $form->shortDescription,
            $form->description,
            $form->transactionLimit,
            $form->status
        );
        $this->methods->save($method);
        return $method;
    }

    public function edit($id, PaymentMethodForm $form): void
    {
        $method = $this->methods->get($id);
        $method->edit(
            $form->name,
            $form->icon,
            $form->shortDescription,
            $form->description,
            $form->transactionLimit,
            $form->status
        );
        $this->methods->save($method);
    }

    public function remove($id): void
    {
        $method = $this->methods->get($id);
        $this->methods->remove($method);
    }
}
