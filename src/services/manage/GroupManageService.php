<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Meta;
use exoo\shop\entities\Group\Group;
use exoo\shop\models\backend\forms\GroupForm;
use exoo\shop\repositories\GroupRepository;
use exoo\shop\repositories\ProductRepository;

class GroupManageService
{
    private $categories;
    private $products;

    public function __construct(GroupRepository $categories, ProductRepository $products)
    {
        $this->categories = $categories;
        $this->products = $products;
    }

    public function create(GroupForm $form): Group
    {
        $parent = $this->categories->get($form->parentId);
        $group = Group::create(
            $form->name,
            $form->slug,
            $form->title,
            $form->description,
            $form->characteristicIds,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        $group->appendTo($parent);
        $this->categories->save($group);
        return $group;
    }

    public function edit($id, GroupForm $form): void
    {
        $group = $this->categories->get($id);
        $this->assertIsNotRoot($group);
        $group->edit(
            $form->name,
            $form->slug,
            $form->title,
            $form->description,
            $form->characteristicIds,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );
        if ($form->parentId !== $group->parent->id) {
            $parent = $this->categories->get($form->parentId);
            $group->appendTo($parent);
        }
        $this->categories->save($group);
    }

    public function remove($id): void
    {
        $group = $this->categories->get($id);
        $this->assertIsNotRoot($group);
        $this->categories->remove($group);
    }

    private function assertIsNotRoot(Group $group): void
    {
        if ($group->isRoot()) {
            throw new \DomainException('Unable to manage the root group.');
        }
    }
}
