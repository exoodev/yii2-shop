<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Unit;
use exoo\shop\models\backend\forms\UnitForm;
use exoo\shop\repositories\UnitRepository;

class UnitManageService
{
    private $units;

    public function __construct(UnitRepository $units)
    {
        $this->units = $units;
    }

    public function create(UnitForm $form): Unit
    {
        $unit = Unit::create(
            $form->name,
            $form->short_name
        );
        $this->units->save($unit);
        return $unit;
    }

    public function edit($id, UnitForm $form): void
    {
        $unit = $this->units->get($id);
        $unit->edit(
            $form->name,
            $form->short_name
        );
        $this->units->save($unit);
    }

    public function remove($id): void
    {
        $unit = $this->units->get($id);
        $this->units->remove($unit);
    }
}
