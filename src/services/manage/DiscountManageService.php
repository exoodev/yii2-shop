<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Discount;
use exoo\shop\models\backend\forms\DiscountForm;
use exoo\shop\repositories\DiscountRepository;

class DiscountManageService
{
    private $discounts;

    public function __construct(DiscountRepository $discounts)
    {
        $this->discounts = $discounts;
    }

    public function create(DiscountForm $form): Discount
    {
        $discount = Discount::create(
            $form->percent,
            $form->name,
            $form->fromDate,
            $form->toDate,
            $form->description,
            $form->status
        );
        $this->discounts->save($discount);
        return $discount;
    }

    public function edit($id, DiscountForm $form): void
    {
        $discount = $this->discounts->get($id);
        $discount->edit(
            $form->percent,
            $form->name,
            $form->fromDate,
            $form->toDate,
            $form->description,
            $form->status
        );
        $this->discounts->save($discount);
    }

    public function remove($id): void
    {
        $discount = $this->discounts->get($id);
        $this->discounts->remove($discount);
    }
}
