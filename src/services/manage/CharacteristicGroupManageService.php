<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Characteristic\CharacteristicGroup;
use exoo\shop\models\backend\forms\CharacteristicGroupForm;
use exoo\shop\repositories\CharacteristicGroupRepository;

class CharacteristicGroupManageService
{
    private $characteristicGroups;

    public function __construct(CharacteristicGroupRepository $characteristicGroups)
    {
        $this->characteristicGroups = $characteristicGroups;
    }

    public function create(CharacteristicGroupForm $form): CharacteristicGroup
    {
        $characteristicGroup = CharacteristicGroup::create($form->name);
        $this->characteristicGroups->save($characteristicGroup);
        return $characteristicGroup;
    }

    public function edit($id, CharacteristicGroupForm $form): void
    {
        $characteristicGroup = $this->characteristicGroups->get($id);
        $characteristicGroup->edit($form->name);
        $this->characteristicGroups->save($characteristicGroup);
    }

    public function remove($id): void
    {
        $characteristicGroup = $this->characteristicGroups->get($id);
        $this->characteristicGroups->remove($characteristicGroup);
    }
}
