<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Warehouse;
use exoo\shop\models\backend\forms\WarehouseForm;
use exoo\shop\repositories\WarehouseRepository;
use exoo\shop\repositories\ProductRepository;

class WarehouseManageService
{
    private $warehouses;
    private $products;

    public function __construct(WarehouseRepository $warehouses, ProductRepository $products)
    {
        $this->warehouses = $warehouses;
        $this->products = $products;
    }

    public function create(WarehouseForm $form): Warehouse
    {
        $warehouse = Warehouse::create(
            $form->name,
            $form->description
        );
        $this->warehouses->save($warehouse);
        return $warehouse;
    }

    public function edit($id, WarehouseForm $form): void
    {
        $warehouse = $this->warehouses->get($id);
        $warehouse->edit(
            $form->name,
            $form->description
        );
        $this->warehouses->save($warehouse);
    }

    public function remove($id): void
    {
        $warehouse = $this->warehouses->get($id);
        if ($this->products->existsByWarehouse($warehouse->id)) {
            throw new \DomainException('Unable to remove warehouse with products.');
        }
        $this->warehouses->remove($warehouse);
    }
}
