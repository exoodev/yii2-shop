<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\models\backend\forms\CharacteristicForm;
use exoo\shop\repositories\CharacteristicRepository;

class CharacteristicManageService
{
    private $characteristics;

    public function __construct(CharacteristicRepository $characteristics)
    {
        $this->characteristics = $characteristics;
    }

    public function create(CharacteristicForm $form): Characteristic
    {
        $characteristic = Characteristic::create(
            $form->name,
            $form->slug,
            $form->description,
            $form->group_id,
            $form->type,
            $form->required,
            $form->default,
            $form->variants
        );
        $this->characteristics->save($characteristic);
        return $characteristic;
    }

    public function edit($id, CharacteristicForm $form): void
    {
        $characteristic = $this->characteristics->get($id);
        $characteristic->edit(
            $form->name,
            $form->slug,
            $form->description,
            $form->group_id,
            $form->type,
            $form->required,
            $form->default,
            $form->variants
        );
        $this->characteristics->save($characteristic);
    }

    public function remove($id): void
    {
        $characteristic = $this->characteristics->get($id);
        $this->characteristics->remove($characteristic);
    }
}
