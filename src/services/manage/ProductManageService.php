<?php

namespace exoo\shop\services\manage;

use exoo\shop\entities\Meta;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Tag;
use exoo\shop\models\backend\forms\Product\QuantityForm;
use exoo\shop\models\backend\forms\Product\CategoriesForm;
use exoo\shop\models\backend\forms\Product\PhotosForm;
use exoo\shop\models\backend\forms\Product\PriceForm;
use exoo\shop\models\backend\forms\Product\ProductCreateForm;
use exoo\shop\models\backend\forms\Product\ModificationEditForm;
use exoo\shop\models\backend\forms\Product\ModificationCreateForm;
use exoo\shop\models\backend\forms\Product\ProductEditForm;
use exoo\shop\repositories\BrandRepository;
use exoo\shop\repositories\CategoryRepository;
use exoo\shop\repositories\WarehouseRepository;
use exoo\shop\repositories\ProductRepository;
use exoo\shop\repositories\TagRepository;
use exoo\shop\repositories\UnitRepository;
use exoo\shop\services\TransactionManager;

class ProductManageService
{
    private $products;
    private $warehouses;
    private $brands;
    private $units;
    private $categories;
    private $tags;
    private $transaction;

    public function __construct(
        ProductRepository $products,
        WarehouseRepository $warehouses,
        BrandRepository $brands,
        UnitRepository $units,
        CategoryRepository $categories,
        TagRepository $tags,
        TransactionManager $transaction
    )
    {
        $this->products = $products;
        $this->brands = $brands;
        $this->warehouses = $warehouses;
        $this->units = $units;
        $this->categories = $categories;
        $this->tags = $tags;
        $this->transaction = $transaction;
    }

    public function create(ProductCreateForm $form): Product
    {
        $product = Product::create($form->name);
        
        $this->transaction->wrap(function() use ($product, $form) {
            $this->products->save($product);
        });

        return $product;
    }

    public function edit($id, ProductEditForm $form): void
    {
        $product = $this->products->get($id);
        $brand = $this->brands->get($form->brandId);
        $category = $this->categories->get($form->categories->main);
        $warehouse = $this->warehouses->get($form->warehouseId);
        $unit = $this->units->get($form->unitId);

        $product->edit(
            $brand->id,
            $warehouse->id,
            $unit->id,
            $form->sku,
            $form->name,
            $form->slug,
            $form->shortDescription,
            $form->description,
            $form->modificationName,
            $form->weight,
            $form->length,
            $form->width,
            $form->height,
            $form->quantity->quantity,
            $form->adminNote,
            $form->status,
            new Meta(
                $form->meta->title,
                $form->meta->description,
                $form->meta->keywords
            )
        );

        $product->changeMainCategory($category->id);
        $product->setPrice($form->price->new, $form->price->old);

        $this->transaction->wrap(function () use ($product, $form) {
            $product->revokeCategories();
            $product->revokeTags();
            $product->revokeValues();
            $this->products->save($product);

            foreach ($form->categories->others as $otherId) {
                $category = $this->categories->get($otherId);
                $product->assignCategory($category->id);
            }

            foreach ($form->values as $value) {
                $product->setValue($value->id, $value->value);
            }

            foreach ($form->tags->existing as $tagId) {
                $tag = $this->tags->get($tagId);
                $product->assignTag($tag->id);
            }

            foreach ($form->tags->newNames as $tagName) {
                if (!$tag = $this->tags->findByName($tagName)) {
                    $tag = Tag::create($tagName);
                    $this->tags->save($tag);
                }
                $product->assignTag($tag->id);
            }
            $this->products->save($product);
        });
    }

    public function removeValue($id, $characteristic_id): void
    {
        $product = $this->products->get($id);
        $product->getValue($characteristic_id)->delete();
    }

    public function changePrice($id, PriceForm $form): void
    {
        $product = $this->products->get($id);
        $product->setPrice($form->new, $form->old);
        $this->products->save($product);
    }

    public function changeQuantity($id, QuantityForm $form): void
    {
        $product = $this->products->get($id);
        $product->changeQuantity($form->quantity);
        $this->products->save($product);
    }

    public function addRelatedProduct($id, $otherId): void
    {
        $product = $this->products->get($id);
        $other = $this->products->get($otherId);
        $product->assignRelatedProduct($other->id);
        $this->products->save($product);
    }

    public function removeRelatedProduct($id, $otherId): void
    {
        $product = $this->products->get($id);
        $other = $this->products->get($otherId);
        $product->revokeRelatedProduct($other->id);
        $this->products->save($product);
    }

    public function addModification($id, ModificationCreateForm $form): void
    {
        $product = $this->products->get($id);
        $product->addModification(
            $form->sku,
            $form->name,
            $form->price->new,
            $form->price->old,
            $form->weight,
            $form->length,
            $form->width,
            $form->height,
            $form->color,
            $form->quantity
        );
        $this->products->save($product);
    }

    public function editModification($id, $modificationId, ModificationEditForm $form): void
    {
        $product = $this->products->get($id);
        $product->editModification(
            $modificationId,
            $form->sku,
            $form->name,
            $form->price->new,
            $form->price->old,
            $form->weight,
            $form->length,
            $form->width,
            $form->height,
            $form->color,
            $form->quantity
        );
        $this->products->save($product);
    }

    public function removeModification($id, $modificationId): void
    {
        $product = $this->products->get($id);
        $product->removeModification($modificationId);
        $this->products->save($product);
    }

    public function remove($id): void
    {
        $product = $this->products->get($id);
        $this->products->remove($product);
    }
}
