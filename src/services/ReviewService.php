<?php

namespace exoo\shop\services;

use exoo\shop\repositories\ReviewRepository;
use exoo\shop\repositories\ProductRepository;
use exoo\shop\repositories\UserRepository;
use exoo\shop\entities\User;
use exoo\shop\models\frontend\forms\ReviewForm;

class ReviewService
{
    private $users;
    private $products;
    private $reviews;

    public function __construct(UserRepository $users, ProductRepository $products, ReviewRepository $reviews)
    {
        $this->users = $users;
        $this->products = $products;
        $this->reviews = $reviews;
    }

    public function add($userId, $productId, ReviewForm $form): void
    {
        $product = $this->products->get($productId);
        $product->addReview(
            $userId,
            $form->vote,
            $form->plus,
            $form->minus,
            $form->comment
        );
        $this->products->save($product);
    }

    public function edit($userId, $productId, $reviewId, ReviewForm $form): void
    {
        // $product = $this->products->get($productId);
        // $product->editReview(
        //     $reviewId,
        //     $form->vote,
        //     $form->text
        // );
        // $this->products->save($product);
    }

    public function remove($reviewId, $userId = null): void
    {
        if ($userId) {
            $user = $this->users->get($userId);
            $review = $this->reviews->getBy($reviewId, $user->id);
        } else {
            $review = $this->reviews->get($reviewId);
        }
        $product = $review->product;
        $review->product->removeReview($reviewId);
        $this->products->save($product);
    }

    public function status($reviewId)
    {
        $review = $this->reviews->get($reviewId);
        $product = $review->product;
        if ($review->isActive()) {
            $product->inactivateReview($review->id);
        } else {
            $product->activateReview($review->id);
        }
        $this->reviews->save($review);
        $this->products->save($product);
    }
}
