<?php

namespace exoo\shop\services;

use Yii;
use exoo\shop\components\cart\Cart;
use exoo\shop\components\cart\CartItem;
use exoo\shop\entities\Order\CustomerData;
use exoo\shop\entities\Order\DeliveryData;
use exoo\shop\entities\Order\Order;
use exoo\shop\entities\Order\OrderItem;
use exoo\shop\models\frontend\forms\Order\OrderForm;
use exoo\shop\repositories\DeliveryMethodRepository;
use exoo\shop\repositories\OrderRepository;
use exoo\shop\repositories\ProductRepository;
use exoo\shop\services\TransactionManager;

class OrderService
{
    private $cart;
    private $orders;
    private $products;
    private $deliveryMethods;
    private $transaction;

    public function __construct(
        Cart $cart,
        OrderRepository $orders,
        ProductRepository $products,
        DeliveryMethodRepository $deliveryMethods,
        TransactionManager $transaction
    )
    {
        $this->cart = $cart;
        $this->orders = $orders;
        $this->products = $products;
        $this->deliveryMethods = $deliveryMethods;
        $this->transaction = $transaction;
    }

    public function pay($orderId, $method): void
    {
        $order = $this->orders->get($orderId);
        $order->pay($method);
        $this->orders->save($order);
    }

    public function failPay($orderId, $method): void
    {
        $order = $this->orders->get($orderId);
        $order->failPay($method);
        $this->orders->save($order);
    }

    public function checkout($userId, OrderForm $form): Order
    {
        $user = Yii::$app->user->identity->findIdentity($userId);

        $products = [];

        $items = array_map(function (CartItem $item) use (&$products) {
            $product = $item->getProduct();
            $product->checkout($item->getModificationId(), $item->getQuantity());
            $products[] = $product;
            return OrderItem::create(
                $product,
                $item->getModificationId(),
                $item->getPrice(),
                $item->getQuantity()
            );
        }, $this->cart->getItems());

        $order = Order::create(
            $user->id,
            new CustomerData(
                $form->customer->phone,
                $form->customer->name
            ),
            $items,
            $this->cart->getCost()->getTotal(),
            $form->note
        );

        $order->setDeliveryInfo(
            $this->deliveryMethods->get($form->delivery->method),
            new DeliveryData(
                $form->delivery->index,
                $form->delivery->address
            )
        );

        $this->transaction->wrap(function () use ($order, $products) {
            $this->orders->save($order);
            foreach ($products as $product) {
                $this->products->save($product);
            }
            $this->cart->clear();
        });

        return $order;
    }
}
