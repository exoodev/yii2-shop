<?php

namespace exoo\shop\services;

use exoo\shop\repositories\ProductRepository;
use exoo\shop\repositories\UserRepository;
use exoo\shop\entities\User;

class WishlistService
{
    private $users;
    private $products;

    public function __construct(UserRepository $users, ProductRepository $products)
    {
        $this->users = $users;
        $this->products = $products;
    }

    public function add($userId, $productId): void
    {
        $user = $this->users->get($userId);
        $product = $this->products->get($productId);
        $user->addToWishList($product->id);
        $this->users->save($user);
    }

    public function remove($userId, $productId): void
    {
        $user = $this->users->get($userId);
        $product = $this->products->get($productId);
        $user->removeFromWishList($product->id);
        $this->users->save($user);
    }

    public function countWishlistByUser($user_id): int
    {
        $user = User::find()->where(['id' => $user_id])->one();
        return $user ? $user->getWishlistItemsCount() : 0;
    }
}
