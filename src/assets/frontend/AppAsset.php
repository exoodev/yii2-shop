<?php

namespace exoo\shop\assets\frontend;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@exoo/shop/assets/frontend';
    public $css = [
        'css/shop.css',
    ];
    public $js = [
        'js/shop.js'
    ];
    public $depends = [
        'exoo\uikit\UikitAsset',
        'exoo\storejs\StoreJsAsset',
        'exoo\fontawesome\FontawesomeAsset',
    ];
}
