$(document).ready(function() {
	wishlist.init()
	cart.init()
});

var product = {
	grid: function (columns) {
		this.columns = columns
		var _this = this

		this.changeMode()

		$(document).on('click', '[data-mode-view]', function (e) {
			e.preventDefault()
			var type = $(this).data('modeView')
			_this.changeMode(type)
			$.post('/shop/api/product/mode-view', {mode: type})
		});
	},
	changeMode: function (type) {
		var $list = $('.product-items')
		var $item = $('.product-item')
		var $image = $('.product-image')
		var $misc = $('.product-misc')
		var widthCssClasses = this._getWidthCssClasses()
		var column = 'uk-width-1-4@m'

		if (!$item.length) return

		if (type) {
			store.set('product-mode-view', type)
		} else {
			type = store.get('product-mode-view')
		}

		if (type == 'list') {
			// $list.addClass('uk-child-width-1-1')
			// $list.removeClass(widthCssClasses)
			// $image.parent().addClass(column).removeClass('uk-width-1-1')
			// $misc.parent().addClass(column).removeClass('uk-width-1-1')
			$list.addClass('list').removeClass('block')
		} else if (type == 'block') {
			// $list.addClass(widthCssClasses)
			// $list.removeClass('uk-child-width-1-1')
			// $image.parent().addClass('uk-width-1-1').removeClass(column)
			// $misc.parent().addClass('uk-width-1-1').removeClass(column)
			$list.addClass('block').removeClass('list')
		}
		
		UIkit.update($list, 'update')
	},
	_getWidthCssClasses: function () {
		var classes = []
		$.each(this.columns, function (size, value) {
			classes.push('uk-child-width-1-' + value + '@' + size)
		})

		return classes.join(' ')
	},
	_getCssClasses: function (element, className) {
		var classes = []
		var elClasses = element.attr('class').split(' ')

		for (var index in elClasses) {
			if (elClasses[index].search(className) !== -1) {
				classes.push(elClasses[index])
			}
		}
		return classes.join(' ')
	},
	getId: function (el) {
		if (!el instanceof jQuery) {
			el = $(el)
		}
		return el.closest('[data-id]').data('id')
	}
}

var wishlist = {
	init: function () {
		var _this = this

		$('.shop-wishlist-toggle').each(function (index, element) {
			_this.setTooltip(element)
		})
	},
	setTooltip: function (element) {
		var $el = $(element),
			action = $el.data('action')
		UIkit.tooltip($el, { title: wishlist_tooltip[action] })
	},
	toggle: function (event) {
		event.preventDefault()

		var $el = $(event.currentTarget),
			action = $el.data('action'),
			url = '/shop/api/wishlist/' + action + '?id=' + product.getId($el)
		
		wishlist[action]($el, url)
	},
	add: function ($el, url) {
		var _this = this
        $.post(url).done(function (res) {
			if (res.result) {
				$el.data('action', 'delete').html('<i class="fa-heart fa-lg fas uk-text-danger"></i>')
				_this.setTooltip($el)
            }
        });
	},
	delete: function ($el, url) {
		var _this = this
		$.post(url).done(function (res) {
			if (res.result) {
				$el.data('action', 'add').html('<i class="fa-heart fa-lg far"></i>')
				_this.setTooltip($el)
			}
        });
	}
}

var cart = {
	init: function () {
		$(document).on('afterSubmit', '#addToCartForm', function (e, data) {
			cart.update(data);
		});
	},
	update: function (data) {
		var $link = $('.shop-cart-link');
		var $total = $link.find('.shop-cart-total');
		var $name = $link.find('.shop-cart-name');

		if (data.totalAmount) {
			$total.show();
			$name.hide();
			$link.find('.shop-cart-price').html(data.totalPrice);
			$link.find('.shop-cart-amount').html(data.totalAmount);
		} else {
			$total.hide();
			$name.show();
		}
	},
	getTotal: function() {
		var total;
		$.get('/shop/api/cart/total').done(function (response) {
			total = response;
		});

		return total;
	}
}

var compare = {
	add: function (e) {
		e.preventDefault();
		var $e = $(e.target);

		$.post($e.data('url'), {product_id: $e.data('product')}).done(function (data) {
			console.log(data);
		});
	}
}
