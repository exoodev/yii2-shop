<?php

namespace exoo\shop\widgets\frontend;

use exoo\shop\entities\Category\Category;
use exoo\shop\repositories\read\CategoryReadRepository;
use yii\base\Widget;
use yii\helpers\Html;

class Categories extends Widget
{
    /**
     * @var array
     */
    public $options = ['class' => 'uk-nav-default uk-nav-parent-icon'];
    /**
     * @var array
     */
    public $itemOptions = [];
    /**
     * @var array
     */
    public $linkOptions = [];
    /**
     * @var string|boolean
     */
    public $clientOptions = true;
    /**
     * @var Category|null
     */
    public $active;
    /**
     * @var integer
     */
    public $minWidth = 320;

    private $categories;

    public function __construct(CategoryReadRepository $categories, $config = [])
    {
        parent::__construct($config);
        $this->categories = $categories;

        $this->options['style'] = ['min-width' => $this->minWidth . 'px'];
    }

    public function run(): string
    {
        return $this->renderItems($this->categories->getTree());
    }

    protected function renderItems($items): string
    {
        $result = [];

        foreach ($items as $i => $item) {
            $options = $this->options;
            $itemOptions = $this->itemOptions;
            $depth = $item->category->depth;
            $active = $this->active && ($this->active->id == $item->category->id || $this->active->isChildOf($item->category));

            if ($depth == 2) {
                $options['class'] = 'uk-nav-sub';
            } elseif ($depth > 2) {
                $options['class'] = null;
            } else {
                $options['uk-nav'] = $this->clientOptions;
            }

            $link = Html::a(Html::encode($item->category->name), ['/shop/catalog/category', 'id' => $item->category->id], $this->linkOptions);

            if (!empty($item->items)) {
                if ($depth == 1) {
                    Html::addCssClass($itemOptions, 'uk-parent');
                    if ($active) {
                        Html::addCssClass($itemOptions, 'uk-open');
                    }
                }

                $link .= $this->renderItems($item->items);
            }

            if ($active) {
                Html::addCssClass($itemOptions, 'uk-active');
            }
            $result[] = Html::tag('li', $link, $itemOptions);
        }

        return Html::tag('ul', implode(PHP_EOL, $result), $options);
    }
}
