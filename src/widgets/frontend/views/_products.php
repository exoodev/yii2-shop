<?php
use exoo\shop\helpers\PriceHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div uk-dropdown style="width:600px">
    <table class="uk-table uk-table-divider uk-table-small">
        <?php foreach ($cart->getItems() as $item): ?>
        <?php
        $product = $item->getProduct();
        $modification = $item->getModification();
        $url = Url::to(['/shop/catalog/product', 'id' => $product->id]);
        ?>
        <tr>
            <td>
                <?php if ($product->mainImage): ?>
                    <img src="<?= $product->mainImage->url('small') ?>" alt="" class="img-thumbnail">
                <?php endif; ?>
            </td>
            <td class="text-left">
                <a href="<?= $url ?>"><?= Html::encode($product->name) ?></a>
                <?php if ($modification): ?>
                    <br/><small><?= Html::encode($modification->name) ?></small>
                <?php endif; ?>
            </td>
            <td>x <?= $item->getQuantity() ?></td>
            <td><?= PriceHelper::format($item->getCost()) ?></td>
            <td>
                <a href="<?= Url::to(['/shop/cart/remove', 'id' => $item->getId()]) ?>" title="Remove" class="uk-button uk-button-danger btn-xs" data-method="post"><i class="fa fa-times"></i></a>
            </td>
        </tr>
        <?php endforeach ?>
    </table>
    <div>
        <?php $cost = $cart->getCost(); ?>
        <table class="uk-table uk-table-divider uk-table-small">
            <tr>
                <td><strong>Sub-Total:</strong></td>
                <td><?= PriceHelper::format($cost->getOrigin()) ?></td>
            </tr>
            <?php foreach ($cost->getDiscounts() as $discount): ?>
                <tr>
                    <td><strong><?= Html::encode($discount->getName()) ?>:</strong></td>
                    <td><?= PriceHelper::format($discount->getValue()) ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td><strong>Total:</strong></td>
                <td><?= PriceHelper::format($cost->getTotal()) ?></td>
            </tr>
        </table>
        <p>
            <a href="<?= Url::to(['/shop/cart/index']) ?>"><strong><i class="fa fa-shopping-cart"></i> View Cart</strong></a>
            <a href="/index.php?route=checkout/checkout"><strong><i class="fa fa-share"></i> Checkout</strong></a>
        </p>
    </div>
</div>
