<?php

namespace exoo\shop\widgets\frontend;

use Yii;
use yii\base\Widget;
use exoo\shop\components\cart\Cart;
use yii\helpers\Html;
use exoo\shop\helpers\PriceHelper;

class CartWidget extends Widget
{
    public $options = [];
    public $iconOptions = ['uk-icon' => 'cart'];
    public $labelOptions = ['class' => 'uk-margin-small-left'];
    public $totalOptions = [];
    public $amountOptions = ['class' => 'uk-label uk-margin-small-left'];

    public $template = '{icon} {label} {total}';
    public $totalTemplate = '{price} {currency} {amount}';
    
    private $cart;

    public function __construct(Cart $cart, $config = [])
    {
        parent::__construct($config);
        $this->cart = $cart;
    }

    public function run(): string
    {
        $amount = $this->cart->getAmount();
        $total = $this->cart->getCost()->getTotal();
        $hidden = ['display' => 'none'];

        if ($amount) {
            $this->labelOptions['style'] = $hidden;
        } else {
            $this->totalOptions['style'] = $hidden;
        }

        $icon = Html::tag('span', null, $this->iconOptions);
        $label = Html::tag('span', Yii::t('shop', 'Shopping Cart'), $this->labelOptions);

        Html::addCssClass($this->amountOptions, 'shop-cart-amount');
        $total = Html::tag('span', strtr($this->totalTemplate, [
            '{price}' => '<span class="shop-cart-price uk-margin-small-left">' . PriceHelper::format($total, false) . '</span>',
            '{currency}' => '<span class="shop-cart-currency">' . PriceHelper::currency($total) . '</span>',
            '{amount}' => Html::tag('span', $amount, $this->amountOptions),
        ]), $this->totalOptions);

        Html::addCssClass($this->options, 'shop-cart-link');
        
        return Html::a(strtr($this->template, [
            '{icon}' => $icon,
            '{label}' => $label,
            '{total}' => $total,
        ]), ['/shop/cart/index'], $this->options);
    }
}
