<?php

namespace exoo\shop\widgets\frontend;

use yii\helpers\Html;
use yii\widgets\InputWidget;
use yii\base\InvalidConfigException;

class Modifications extends InputWidget
{
    /**
     * @var array
     */
    public $items = [];
    /**
     * @var array
     */
    public $linkOptions = [];
    /**
     * @var string
     */
    public $productSlug;

    public function init()
    {
        if (!$this->items === null) {
            throw new InvalidConfigException("The 'items' option is required.");
        }
        $this->options['value'] = $this->value;
        parent::init();
    }

    public function run()
    {
        $buttons = [];
        foreach ($this->items as $item) {
            $active = $this->options['value'] == $item->id;
            $linkOptions = $this->linkOptions;
            $text = Html::encode($item->name);

            if ($item->color) {
                $linkOptions['uk-tooltip'] = $text;
                $text = '<span style="background-color: ' . $item->color . '"></span>';
                $class = 'shop-button-color' . ($active ? ' active' : '');
            } else {
                $class = 'uk-button uk-button-small uk-button-' . ($active ? 'primary' : 'default');
            }

            Html::addCssClass($linkOptions, $class);

            $buttons[] = Html::a($text, ['product', 'slug' => $this->productSlug, 'mId' => $item->id], $linkOptions);
        }

        echo Html::tag('div', implode(PHP_EOL, $buttons), ['uk-margin' => true]);

        if ($this->hasModel()) {
            echo Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        }
    }
}
