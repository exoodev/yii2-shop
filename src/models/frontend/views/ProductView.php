<?php
namespace exoo\shop\models\frontend\views;

use Yii;
use exoo\shop\entities\Product\Product;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class ProductView extends Object
{
    public $product;
    public $sku;
    public $modification;
    public $images;
    public $values;
    public $priceNew;
    public $priceOld;
    public $quantity;
    public $unit;

    public function __construct(Product $product, int $modificationId = null)
    {
        $this->product = $product;

        if ($modificationId) {
            $modification = $product->getModification($modificationId);
            $this->sku = $modification->sku;
            $this->priceNew = $modification->price_new;
            $this->priceOld = $modification->price_old;
            $this->quantity = $modification->quantity;
            $this->images = $modification->images ?: $product->images;
            $this->values = $modification->values;
            $this->modification = $modification;
        } else {
            $this->sku = $product->sku;
            $this->priceNew = $product->price_new;
            $this->priceOld = $product->price_old;
            $this->quantity = $product->quantity;
            $this->images = $product->images;
            $this->values = $product->values;
        }
        $this->unit = $product->unit;
    }

    // public function allowOrder(): bool
    // {
    //     return Yii::$app->settings->get('shop', 'allowOrder') && $this->quantity == 0;
    // }

    public function getModificationsList(): array
    {
        return ArrayHelper::map($this->product->modifications, 'id', 'name');
    }
}
