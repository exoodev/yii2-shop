<?php

namespace exoo\shop\models\frontend\forms;

use Yii;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\entities\Product\Product;
use exoo\shop\helpers\PriceHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class AddToCartForm extends Model
{
    public $modificationId;
    public $quantity;

    private $_product;

    public function __construct(Product $product, $mId = null, $config = [])
    {
        $this->_product = $product;
        if ($mId) {
            $this->modificationId = $product->getModification($mId);
        }
        $this->quantity = 1;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return array_filter([
            ['quantity', 'required'],
            [['modificationId', 'quantity'], 'integer'],
            $this->_product->modifications ? ['modificationId', 'required', 'message' => Yii::t('shop', 'Select') . ' ' . mb_strtolower($this->_product->modification_name)] : false,
            Yii::$app->settings->get('shop', 'allowStock') ? ['quantity', 'checkMaxQuantity'] : false,
        ]);
    }

    public function checkMaxQuantity($attribute, $params): void
    {
        $inStock = $this->_product->getQuantity($this->modificationId);

        if ($this->$attribute > $inStock) {
            $this->addError($attribute, Yii::t('shop', 'This goods is only available in {quantity} {unit}', [
                'quantity' => $inStock,
                'unit' => $this->_product->unit->short_name,
            ]));
        }
    }

    public function getSlug(): string
    {
        return $this->_product->slug;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'modificationId' => $this->_product->modification_name,
            'quantity' => Yii::t('shop', 'Quantity'),
        ];
    }

    public function getModifications(): array
    {
        $modifications = $this->_product->modifications;

        if (Yii::$app->settings->get('shop', 'allowStock')) {
            foreach ($modifications as $key => $modification) {
                if (!$modification->quantity) {
                    unset($modifications[$key]);
                }
            }
        }

        return $modifications;
    }
}
