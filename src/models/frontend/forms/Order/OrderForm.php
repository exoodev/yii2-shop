<?php

namespace exoo\shop\models\frontend\forms\Order;

use Yii;
use elisdn\compositeForm\CompositeForm;

/**
 * @property DeliveryForm $delivery
 * @property CustomerForm $customer
 */
class OrderForm extends CompositeForm
{
    public $note;

    public function __construct(int $weight, array $config = [])
    {
        $this->delivery = new DeliveryForm($weight);
        $this->customer = new CustomerForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['note'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'note' => Yii::t('shop', 'Note'),
        ];
    }

    protected function internalForms(): array
    {
        return ['delivery', 'customer'];
    }
}
