<?php

namespace exoo\shop\models\frontend\forms\Order;

use Yii;
use yii\base\Model;

class CustomerForm extends Model
{
    public $phone;
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['phone', 'name'], 'required'],
            [['phone', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'phone' => Yii::t('shop', 'Phone'),
            'name' => Yii::t('shop', 'First Name'),
        ];
    }
}
