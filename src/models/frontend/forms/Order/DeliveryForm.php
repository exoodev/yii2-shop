<?php

namespace exoo\shop\models\frontend\forms\Order;

use Yii;
use exoo\shop\entities\DeliveryMethod;
use exoo\shop\helpers\PriceHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class DeliveryForm extends Model
{
    public $method;
    public $index;
    public $address;

    private $_weight;

    public function __construct(int $weight, array $config = [])
    {
        $this->_weight = $weight;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_filter([
            [['method'], 'integer'],
            Yii::$app->settings->get('shop', 'deliveryStatus') ? [['index', 'address'], 'required'] : false,
            [['index'], 'string', 'max' => 255],
            [['address'], 'string'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'method' => Yii::t('shop', 'Method'),
            'index' => Yii::t('shop', 'Post code'),
            'address' => Yii::t('shop', 'Address'),
        ];
    }

    public function deliveryMethodsList(): array
    {
        $methods = DeliveryMethod::find()->availableForWeight($this->_weight)->orderBy('position')->all();

        return ArrayHelper::map($methods, 'id', function (DeliveryMethod $method) {
            return $method->name . ' (' . PriceHelper::format($method->cost) . ')';
        });
    }
}
