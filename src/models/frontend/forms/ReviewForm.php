<?php

namespace exoo\shop\models\frontend\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use exoo\shop\entities\Product\Review;

class ReviewForm extends Model
{
    public $vote;
    public $plus;
    public $minus;
    public $comment;
    public $maxRating;

    public function init()
    {
        parent::init();
        $this->maxRating = Yii::$app->settings->get('shop', 'maxRating');
    }

    public function rules(): array
    {
        return array_filter([
            [['vote', 'comment'], 'required'],
            [['vote'], 'in', 'range' => array_keys($this->votesList())],
            [['plus', 'minus', 'comment'], 'string'],
            Yii::$app->settings->get('shop', 'plusMinusReview') ? [['plus', 'minus'], 'required'] : false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vote' => Yii::t('shop', 'Vote'),
            'plus' => Yii::t('shop', 'Advantages'),
            'minus' => Yii::t('shop', 'Disadvantages'),
            'comment' => Yii::t('shop', 'Comment'),
        ];
    }

    public function votesList(): array
    {
        $result = [];
        for ($i = 1; $i < $this->maxRating + 1; $i++) {
            $result[$i] = $i;
        }

        return $result;
    }

    public function getProductReviews($product_id): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => Review::find()->where(['id' => $product_id])->active(),
            'sort' => false,
        ]);
    }
}
