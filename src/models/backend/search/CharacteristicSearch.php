<?php

namespace exoo\shop\models\backend\search;

use exoo\shop\helpers\CharacteristicHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\entities\Characteristic\CharacteristicGroup;

class CharacteristicSearch extends Model
{
    public $id;
    public $name;
    public $type;
    public $required;
    public $group_id;

    public function rules(): array
    {
        return [
            [['id', 'type', 'required', 'group_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Characteristic::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'required' => $this->required,
            'group_id' => $this->group_id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function typesList(): array
    {
        return CharacteristicHelper::typeList();
    }

    public function requiredList(): array
    {
        return [
            1 => \Yii::$app->formatter->asBoolean(true),
            0 => \Yii::$app->formatter->asBoolean(false),
        ];
    }

    public function getGroupsList(): array
    {
        return ArrayHelper::map(CharacteristicGroup::find()->all(), 'id', 'name');
    }
}
