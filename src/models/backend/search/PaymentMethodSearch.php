<?php

namespace exoo\shop\models\backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use exoo\shop\entities\PaymentMethod;

class PaymentMethodSearch extends Model
{
    public $id;
    public $name;
    public $transactionLimit;
    public $status;

    public function rules(): array
    {
        return [
            [['id', 'status', 'transactionLimit'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = PaymentMethod::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'transaction_limit' => $this->transactionLimit,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function getStatusList()
    {
        return PaymentMethod::getStatusList();
    }
}
