<?php

namespace exoo\shop\models\backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use exoo\shop\entities\Unit;

class UnitSearch extends Model
{
    public $id;
    public $name;
    public $short_name;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'short_name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Unit::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'short_name', $this->short_name]);

        return $dataProvider;
    }
}
