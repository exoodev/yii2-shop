<?php

namespace exoo\shop\models\backend\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use exoo\shop\entities\Discount;

class DiscountSearch extends Model
{
    public $id;
    public $name;
    public $percent;
    public $from_date;
    public $to_date;
    public $status;

    public function rules(): array
    {
        return [
            [['id', 'percent', 'status'], 'integer'],
            [['name', 'from_date', 'to_date'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Discount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['position' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'percent' => $this->percent,
            'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'from_date', $this->from_date])
            ->andFilterWhere(['like', 'to_date', $this->to_date]);

        return $dataProvider;
    }

    public function getStatusList()
    {
        return Discount::getStatusList();
    }
}
