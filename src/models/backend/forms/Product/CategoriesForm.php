<?php

namespace exoo\shop\models\backend\forms\Product;

use Yii;
use exoo\shop\entities\Category\Category;
use exoo\shop\entities\Product\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class CategoriesForm extends Model
{
    public $main;
    public $others = [];

    public function __construct(Product $product = null, $config = [])
    {
        if ($product) {
            if (Yii::$app->settings->get('shop', 'characteristicsToCategory')
                && Yii::$app->request->get('charPjax')) {
                $this->main = Yii::$app->request->get('main', null);
                $this->others = Yii::$app->request->get('others', []);
            } else {
                $this->main = $product->category_id;
                $this->others = ArrayHelper::getColumn($product->categoryAssignments, 'category_id');
            }
        }
        parent::__construct($config);
    }

    public function getIds()
    {
        return array_merge([$this->main], $this->others);
    }

    public function getCategoriesList(): array
    {
        return Category::find()
            ->select(['id', 'name AS title', 'depth'])
            ->where(['>', 'depth', 0])
            ->orderBy('lft')
            ->asArray()
            ->all();
    }

    public function rules(): array
    {
        return [
            ['main', 'required'],
            ['main', 'integer'],
            ['others', 'each', 'rule' => ['integer']],
            ['others', 'default', 'value' => []],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'main' => Yii::t('shop', 'Category'),
            'others' => Yii::t('shop', 'Additional categories'),
        ];
    }
}
