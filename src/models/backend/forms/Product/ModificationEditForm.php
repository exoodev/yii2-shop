<?php

namespace exoo\shop\models\backend\forms\Product;

use Yii;
use elisdn\compositeForm\CompositeForm;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\models\backend\forms\Product\ValueForm;
use exoo\shop\models\backend\forms\Product\PriceForm;
use exoo\shop\models\backend\forms\Product\CategoriesForm;
use yii\base\Model;

/**
 * @property ValueForm[] $values
 * @property PriceForm[] $price
 */
class ModificationEditForm extends CompositeForm
{
    public $sku;
    public $name;
    public $weight;
    public $length;
    public $width;
    public $height;
    public $color;
    public $quantity;

    private $_product;
    private $_modification;

    public function __construct(Product $product, Modification $modification, $config = [])
    {
        $this->sku = $modification->sku;
        $this->name = $modification->name;
        $this->weight = $modification->weight;
        $this->length = $modification->length;
        $this->width = $modification->width;
        $this->height = $modification->height;
        $this->color = $modification->color;
        $this->quantity = $modification->quantity;
        $this->price = new PriceForm($modification);

        $this->_product = $product;
        $this->_modification = $modification;
        parent::__construct($config);
    }

    public function getValues(): array
    {
        $query = Characteristic::find();

        if (Yii::$app->settings->get('shop', 'characteristicsToCategory')) {
            $categories = new CategoriesForm($this->_product);
            $query
                ->joinWith('categoryCharacteristics c', false)
                ->orWhere(['c.category_id' => $categories->ids]);
            if ($this->_modification) {
                $query
                    ->joinWith(['modificationValues v'], false)
                    ->orWhere(['v.modification_id' => $this->_modification->id]);
            }
        }
        return array_map(function(Characteristic $characteristic) {
            return new ValueForm($characteristic, $this->_product->getValue($characteristic->id));
        }, $query->orderBy('position')->all());
    }

    public function getId()
    {
        return $this->_modification->id;
    }

    public function rules(): array
    {
        $rules = [
            [['sku', 'name'], 'required'],
            [
                'sku',
                'unique',
                'targetClass' => Product::class,
                'filter' => ['<>', 'id', $this->_product->id],
            ],
            [
                'sku',
                'unique',
                'targetClass' => Modification::class,
                'filter' => ['<>', 'id', $this->id],
            ],
            [['weight', 'length', 'width', 'height'], 'number', 'min' => 0],
            ['quantity', 'integer', 'min' => 0],
            ['quantity', 'default', 'value' => 0],
            ['color', 'string', 'max' => 50],
        ];

        if (Yii::$app->settings->get('shop', 'allowStock')) {
            $rules[] = ['quantity', 'required'];
        }

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'sku' => Yii::t('shop', 'SKU'),
            'quantity' => Yii::t('shop', 'Quantity'),
            'weight' => Yii::t('shop', 'Weight in kg.'),
            'length' => Yii::t('shop', 'Length'),
            'width' => Yii::t('shop', 'Width'),
            'height' => Yii::t('shop', 'Height'),
            'color' => Yii::t('shop', 'Color'),
            'dimensions' => Yii::t('shop', 'Dimensions (LxWxH) in cm.'),
        ];
    }

    protected function internalForms(): array
    {
        return ['values', 'price'];
    }
}
