<?php

namespace exoo\shop\models\backend\forms\Product;

use yii\base\Model;

class ProductCreateForm extends Model
{
    public $name;

    public function rules(): array
    {
        return [
            [['name'], 'string', 'max' => 255],
            ['name', 'default', 'value' => ''],
        ];
    }
}
