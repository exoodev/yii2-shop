<?php

namespace exoo\shop\models\backend\forms\Product;

use Yii;
use yii\base\Model;

class PriceForm extends Model
{
    public $old;
    public $new;

    /**
     * @param Product|Modification $entitie.
     */
    public function __construct($entitie = null, $config = [])
    {
        if ($entitie) {
            $this->new = $entitie->price_new;
            $this->old = $entitie->price_old;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['new'], 'required'],
            [['old', 'new'], 'number', 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'old' => Yii::t('shop', 'Price old'),
            'new' => Yii::t('shop', 'Price'),
        ];
    }
}
