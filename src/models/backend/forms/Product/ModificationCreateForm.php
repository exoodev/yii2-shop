<?php

namespace exoo\shop\models\backend\forms\Product;

use Yii;
use elisdn\compositeForm\CompositeForm;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\models\backend\forms\Product\PriceForm;

/**
 * @property PriceForm[] $price
 */
class ModificationCreateForm extends CompositeForm
{
    public $sku;
    public $name;
    public $weight;
    public $length;
    public $width;
    public $height;
    public $color;
    public $quantity;

    public function __construct($config = [])
    {
        $this->price = new PriceForm();
        parent::__construct($config);
    }

    public function rules(): array
    {
        $rules = [
            [['name'], 'required'],
            ['sku', 'unique', 'targetClass' => Product::class],
            ['sku', 'unique', 'targetClass' => Modification::class],
            [['weight', 'length', 'width', 'height'], 'number', 'min' => 0],
            ['quantity', 'integer', 'min' => 0],
            ['color', 'string', 'max' => 50],
        ];

        if (Yii::$app->settings->get('shop', 'allowStock')) {
            $rules[] = ['quantity', 'required'];
        }

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'sku' => Yii::t('shop', 'SKU'),
            'quantity' => Yii::t('shop', 'Quantity'),
            'weight' => Yii::t('shop', 'Weight in kg.'),
            'length' => Yii::t('shop', 'Length'),
            'width' => Yii::t('shop', 'Width'),
            'height' => Yii::t('shop', 'Height'),
            'color' => Yii::t('shop', 'Color'),
            'dimensions' => Yii::t('shop', 'Dimensions (LxWxH) in cm.'),
        ];
    }

    protected function internalForms(): array
    {
        return ['price'];
    }
}
