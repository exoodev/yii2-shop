<?php

namespace exoo\shop\models\backend\forms\Product;

use Yii;
use exoo\shop\entities\Brand;
use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Unit;
use exoo\shop\entities\Warehouse;
use elisdn\compositeForm\CompositeForm;
use exoo\shop\models\backend\forms\MetaForm;
use exoo\shop\models\backend\forms\Product\QuantityForm;
use yii\helpers\ArrayHelper;

/**
 * @property QuantityForm $quantity
 * @property MetaForm $meta
 * @property CategoriesForm $categories
 * @property TagsForm $tags
 * @property ValueForm[] $values
 */
class ProductEditForm extends CompositeForm
{
    public $brandId;
    public $sku;
    public $name;
    public $shortDescription;
    public $description;
    public $weight;
    public $slug;
    public $modificationName;
    public $warehouseId;
    public $unitId;
    public $length;
    public $width;
    public $height;
    public $adminNote;
    public $status;

    private $_product;

    public function __construct(Product $product, $config = [])
    {
        $this->brandId = $product->brand_id;
        $this->sku = $product->sku ?: $this->getSkuNumber($product);
        $this->name = $product->name;
        $this->slug = $product->slug;
        $this->shortDescription = $product->short_description;
        $this->description = $product->description;
        $this->modificationName = $product->modification_name;
        $this->warehouseId = $product->warehouse_id;
        $this->unitId = $product->unit_id;
        $this->weight = $product->weight;
        $this->length = $product->length;
        $this->width = $product->width;
        $this->height = $product->height;
        $this->adminNote = $product->admin_note;
        $this->status = $product->status;
        $this->quantity = new QuantityForm($product);
        $this->meta = new MetaForm($product->meta);
        $this->price = new PriceForm($product);
        $this->categories = new CategoriesForm($product);
        $this->tags = new TagsForm($product);
        $this->values = $this->getValues($product);

        $this->_product = $product;
        parent::__construct($config);
    }

    public function getSkuNumber($product)
    {
        $settings = Yii::$app->settings;
        if ($settings->get('shop', 'useSkuPrefix')) {
            $prefix = $settings->get('shop', 'skuPrefix', '');
            $number = '';

            if ($settings->get('shop', 'insertIdSku')) {
                $leadingZeros = $settings->get('shop', 'leadingZeros');
                $number = str_pad($product->id, $leadingZeros, 0, STR_PAD_LEFT);
            }
            return $prefix . $number;
        }
    }

    public function getValues($product): array
    {
        $query = Characteristic::find();

        if (Yii::$app->settings->get('shop', 'characteristicsToCategory')) {
            $query
                ->joinWith(['productValues v'], false)
                ->orWhere(['v.product_id' => $product->id])
                ->joinWith('categoryCharacteristics c', false)
                ->orWhere([
                    'c.category_id' => array_merge(
                        [$this->categories->main],
                        $this->categories->others
                    )
                ]);
        }
        return array_map(function(Characteristic $characteristic) use ($product) {
            return new ValueForm($characteristic, $product->getValue($characteristic->id));
        }, $query->orderBy('position')->all());
    }

    public function rules(): array
    {
        return [
            [['brandId', 'sku', 'name', 'weight'], 'required'],
            ['modificationName', 'required', 'when' => function($model) {
                return $this->_product->modifications;
            }, 'enableClientValidation' => false],
            [['brandId', 'warehouseId', 'unitId', 'status'], 'integer'],
            [['sku', 'name', 'slug'], 'string', 'max' => 255],
            [['sku'], 'unique', 'targetClass' => Product::class, 'filter' => $this->_product ? ['<>', 'id', $this->_product->id] : null],
            [['shortDescription', 'description', 'adminNote'], 'string'],
            [['shortDescription', 'description'], 'trim'],
            [['weight', 'length', 'width', 'height'], 'number', 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'shortDescription' => Yii::t('shop', 'Short description'),
            'description' => Yii::t('shop', 'Description'),
            'modificationName' => Yii::t('shop', 'Modification name'),
            'warehouseId' => Yii::t('shop', 'Warehouse'),
            'brandId' => Yii::t('shop', 'Brand'),
            'unitId' => Yii::t('shop', 'Unit'),
            'slug' => Yii::t('shop', 'Slug'),
            'sku' => Yii::t('shop', 'SKU'),
            'weight' => Yii::t('shop', 'Weight in kg.'),
            'length' => Yii::t('shop', 'Length in cm.'),
            'width' => Yii::t('shop', 'Width in cm.'),
            'height' => Yii::t('shop', 'Height in cm.'),
            'dimensions' => Yii::t('shop', 'Dimensions (LxWxH) in cm.'),
            'status' => Yii::t('shop', 'Status'),
            'adminNote' => Yii::t('shop', 'Admin note'),
        ];
    }

    public function getWarehousesList()
    {
        return ArrayHelper::map(Warehouse::find()->asArray()->all(), 'id', 'name');
    }

    public function getBrandsList()
    {
        return ArrayHelper::map(Brand::find()->asArray()->all(), 'id', 'name');
    }

    public function getUnitsList()
    {
        return ArrayHelper::map(Unit::find()->asArray()->all(), 'id', 'short_name');
    }

    protected function internalForms(): array
    {
        return ['meta', 'categories', 'tags', 'values', 'price', 'quantity'];
    }
}
