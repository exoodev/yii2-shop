<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Discount;
use elisdn\compositeForm\CompositeForm;

class DiscountForm extends CompositeForm
{
    public $percent;
    public $name;
    public $fromDate;
    public $toDate;
    public $description;
    public $status;

    private $_discount;

    public function __construct(Discount $discount = null, $config = [])
    {
        if ($discount) {
            $this->percent = $discount->percent;
            $this->name = $discount->name;
            $this->fromDate = $discount->from_date;
            $this->toDate = $discount->to_date;
            $this->description = $discount->description;
            $this->status = $discount->status;
            $this->_discount = $discount;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'percent'], 'required'],
            ['name', 'string', 'max' => 255],
            [['percent', 'status'], 'integer'],
            [['fromDate', 'toDate'], 'date', 'format' => 'yyyy-MM-dd'],
            ['description', 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'percent' => Yii::t('shop', 'Percent'),
            'fromDate' => Yii::t('shop', 'From date'),
            'toDate' => Yii::t('shop', 'To date'),
            'position' => Yii::t('shop', 'Sorting'),
            'status' => Yii::t('shop', 'Status'),
            'created_at' => Yii::t('shop', 'Created at'),
            'updated_at' => Yii::t('shop', 'Updated at'),
            'created_by' => Yii::t('shop', 'Created by'),
            'updated_by' => Yii::t('shop', 'Updated by'),
        ];
    }

    public function internalForms(): array
    {
        return ['meta'];
    }
}
