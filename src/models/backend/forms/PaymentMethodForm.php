<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\PaymentMethod;
use yii\base\Model;

class PaymentMethodForm extends Model
{
    public $name;
    public $icon;
    public $shortDescription;
    public $description;
    public $transactionLimit;
    public $status;

    public function __construct(PaymentMethod $method = null, $config = [])
    {
        if ($method) {
            $this->name = $method->name;
            $this->icon = $method->icon;
            $this->shortDescription = $method->short_description;
            $this->description = $method->description;
            $this->transactionLimit = $method->transaction_limit;
            $this->status = $method->status;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name', 'icon'], 'string', 'max' => 255],
            [['shortDescription', 'description'], 'string'],
            [['transactionLimit', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'icon' => Yii::t('shop', 'Icon'),
            'shortDescription' => Yii::t('shop', 'Short description'),
            'description' => Yii::t('shop', 'Description'),
            'transactionLimit' => Yii::t('shop', 'Transaction limit'),
            'status' => Yii::t('shop', 'Status'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }
}
