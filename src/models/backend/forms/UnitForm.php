<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Unit;
use yii\base\Model;

class UnitForm extends Model
{
    public $name;
    public $short_name;

    private $_unit;

    public function __construct(Unit $unit = null, $config = [])
    {
        if ($unit) {
            $this->name = $unit->name;
            $this->short_name = $unit->short_name;
            $this->_unit = $unit;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'short_name'], 'required'],
            [['name', 'short_name'], 'string', 'max' => 255],
            ['short_name', 'unique', 'targetClass' => Unit::class, 'filter' => $this->_unit ? ['<>', 'id', $this->_unit->id] : null]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'short_name' => Yii::t('shop', 'Short name'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }
}
