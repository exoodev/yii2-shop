<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Characteristic\CharacteristicGroup;
use yii\base\Model;

class CharacteristicGroupForm extends Model
{
    public $name;

    private $_characteristicGroup;

    public function __construct(CharacteristicGroup $characteristicGroup = null, $config = [])
    {
        if ($characteristicGroup) {
            $this->name = $characteristicGroup->name;
            $this->_characteristicGroup = $characteristicGroup;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }
}
