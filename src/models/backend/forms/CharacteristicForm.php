<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\entities\Characteristic\CharacteristicGroup;
use exoo\shop\helpers\CharacteristicHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * @property array $variants
 */
class CharacteristicForm extends Model
{
    public $name;
    public $slug;
    public $group_id;
    public $description;
    public $type;
    public $required;
    public $default;
    public $textVariants;

    private $_characteristic;

    public function __construct(Characteristic $characteristic = null, $config = [])
    {
        if ($characteristic) {
            $this->name = $characteristic->name;
            $this->slug = $characteristic->slug;
            $this->description = $characteristic->description;
            $this->group_id = $characteristic->group_id;
            $this->type = $characteristic->type;
            $this->required = $characteristic->required;
            $this->default = $characteristic->default;
            $this->textVariants = implode(PHP_EOL, $characteristic->variants);
            $this->_characteristic = $characteristic;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'type'], 'required'],
            [['required'], 'boolean'],
            ['group_id', 'integer'],
            [['default'], 'string', 'max' => 255],
            [['textVariants', 'description'], 'string'],
            [['name'], 'unique', 'targetClass' => Characteristic::class, 'filter' => $this->_characteristic ? ['<>', 'id', $this->_characteristic->id] : null]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'group_id' => Yii::t('shop', 'Group'),
            'slug' => Yii::t('shop', 'Slug'),
            'type' => Yii::t('shop', 'Type'),
            'textVariants' => Yii::t('shop', 'Values list'),
            'default' => Yii::t('shop', 'Default value'),
            'required' => Yii::t('shop', 'Required'),
        ];
    }

    public function getTypesList(): array
    {
        return CharacteristicHelper::typeList();
    }

    public function getGroupsList()
    {
        return ArrayHelper::map(CharacteristicGroup::find()->all(), 'id', 'name');
    }

    public function getVariants(): array
    {
        return $this->textVariants ? preg_split('#\s+#i', $this->textVariants) : [];
    }
}
