<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Group\Group;
use exoo\shop\entities\Characteristic\Characteristic;
use elisdn\compositeForm\CompositeForm;
use exoo\shop\models\backend\forms\MetaForm;
use yii\helpers\ArrayHelper;

/**
 * @property MetaForm $meta;
 */
class GroupForm extends CompositeForm
{
    public $name;
    public $slug;
    public $title;
    public $description;
    public $parentId;
    public $characteristicIds;

    private $_group;

    public function __construct(Group $group = null, $config = [])
    {
        if ($group) {
            $this->name = $group->name;
            $this->slug = $group->slug;
            $this->title = $group->title;
            $this->description = $group->description;
            $this->parentId = $group->parent ? $group->parent->id : null;
            $this->characteristicIds = $group->characteristicIds;
            $this->meta = new MetaForm($group->meta);
            $this->_group = $group;
        } else {
            $this->meta = new MetaForm();
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['parentId'], 'integer'],
            [['name', 'slug', 'title'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['name', 'slug'], 'unique', 'targetClass' => Group::class, 'filter' => $this->_group ? ['<>', 'id', $this->_group->id] : null],
            ['characteristicIds', 'safe'],
            ['parentId', 'default', 'value' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'title' => Yii::t('shop', 'Title'),
            'description' => Yii::t('shop', 'Description'),
            'slug' => Yii::t('shop', 'Slug'),
            'characteristicIds' => Yii::t('shop', 'Characteristics'),
            'parentId' => Yii::t('shop', 'Parent group'),
        ];
    }

    public function categoriesList(): array
    {
        $categories = Group::find()
            ->where(['>', 'depth', 0])
            ->orderBy('lft')
            ->asArray();

        if ($this->_group) {
            $unsetIds = $this->_group->children()->select('id')->column();
            $unsetIds[] = $this->_group->id;
            $categories->andWhere(['not', ['id' => $unsetIds]]);
        }

        return ArrayHelper::map($categories->all(), 'id', function(array $group) {
            return str_repeat('-- ', $group['depth'] - 1) . $group['name'];
        });
    }

    public function getCharacteristicsList(): array
    {
        return ArrayHelper::map(Characteristic::find()->asArray()->all(), 'id', 'name');
    }

    public function internalForms(): array
    {
        return ['meta'];
    }
}
