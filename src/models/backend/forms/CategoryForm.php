<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Category\Category;
use exoo\shop\entities\Characteristic\Characteristic;
use elisdn\compositeForm\CompositeForm;
use exoo\shop\models\backend\forms\MetaForm;
use yii\helpers\ArrayHelper;

/**
 * @property MetaForm $meta;
 */
class CategoryForm extends CompositeForm
{
    public $name;
    public $slug;
    public $title;
    public $description;
    public $parentId;
    public $characteristicIds;

    private $_category;

    public function __construct(Category $category = null, $config = [])
    {
        if ($category) {
            $this->name = $category->name;
            $this->slug = $category->slug;
            $this->title = $category->title;
            $this->description = $category->description;
            $this->parentId = $category->parent ? $category->parent->id : null;
            $this->characteristicIds = $category->characteristicIds;
            $this->meta = new MetaForm($category->meta);
            $this->_category = $category;
        } else {
            $this->meta = new MetaForm();
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['parentId'], 'integer'],
            [['name', 'slug', 'title'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['name', 'slug'], 'unique', 'targetClass' => Category::class, 'filter' => $this->_category ? ['<>', 'id', $this->_category->id] : null],
            ['characteristicIds', 'safe'],
            ['parentId', 'default', 'value' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'title' => Yii::t('shop', 'Title'),
            'description' => Yii::t('shop', 'Description'),
            'slug' => Yii::t('shop', 'Slug'),
            'characteristicIds' => Yii::t('shop', 'Characteristics'),
            'parentId' => Yii::t('shop', 'Parent category'),
        ];
    }

    public function getCategoriesList(): array
    {
        $categories = Category::find()
            ->where(['>', 'depth', 0])
            ->orderBy('lft')
            ->asArray();

        if ($this->_category) {
            $unsetIds = $this->_category->children()->select('id')->column();
            $unsetIds[] = $this->_category->id;
            $categories->andWhere(['not', ['id' => $unsetIds]]);
        }

        return ArrayHelper::map($categories->all(), 'id', function (array $category) {
            return str_repeat('-- ', $category['depth'] - 1) . $category['name'];
        });
    }

    public function getCharacteristicsList(): array
    {
        return ArrayHelper::map(Characteristic::find()->asArray()->all(), 'id', 'name');
    }

    public function internalForms(): array
    {
        return ['meta'];
    }
}
