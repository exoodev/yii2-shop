<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\DeliveryMethod;
use yii\base\Model;

class DeliveryMethodForm extends Model
{
    public $name;
    public $cost;
    public $minWeight;
    public $maxWeight;
    public $status;

    public function __construct(DeliveryMethod $method = null, $config = [])
    {
        if ($method) {
            $this->name = $method->name;
            $this->cost = $method->cost;
            $this->minWeight = $method->min_weight;
            $this->maxWeight = $method->max_weight;
            $this->status = $method->status;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'cost'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['cost', 'minWeight', 'maxWeight', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'minWeight' => Yii::t('shop', 'Min weight'),
            'maxWeight' => Yii::t('shop', 'Max weight'),
            'cost' => Yii::t('shop', 'Cost'),
            'status' => Yii::t('shop', 'Status'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }
}
