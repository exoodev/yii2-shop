<?php

namespace exoo\shop\models\backend\forms;

use Yii;
use exoo\shop\entities\Warehouse;
use yii\base\Model;

class WarehouseForm extends Model
{
    public $name;
    public $description;

    private $_warehouse;

    public function __construct(Warehouse $warehouse = null, $config = [])
    {
        if ($warehouse) {
            $this->name = $warehouse->name;
            $this->description = $warehouse->description;
            $this->_warehouse = $warehouse;
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            ['name', 'string', 'max' => 255],
            ['description', 'string'],
            ['name', 'unique', 'targetClass' => Warehouse::class, 'filter' => $this->_warehouse ? ['<>', 'id', $this->_warehouse->id] : null]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
        ];
    }
}
