<?php

namespace exoo\shop\models\backend;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use exoo\shop\entities\Product\Review;
use exoo\shop\entities\Currency;

/**
 * Setting class.
 */
class Settings extends Model
{
    const NUMBER_SEPARATOR_SPACE = 'space';
    const NUMBER_SEPARATOR_DOT = 'dot';
    const NUMBER_SEPARATOR_COMMA = 'comma';

    const VIEW_TYPE_STRING = 'string';
    const VIEW_TYPE_LIST = 'list';
    const VIEW_TYPE_LINK = 'links';

    // SKU
    /**
    * @var integer the number leading zeros
    */
    public $leadingZeros = 6;
    /**
    * @var boolean the sku prefix
    */
    public $useSkuPrefix = true;
    /**
     * @var string the sku prefix
     */
    public $skuPrefix = 'AAA-';
    /**
     * @var boolean the insert model id in sku
     */
    public $insertIdSku = true;

    // Characterisctics

    /**
     * @var boolean bind the characteristics to the category
     */
    public $characteristicsToCategory = false;
    /**
     * @var boolean bind the characteristics to the group
     */
    public $characteristicsToGroup = false;
    /**
     * @var boolean show characteristics in the product list (frontend)
     */
    public $showCharacteristicsInListProduct = false;
    /**
     * @var string type characteristics in the product list (frontend)
     */
    public $valuesInListProducts = 'list';
    /**
     * @var boolean show modifications in the product list (frontend)
     */
    public $showModificationsInListProduct = true;
    /**
     * @var integer the character displayed as the decimal point when formatting a number.
     */
    public $decimalSeparator;
    /**
     * @var string the character displayed as the thousands separator character when formatting a number.
     */
    public $thousandSeparator;
    /**
     * @var string the maximum fraction digits
     */
    public $maxFractionDigits = 0;
    /**
     * @var string the minimum fraction digits
     */
    public $minFractionDigits = 0;
    /**
     * @var string the currency
     */
    public $currency = 'RUB';
    /**
     * @var boolean учитывать остатки товаров
     */
    public $allowStock = true;
    /**
     * @var boolean разрешить заказ товара
     */
    public $allowOrder = true;
    /**
     * @var boolean показывать артикул (frontend)
     */
    public $siteShowSku = true;
    /**
     * @var boolean показывать остаток (frontend)
     */
    public $siteShowStock = false;
    /**
     * @var boolean скрыть товары, отсутствующие на складе (frontend)
     */
    public $hideProductsAreNotInStock = true;
    /**
     * @var boolean отзывы
     */
    public $reviews = true;
    /**
     * @var string the property
     */
    public $reviewsNote;
    /**
     * @var string тип представления модификаций в карточке товара (frontend)
     */
    public $modificationsInProductCard = 'links';

    // Reviews

    /**
     * @var boolean добавить отзыв можно только купив товар
     */
    public $canAddReviewOnlyBuyer = false;
    /**
     * @var integer максимальная оценка
     */
    public $maxRating = 5;
    /**
     * @var boolean описание достоинства и недостатков товара в отзыве
     */
    public $plusMinusReview = true;


    // Delivary

    /**
    * @var boolean the property
    */
    public $deliveryStatus = true;

    // Compare

    /**
    * @var boolean the property
    */
    public $compareStatus = false;

    // Catalog

    /**
    * @var string the property
    */
    public $titleCatalog = 'Catalog';
    /**
    * @var boolean the property
    */
    public $showTitleCatalog = false;

    // Categories

    /**
    * @var boolean показывать виджет списока категорий на сайте
    */
    public $showCategoryWidget = true;
    /**
     * @var string позиция виджет списка категорий в теме сайта
     */
    public $positionCategoryWidget;


    /**
     * @var boolean
     */
    private $_reviewsExists;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'skuPrefix',
                'valuesInListProducts',
                'currency',
                'decimalSeparator',
                'thousandSeparator',
                'modificationsInProductCard',
                'titleCatalog',
                'positionCategoryWidget',
            ], 'string', 'max' => 255],
            [[
                'useSkuPrefix',
                'insertIdSku',
                'characteristicsToCategory',
                'characteristicsToGroup',
                'showCharacteristicsInListProduct',
                'showModificationsInListProduct',
                'allowStock',
                'allowOrder',
                'siteShowSku',
                'siteShowStock',
                'deliveryStatus',
                'reviews',
                'hideProductsAreNotInStock',
                'canAddReviewOnlyBuyer',
                'plusMinusReview',
                'compareStatus',
                'showTitleCatalog',
                'showCategoryWidget',
            ], 'boolean'],
            [['leadingZeros'], 'integer', 'max' => 10],
            [['maxFractionDigits', 'minFractionDigits'], 'in', 'range' => [0, 1, 2]],
            [['maxFractionDigits', 'minFractionDigits'], 'integer'],
            [['reviewsNote'], 'string'],
            ['maxRating', 'integer', 'when' => function() {
                return $this->canChangeMaxRating();
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'skuPrefix' => Yii::t('shop', 'SKU prefix'),
            'useSkuPrefix' => Yii::t('shop', 'Use SKU prefix'),
            'insertIdSku' => Yii::t('shop', 'Insert Item ID in SKU'),
            'leadingZeros' => Yii::t('shop', 'Number of leading zeros'),
            'characteristicsToCategory' => Yii::t('shop', 'Bind the characteristics to the category'),
            'characteristicsToGroup' => Yii::t('shop', 'Bind the characteristics to the group'),
            'showCharacteristicsInListProduct' => Yii::t('shop', 'Show characteristics'),
            'showModificationsInListProduct' => Yii::t('shop', 'Show modifications'),
            'valuesInListProducts' => Yii::t('shop', 'Сharacteristics View Type'),
            'modificationsInProductCard' => Yii::t('shop', 'Modifications View Type'),
            'currency' => Yii::t('shop', 'Currency'),
            'maxFractionDigits' => Yii::t('shop', 'Maximum fraction digits'),
            'minFractionDigits' => Yii::t('shop', 'Minimum fraction digits'),
            'decimalSeparator' => Yii::t('shop', 'Decimal separator'),
            'thousandSeparator' => Yii::t('shop', 'Thousands separator'),
            'allowStock' => Yii::t('shop', 'Allow stock'),
            'allowOrder' => Yii::t('shop', 'Allow product order with zero remaining'),
            'siteShowSku' => Yii::t('shop', 'Show SKU'),
            'siteShowStock' => Yii::t('shop', 'Show stock'),
            'deliveryStatus' => Yii::t('shop', 'Delivery'),
            'reviews' => Yii::t('shop', 'Reviews'),
            'reviewsNote' => Yii::t('shop', 'Note in reviews'),
            'hideProductsAreNotInStock' => Yii::t('shop', 'Hide products that are not in stock'),
            'canAddReviewOnlyBuyer' => Yii::t('shop', 'Add a review can only bought the product'),
            'maxRating' => Yii::t('shop', 'Maximum rating'),
            'plusMinusReview' => Yii::t('shop', 'Advantages and disadvantages of the product in the review'),
            'compareStatus' => Yii::t('shop', 'Compare products'),
            'titleCatalog' => Yii::t('shop', 'Title catalog'),
            'showTitleCatalog' => Yii::t('shop', 'Show catalog title'),
            'showCategoryWidget' => Yii::t('shop', 'Show category widget'),
            'positionCategoryWidget' => Yii::t('shop', 'Category widget position'),
        ];
    }

    public static function getViewTypesCharacteristics()
    {
        return [
            self::VIEW_TYPE_STRING => Yii::t('shop', 'String'),
            self::VIEW_TYPE_LIST => Yii::t('shop', 'List'),
        ];
    }

    public function getViewTypeCharacteristics()
    {
        $types = self::getViewTypesCharacteristics();
        return $types[$this->valuesInListProducts];
    }

    public static function getViewTypesModifications()
    {
        return [
            self::VIEW_TYPE_LINK => Yii::t('shop', 'Buttons'),
            self::VIEW_TYPE_LIST => Yii::t('shop', 'List'),
        ];
    }

    public function getViewTypeModifications()
    {
        $types = self::getViewTypes();
        return $types[$this->modificationsInProductCard];
    }

    public static function getNumberSeparators()
    {
        return [
            self::NUMBER_SEPARATOR_DOT => Yii::t('shop', 'Dot'),
            self::NUMBER_SEPARATOR_COMMA => Yii::t('shop', 'Comma'),
            self::NUMBER_SEPARATOR_SPACE => Yii::t('shop', 'Space'),
        ];
    }

    public static function getNumberSeparator($separator)
    {
        $separators = [
            self::NUMBER_SEPARATOR_DOT => '.',
            self::NUMBER_SEPARATOR_COMMA => ',',
            self::NUMBER_SEPARATOR_SPACE => ' ',
        ];
        return $separators[Yii::$app->settings->get('shop', '' . $separator)];
    }

    public function getCurrencyList()
    {
        return ArrayHelper::map(Currency::find()->all(), 'code', 'code');
    }

    public function canChangeMaxRating(): bool
    {
        if ($this->_reviewsExists === null) {
            $this->_reviewsExists = !Review::find()->exists();
        }

        return $this->_reviewsExists;
    }
}
