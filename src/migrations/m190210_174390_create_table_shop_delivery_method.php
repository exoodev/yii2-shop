<?php

use yii\db\Migration;

class m190210_174390_create_table_shop_delivery_method extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_delivery_method}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'cost' => $this->integer(),
            'min_weight' => $this->integer(),
            'max_weight' => $this->integer(),
            'status' => $this->tinyInteger()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%shop_delivery_method}}');
    }
}
