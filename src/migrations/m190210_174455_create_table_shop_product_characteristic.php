<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_characteristic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_characteristic}}', [
            'product_id' => $this->integer()->notNull(),
            'characteristic_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_product_characteristic}}', ['product_id', 'characteristic_id']);
        $this->createIndex('nom_id', '{{%shop_product_characteristic}}', 'product_id');
        $this->createIndex('characteristic_id', '{{%shop_product_characteristic}}', 'characteristic_id');
        $this->addForeignKey('shop_product_characteristic_ibfk_1', '{{%shop_product_characteristic}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_product_characteristic_ibfk_2', '{{%shop_product_characteristic}}', 'characteristic_id', '{{%shop_characteristic}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_characteristic}}');
    }
}
