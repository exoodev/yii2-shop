<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_related extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_related}}', [
            'product_id' => $this->integer()->notNull(),
            'related_id' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_product_related}}', ['product_id', 'related_id']);
        $this->createIndex('product_id', '{{%shop_product_related}}', 'product_id');
        $this->createIndex('related_id', '{{%shop_product_related}}', 'related_id');
        $this->addForeignKey('shop_product_related_ibfk_1', '{{%shop_product_related}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_product_related_ibfk_2', '{{%shop_product_related}}', 'related_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_related}}');
    }
}
