<?php

use yii\db\Migration;

class m190210_174454_create_table_shop_cart extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_cart}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'modification_id' => $this->integer(),
            'quantity' => $this->float()->notNull(),
        ], $tableOptions);

        $this->createIndex('modification_id', '{{%shop_cart}}', 'modification_id');
        $this->createIndex('product_id', '{{%shop_cart}}', 'product_id');
        $this->createIndex('user_id', '{{%shop_cart}}', 'user_id');
        $this->addForeignKey('shop_cart_ibfk_1', '{{%shop_cart}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_cart_ibfk_2', '{{%shop_cart}}', 'modification_id', '{{%shop_modification}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_cart_ibfk_3', '{{%shop_cart}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_cart}}');
    }
}
