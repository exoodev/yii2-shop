<?php

use yii\db\Migration;

class m190210_174443_create_table_shop_order_item extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_order_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer(),
            'product_sku' => $this->string()->notNull(),
            'product_name' => $this->string()->notNull(),
            'modification_id' => $this->integer(),
            'modification_name' => $this->string(),
            'modification_sku' => $this->string(),
            'price' => $this->decimal()->notNull(),
            'quantity' => $this->float()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('modification_id', '{{%shop_order_item}}', 'modification_id');
        $this->createIndex('idx-order_item', '{{%shop_order_item}}', ['order_id', 'product_id']);
        $this->createIndex('nom_id', '{{%shop_order_item}}', 'product_id');
        $this->addForeignKey('fk-shop_order_item-shop_order', '{{%shop_order_item}}', 'order_id', '{{%shop_order}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_order_item_ibfk_1', '{{%shop_order_item}}', 'product_id', '{{%shop_product}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('shop_order_item_ibfk_2', '{{%shop_order_item}}', 'modification_id', '{{%shop_modification}}', 'id', 'SET NULL', 'RESTRICT');
        $this->addForeignKey('shop_order_item_ibfk_3', '{{%shop_order_item}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_order_item}}');
    }
}
