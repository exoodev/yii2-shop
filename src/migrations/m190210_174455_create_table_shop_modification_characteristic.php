<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_modification_characteristic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_modification_characteristic}}', [
            'id' => $this->primaryKey(),
            'modification_id' => $this->integer()->notNull(),
            'characteristic_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('characteristic_id', '{{%shop_modification_characteristic}}', 'characteristic_id');
        $this->createIndex('modification_id', '{{%shop_modification_characteristic}}', 'modification_id');
        $this->addForeignKey('shop_modification_characteristic_ibfk_1', '{{%shop_modification_characteristic}}', 'modification_id', '{{%shop_modification}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_modification_characteristic_ibfk_2', '{{%shop_modification_characteristic}}', 'characteristic_id', '{{%shop_characteristic}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_modification_characteristic}}');
    }
}
