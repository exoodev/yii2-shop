<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_file extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_file}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('nom_id', '{{%shop_product_file}}', 'product_id');
        $this->addForeignKey('shop_product_file_ibfk_1', '{{%shop_product_file}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_file}}');
    }
}
