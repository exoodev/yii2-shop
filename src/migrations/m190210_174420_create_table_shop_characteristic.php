<?php

use yii\db\Migration;

class m190210_174420_create_table_shop_characteristic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_characteristic}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'group_id' => $this->integer(),
            'slug' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'default' => $this->string()->notNull(),
            'required' => $this->tinyInteger()->notNull(),
            'position' => $this->integer()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('group_id', '{{%shop_characteristic}}', 'group_id');
        $this->addForeignKey('shop_characteristic_ibfk_1', '{{%shop_characteristic}}', 'group_id', '{{%shop_characteristic_group}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_characteristic}}');
    }
}
