<?php

use yii\db\Migration;

class m190210_174441_create_table_shop_modification extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_modification}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'sku' => $this->string(),
            'weight' => $this->float(),
            'length' => $this->float(),
            'width' => $this->float(),
            'height' => $this->float(),
            'color' => $this->string(),
            'quantity' => $this->float(),
            'price_new' => $this->decimal(),
            'price_old' => $this->decimal(),
            'position' => $this->integer()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('nom_id', '{{%shop_modification}}', 'product_id');
        $this->createIndex('sku', '{{%shop_modification}}', 'sku');
        $this->addForeignKey('shop_modification_ibfk_1', '{{%shop_modification}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_modification}}');
    }
}
