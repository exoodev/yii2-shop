<?php

use yii\db\Migration;

class m190210_174380_create_table_shop_payment_method extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_payment_method}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'icon' => $this->string()->notNull(),
            'short_description' => $this->text(),
            'description' => $this->text(),
            'transaction_limit' => $this->integer(),
            'status' => $this->tinyInteger()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%shop_payment_method}}');
    }
}
