<?php

use yii\db\Migration;

class m190210_174454_create_table_shop_category_image extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category_image}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('category_id', '{{%shop_category_image}}', 'category_id');
        $this->addForeignKey('shop_category_image_ibfk_1', '{{%shop_category_image}}', 'category_id', '{{%shop_category}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_category_image}}');
    }
}
