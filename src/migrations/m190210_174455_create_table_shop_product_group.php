<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_group extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_group}}', [
            'product_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_product_group}}', ['product_id', 'group_id']);
        $this->createIndex('nom_id', '{{%shop_product_group}}', 'product_id');
        $this->createIndex('group_id', '{{%shop_product_group}}', 'group_id');
        $this->addForeignKey('shop_product_group_ibfk_1', '{{%shop_product_group}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_product_group_ibfk_2', '{{%shop_product_group}}', 'group_id', '{{%shop_group}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_group}}');
    }
}
