<?php

use yii\db\Migration;

class m190210_174433_create_table_shop_unit extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_unit}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'short_name' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('{{%shop_unit}}', [
            'id' => 1,
            'name' => 'Штука',
            'short_name' => 'шт.',
            'position' => 0,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%shop_unit}}');
    }
}
