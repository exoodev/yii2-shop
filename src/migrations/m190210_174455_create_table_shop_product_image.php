<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_image extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_image}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-shop_nom_image', '{{%shop_product_image}}', 'product_id');
        $this->addForeignKey('shop_product_image_ibfk_1', '{{%shop_product_image}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_image}}');
    }
}
