<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_review extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_review}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'plus' => $this->text()->notNull(),
            'minus' => $this->text()->notNull(),
            'comment' => $this->text()->notNull(),
            'vote' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('created_by', '{{%shop_review}}', 'created_by');
        $this->createIndex('nom_id', '{{%shop_review}}', 'product_id');
        $this->createIndex('updated_by', '{{%shop_review}}', 'updated_by');
        $this->addForeignKey('shop_review_ibfk_1', '{{%shop_review}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_review_ibfk_2', '{{%shop_review}}', 'created_by', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_review_ibfk_3', '{{%shop_review}}', 'updated_by', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_review}}');
    }
}
