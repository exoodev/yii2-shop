<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_group_characteristic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_group_characteristic}}', [
            'group_id' => $this->integer()->notNull(),
            'characteristic_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_group_characteristic}}', ['group_id', 'characteristic_id']);
        $this->createIndex('group_id', '{{%shop_group_characteristic}}', 'group_id');
        $this->createIndex('characteristic_id', '{{%shop_group_characteristic}}', 'characteristic_id');
        $this->addForeignKey('shop_group_characteristic_ibfk_1', '{{%shop_group_characteristic}}', 'group_id', '{{%shop_group}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_group_characteristic_ibfk_2', '{{%shop_group_characteristic}}', 'characteristic_id', '{{%shop_characteristic}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_group_characteristic}}');
    }
}
