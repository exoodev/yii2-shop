<?php

use yii\db\Migration;

class m190210_174442_create_table_shop_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_order}}', [
            'id' => $this->primaryKey(),
            'payment_method_id' => $this->integer(),
            'payment_method' => $this->string(),
            'delivery_method_id' => $this->integer(),
            'delivery_method_name' => $this->string(),
            'delivery_cost' => $this->integer(),
            'delivery_index' => $this->string(),
            'delivery_address' => $this->text(),
            'currency_id' => $this->integer(),
            'cost' => $this->decimal()->notNull(),
            'note' => $this->string(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'customer_phone' => $this->string(),
            'customer_name' => $this->string(),
            'statuses_json' => $this->text(),
        ], $tableOptions);

        $this->createIndex('delivery_method_id', '{{%shop_order}}', 'delivery_method_id');
        $this->createIndex('created_by', '{{%shop_order}}', 'created_by');
        $this->createIndex('payment_method_id', '{{%shop_order}}', 'payment_method_id');
        $this->createIndex('currency_id', '{{%shop_order}}', 'currency_id');
        $this->addForeignKey('shop_order_ibfk_1', '{{%shop_order}}', 'payment_method_id', '{{%shop_payment_method}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_order_ibfk_2', '{{%shop_order}}', 'delivery_method_id', '{{%shop_delivery_method}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_order_ibfk_3', '{{%shop_order}}', 'currency_id', '{{%shop_currency}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_order_ibfk_4', '{{%shop_order}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_order}}');
    }
}
