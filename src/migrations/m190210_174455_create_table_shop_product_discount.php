<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_product_discount extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_discount}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'discount_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('discount_id', '{{%shop_product_discount}}', 'discount_id');
        $this->createIndex('nom_id', '{{%shop_product_discount}}', 'product_id');
        $this->addForeignKey('shop_product_discount_ibfk_1', '{{%shop_product_discount}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('shop_product_discount_ibfk_2', '{{%shop_product_discount}}', 'discount_id', '{{%shop_discount}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product_discount}}');
    }
}
