<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_modification_image extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_modification_image}}', [
            'id' => $this->primaryKey(),
            'modification_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'position' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('modification_id', '{{%shop_modification_image}}', 'modification_id');
        $this->addForeignKey('shop_modification_image_ibfk_1', '{{%shop_modification_image}}', 'modification_id', '{{%shop_modification}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%shop_modification_image}}');
    }
}
