<?php

use yii\db\Migration;

class m190210_174440_create_table_shop_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'short_description' => $this->text(),
            'description' => $this->text(),
            'modification_name' => $this->string(),
            'category_id' => $this->integer(),
            'warehouse_id' => $this->integer(),
            'supplier_id' => $this->integer(),
            'brand_id' => $this->integer(),
            'unit_id' => $this->integer(),
            'type_id' => $this->integer(),
            'slug' => $this->string()->notNull(),
            'sku' => $this->string(),
            'weight' => $this->float()->notNull(),
            'length' => $this->float(),
            'width' => $this->float(),
            'height' => $this->float(),
            'quantity' => $this->integer()->notNull(),
            'price_new' => $this->decimal(),
            'price_old' => $this->decimal(),
            'rating' => $this->decimal(),
            'hits' => $this->integer()->notNull()->defaultValue(0),
            'admin_note' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'position' => $this->integer()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('brand_id', '{{%shop_product}}', 'brand_id');
        $this->createIndex('supplier_id', '{{%shop_product}}', 'supplier_id');
        $this->createIndex('warehouse_id', '{{%shop_product}}', 'warehouse_id');
        $this->createIndex('idx-shop_nom', '{{%shop_product}}', 'status');
        $this->createIndex('unit_id', '{{%shop_product}}', 'unit_id');
        $this->createIndex('category_id', '{{%shop_product}}', 'category_id');
        $this->addForeignKey('shop_product_ibfk_1', '{{%shop_product}}', 'brand_id', '{{%shop_brand}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_product_ibfk_2', '{{%shop_product}}', 'supplier_id', '{{%shop_supplier}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_product_ibfk_3', '{{%shop_product}}', 'unit_id', '{{%shop_unit}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_product_ibfk_4', '{{%shop_product}}', 'warehouse_id', '{{%shop_warehouse}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_product_ibfk_5', '{{%shop_product}}', 'category_id', '{{%shop_category}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_product}}');
    }
}
