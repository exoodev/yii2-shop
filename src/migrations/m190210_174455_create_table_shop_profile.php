<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_profile extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_profile}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'data' => $this->text()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_id', '{{%shop_profile}}', 'user_id');
        $this->addForeignKey('shop_profile_ibfk_1', '{{%shop_profile}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_profile}}');
    }
}
