<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_discount extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_discount}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'percent' => $this->integer()->notNull(),
            'from_date' => $this->date(),
            'to_date' => $this->date(),
            'position' => $this->integer()->notNull(),
            'status' => $this->tinyInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('created_by', '{{%shop_discount}}', 'created_by');
        $this->createIndex('updated_by', '{{%shop_discount}}', 'updated_by');
        $this->addForeignKey('shop_discount_ibfk_2', '{{%shop_discount}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_discount_ibfk_3', '{{%shop_discount}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_discount}}');
    }
}
