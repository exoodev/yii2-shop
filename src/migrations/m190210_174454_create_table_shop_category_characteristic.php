<?php

use yii\db\Migration;

class m190210_174454_create_table_shop_category_characteristic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category_characteristic}}', [
            'category_id' => $this->integer()->notNull(),
            'characteristic_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_category_characteristic}}', ['category_id', 'characteristic_id']);
        $this->createIndex('category_id', '{{%shop_category_characteristic}}', 'category_id');
        $this->createIndex('characteristic_id', '{{%shop_category_characteristic}}', 'characteristic_id');
        $this->addForeignKey('shop_category_characteristic_ibfk_1', '{{%shop_category_characteristic}}', 'category_id', '{{%shop_category}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_category_characteristic_ibfk_2', '{{%shop_category_characteristic}}', 'characteristic_id', '{{%shop_characteristic}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_category_characteristic}}');
    }
}
