<?php

use yii\db\Migration;

class m190210_174370_create_table_shop_currency extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_currency}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->string()->notNull(),
            'icon' => $this->string()->notNull(),
        ], $tableOptions);

        $this->batchInsert( '{{%shop_currency}}', ['id', 'name', 'code', 'icon'], [
            [1, 'RUB', 'RUB', 'fas fa-ruble-sign'],
            [2, 'EUR', 'EUR', 'fas fa-euro-sign'],
            [3, 'USD', 'USD', 'fas fa-dollar-sign'],
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%shop_currency}}');
    }
}
