<?php

use yii\db\Migration;

class m190210_174431_create_table_shop_warehouse extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_warehouse}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->insert('{{%shop_warehouse}}', [
            'id' => 1,
            'name' => 'Основной склад',
            'description' => '',
            'data' => null,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%shop_warehouse}}');
    }
}
