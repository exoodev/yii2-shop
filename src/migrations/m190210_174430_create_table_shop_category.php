<?php

use yii\db\Migration;

class m190210_174430_create_table_shop_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'tree' => $this->integer()->notNull(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->insert('{{%shop_category}}', [
            'id' => 1,
            'name' => '',
            'slug' => 'root',
            'title' => '',
            'description' => '',
            'lft' => 1,
            'rgt' => 2,
            'depth' => 0,
            'tree' => 0,
            'data' => null,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%shop_category}}');
    }
}
