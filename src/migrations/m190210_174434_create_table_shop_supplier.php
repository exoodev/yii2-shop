<?php

use yii\db\Migration;

class m190210_174434_create_table_shop_supplier extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_supplier}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'rep' => $this->string(),
            'phone' => $this->string(20)->notNull(),
            'address' => $this->string()->notNull(),
            'email' => $this->string(),
            'website' => $this->string()->notNull(),
            'inn' => $this->integer(10),
            'note' => $this->text(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('created_by', '{{%shop_supplier}}', 'created_by');
        $this->createIndex('updated_by', '{{%shop_supplier}}', 'updated_by');
        $this->addForeignKey('shop_supplier_ibfk_1', '{{%shop_supplier}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('shop_supplier_ibfk_2', '{{%shop_supplier}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_review}}');
    }
}
