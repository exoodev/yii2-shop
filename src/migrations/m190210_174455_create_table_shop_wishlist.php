<?php

use yii\db\Migration;

class m190210_174455_create_table_shop_wishlist extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_wishlist}}', [
            'product_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%shop_wishlist}}', ['product_id', 'user_id']);
        $this->createIndex('user_id', '{{%shop_wishlist}}', 'user_id');
        $this->createIndex('nom_id', '{{%shop_wishlist}}', 'product_id');
        $this->addForeignKey('shop_wishlist_ibfk_1', '{{%shop_wishlist}}', 'product_id', '{{%shop_product}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('shop_wishlist_ibfk_2', '{{%shop_wishlist}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_wishlist}}');
    }
}
