<?php

/**
 * @copyright Copyright &copy; ExooDev, exoodev.com, 2018
 * @package
 * @version 1.0.0
 */

namespace exoo\shop\modules\api;

/**
 * Undocumented class.
 *
 * @author ExooDev <info@exoodev.com>
 * @since 1.0
 */
class Module extends \yii\base\Module
{
}
