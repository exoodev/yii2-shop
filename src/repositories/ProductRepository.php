<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\components\dispatchers\EventDispatcher;
use exoo\shop\entities\Product\Product;
use exoo\shop\repositories\events\EntityPersisted;
use exoo\shop\repositories\events\EntityRemoved;
use exoo\shop\repositories\NotFoundException;

class ProductRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Product
    {
        if (!$product = Product::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Product')
            ]));
        }
        return $product;
    }

    public function existsByWarehouse($id): bool
    {
        return Product::find()->andWhere(['warehouse_id' => $id])->exists();
    }

    public function existsByBrand($id): bool
    {
        return Product::find()->andWhere(['brand_id' => $id])->exists();
    }

    public function existsByMainCategory($id): bool
    {
        return Product::find()->andWhere(['category_id' => $id])->exists();
    }

    public function save(Product $product): void
    {
        if (!$product->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatchAll($product->releaseEvents());
        $this->dispatcher->dispatch(new EntityPersisted($product));
    }

    public function remove(Product $product): void
    {
        if (!$product->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatchAll($product->releaseEvents());
        $this->dispatcher->dispatch(new EntityRemoved($product));
    }
}
