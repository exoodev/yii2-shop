<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\components\dispatchers\EventDispatcher;
use exoo\shop\entities\Group\Group;
use exoo\shop\repositories\events\EntityPersisted;
use exoo\shop\repositories\events\EntityRemoved;
use exoo\shop\repositories\NotFoundException;

class GroupRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Group
    {
        if (!$group = Group::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Group')
            ]));
        }
        return $group;
    }

    public function save(Group $group): void
    {
        if (!$group->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatch(new EntityPersisted($group));
    }

    public function remove(Group $group): void
    {
        if (!$group->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatch(new EntityRemoved($group));
    }
}
