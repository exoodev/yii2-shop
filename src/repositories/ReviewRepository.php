<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Product\Review;
use exoo\shop\repositories\NotFoundException;

class ReviewRepository
{
    public function get($id): Review
    {
        if (!$review = Review::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Review')
            ]));
        }
        return $review;
    }

    public function getBy($id, $userId): Review
    {
        if (!$review = Review::find()->where(['id' => $id, 'created_by' => $userId])->one()) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Review')
            ]));
        }
        return $review;
    }

    public function save(Review $review): void
    {
        if (!$review->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Review $review): void
    {
        if (!$review->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
