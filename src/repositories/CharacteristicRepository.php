<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Characteristic\Characteristic;
use exoo\shop\repositories\NotFoundException;

class CharacteristicRepository
{
    public function get($id): Characteristic
    {
        if (!$characteristic = Characteristic::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Characteristic')
            ]));
        }
        return $characteristic;
    }

    public function save(Characteristic $characteristic): void
    {
        if (!$characteristic->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Characteristic $characteristic): void
    {
        if (!$characteristic->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
