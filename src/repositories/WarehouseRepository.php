<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Warehouse;
use exoo\shop\repositories\NotFoundException;

class WarehouseRepository
{
    public function get($id): Warehouse
    {
        if (!$warehouse = Warehouse::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Warehouse')
            ]));
        }
        return $warehouse;
    }

    public function save(Warehouse $warehouse): void
    {
        if (!$warehouse->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Warehouse $warehouse): void
    {
        if (!$warehouse->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
