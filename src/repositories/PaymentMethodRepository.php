<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\PaymentMethod;
use exoo\shop\repositories\NotFoundException;

class PaymentMethodRepository
{
    public function get($id): PaymentMethod
    {
        if (!$method = PaymentMethod::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'PaymentMethod')
            ]));
        }
        return $method;
    }

    public function findByName($name): ?PaymentMethod
    {
        return PaymentMethod::findOne(['name' => $name]);
    }

    public function save(PaymentMethod $method): void
    {
        if (!$method->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(PaymentMethod $method): void
    {
        if (!$method->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
