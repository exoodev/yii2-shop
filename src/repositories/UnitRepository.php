<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Unit;
use exoo\shop\repositories\NotFoundException;

class UnitRepository
{
    public function get($id): Unit
    {
        if (!$unit = Unit::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Unit')
            ]));
        }
        return $unit;
    }

    public function save(Unit $unit): void
    {
        if (!$unit->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Unit $unit): void
    {
        if (!$unit->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
