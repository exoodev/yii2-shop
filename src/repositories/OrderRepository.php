<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Order\Order;
use exoo\shop\repositories\NotFoundException;

class OrderRepository
{
    public function get($id): Order
    {
        if (!$order = Order::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Order')
            ]));
        }
        return $order;
    }

    public function save(Order $order): void
    {
        if (!$order->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Order $order): void
    {
        if (!$order->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
