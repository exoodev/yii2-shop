<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\components\dispatchers\EventDispatcher;
use exoo\shop\entities\Category\Category;
use exoo\shop\repositories\events\EntityPersisted;
use exoo\shop\repositories\events\EntityRemoved;
use exoo\shop\repositories\NotFoundException;
use yii\caching\TagDependency;

class CategoryRepository
{
    private $dispatcher;

    public function __construct(EventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function get($id): Category
    {
        if (!$category = Category::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Category')
            ]));
        }
        return $category;
    }

    public function save(Category $category): void
    {
        if (!$category->save()) {
            throw new \RuntimeException('Saving error.');
        }
        $this->dispatcher->dispatch(new EntityPersisted($category));
    }

    public function remove(Category $category): void
    {
        if (!$category->delete()) {
            throw new \RuntimeException('Removing error.');
        }
        $this->dispatcher->dispatch(new EntityRemoved($category));
    }
}
