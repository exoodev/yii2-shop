<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Discount;
use exoo\shop\repositories\NotFoundException;

class DiscountRepository
{
    public function get($id): Discount
    {
        if (!$discount = Discount::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Discount')
            ]));
        }
        return $discount;
    }

    public function save(Discount $discount): void
    {
        if (!$discount->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Discount $discount): void
    {
        if (!$discount->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
