<?php

namespace exoo\shop\repositories;

use Yii;
use exoo\shop\entities\Characteristic\CharacteristicGroup;
use exoo\shop\repositories\NotFoundException;

class CharacteristicGroupRepository
{
    public function get($id): CharacteristicGroup
    {
        if (!$characteristicGroup = CharacteristicGroup::findOne($id)) {
            throw new NotFoundException(Yii::t('shop', '{model} is not found.', [
                'model' => Yii::t('shop', 'Characteristics group')
            ]));
        }
        return $characteristicGroup;
    }

    public function findByName($name): ?CharacteristicGroup
    {
        return CharacteristicGroup::findOne(['name' => $name]);
    }

    public function save(CharacteristicGroup $characteristicGroup): void
    {
        if (!$characteristicGroup->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(CharacteristicGroup $characteristicGroup): void
    {
        if (!$characteristicGroup->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
