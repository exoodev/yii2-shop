<?php

namespace exoo\shop\repositories\read;

use exoo\shop\entities\Order\Order;
use yii\data\ActiveDataProvider;

class OrderReadRepository
{
    public function getOwm($userId): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => Order::find()
                ->andWhere(['created_by' => $userId])
                ->orderBy(['id' => SORT_DESC]),
            'sort' => false,
        ]);
    }

    public function findOwn($userId, $id): ?Order
    {
        return Order::find()->andWhere(['created_by' => $userId, 'id' => $id])->one();
    }

    public function find($id): ?Order
    {
        return Order::find()->andWhere(['id' => $id])->one();
    }
}
