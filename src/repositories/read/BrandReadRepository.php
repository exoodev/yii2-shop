<?php

namespace exoo\shop\repositories\read;

use exoo\shop\entities\Brand;

class BrandReadRepository
{
    public function find($id): ?Brand
    {
        return Brand::findOne($id);
    }
}
