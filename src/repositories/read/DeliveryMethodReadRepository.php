<?php

namespace exoo\shop\repositories\read;

use exoo\shop\entities\DeliveryMethod;

class DeliveryMethodReadRepository
{
    public function getAll(): array
    {
        return DeliveryMethod::find()->orderBy('position')->all();
    }
}
