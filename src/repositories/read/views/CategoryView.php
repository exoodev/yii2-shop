<?php

namespace exoo\shop\repositories\read\views;

use exoo\shop\entities\Category\Category;

class CategoryView
{
    public $category;
    public $count;
    public $items;

    public function __construct(Category $category, $count, $items)
    {
        $this->category = $category;
        $this->count = $count;
        $this->items = $items;
    }
}
