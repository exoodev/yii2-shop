<?php

namespace exoo\shop\repositories\read;

use exoo\shop\entities\Tag;

class TagReadRepository
{
    public function find($id): ?Tag
    {
        return Tag::findOne($id);
    }
}
