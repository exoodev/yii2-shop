<?php

namespace exoo\shop\repositories\read;

use Elasticsearch\Client;
use exoo\shop\entities\Category\Category;
use exoo\shop\repositories\read\views\CategoryView;
use yii\helpers\ArrayHelper;

class CategoryReadRepository
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getRoot(): Category
    {
        return Category::find()->roots()->one();
    }

    /**
     * @return Category[]
     */
    public function getAll(): array
    {
        return Category::find()->andWhere(['>', 'depth', 0])->orderBy('lft')->all();
    }

    public function find($id): ?Category
    {
        return Category::find()->where(['id' => $id])->andWhere(['>', 'depth', 0])->one();
    }

    public function findBySlug($slug): ?Category
    {
        return Category::find()->andWhere(['slug' => $slug])->andWhere(['>', 'depth', 0])->one();
    }

    public function getTreeWithSubsOf(Category $category = null): array
    {
        $query = Category::find()->andWhere(['>', 'depth', 0])->orderBy('lft');

        if ($category) {
            $criteria = ['or', ['depth' => 1]];
            foreach (ArrayHelper::merge([$category], $category->parents) as $item) {
                $criteria[] = ['and', ['>', 'lft', $item->lft], ['<', 'rgt', $item->rgt], ['depth' => $item->depth + 1]];
            }
            $query->andWhere($criteria);
        } else {
            $query->andWhere(['depth' => 1]);
        }

        $aggs = $this->client->search([
            'index' => 'shop',
            'type' => 'products',
            'body' => [
                'size' => 0,
                'aggs' => [
                    'group_by_category' => [
                        'terms' => [
                            'field' => 'categories',
                        ]
                    ]
                ],
            ],
        ]);

        $counts = ArrayHelper::map($aggs['aggregations']['group_by_category']['buckets'], 'key', 'doc_count');

        return array_map(function (Category $category) use ($counts) {
            return new CategoryView($category, ArrayHelper::getValue($counts, $category->id, 0), []);
        }, $query->all());
    }

    public function getTree() {
        return $this->treeRecursion(Category::find()->orderBy('lft')->all());
    }

    protected function treeRecursion($nodes, $left = 0, $right = null, $depth = 1) {
        $result = [];
        foreach ($nodes as $index => $node) {
            if ($node->lft >= $left + 1 && (is_null($right) || $node->rgt <= $right) && $node->depth == $depth) {
                $result[$index] = new CategoryView($node, 0, $this->treeRecursion($nodes, $node->lft, $node->rgt, $node->depth + 1));
            }
        }
        return $result;
    }
}
