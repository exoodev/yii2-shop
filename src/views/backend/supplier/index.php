<?php

use yii\helpers\Html;
use exoo\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\search\SupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Suppliers');
//$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a(Yii::t('shop', 'Create'), ['create'], [
                'class' => 'uk-button uk-button-primary',
                'data-pjax' => 0
            ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure you want to delete?'),
                    ],
                'grid-selected' => true
            ]),
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'exoo\grid\CheckboxColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a(Html::encode($model->name), ['update', 'id' => $model->id]);
                },
            ],

            'phone',
            // 'address',
            // 'email:email',
            'website',
            'inn',
            'created_at',

            [
                'class' => 'exoo\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]) ?>
