<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $model exoo\shop\entities\Supplier */
/* @var $form yii\uikit\ActiveForm */

$this->title = Yii::t('shop', 'Supplier');

?>

<?php $form = ActiveForm::begin(); ?>
<div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
    </div>
    <div>
        <?= Html::submitButton(Yii::t('shop', 'Save'), [
            'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
                ]) ?>
                <?php if (!$model->isNewRecord): ?>
                <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $model->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    'class' => 'uk-button uk-button-danger',
                    ]) ?>
                    <?php endif; ?>
                </div>
            </div>
    <div class="uk-card uk-card-default uk-card-body uk-card-small">
        <div class="uk-grid-medium" uk-grid>
            <div class="uk-width-1-2@m">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'inn')->textInput() ?>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'rep')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="uk-width-expand@m">
                <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
                <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
