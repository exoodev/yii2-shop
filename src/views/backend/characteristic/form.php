<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Characteristic */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Characteristic');
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?=  Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?=  Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?=  Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <div uk-grid>
        <div class="uk-width-3-5@m">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'type')->dropdownList($model->typesList) ?>
            <?= $form->field($model, 'textVariants')->textarea(['rows' => 6])->hint(Yii::t('shop', 'All list items from a new line')) ?>
            <?= $form->field($model, 'default')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'required')->toggle() ?>
        </div>
        <div class="uk-width-expand@m">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'group_id')->dropdownList($model->groupsList, ['prompt' => '']) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 7]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
