<?php
use yii\helpers\Html;
?>


<div class="uk-grid-match uk-child-width-1-2@m" uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <h3><?= Html::encode(Yii::t('shop', 'SKU')) ?></h3>
            <?= $form->field($model, 'useSkuPrefix')->checkbox() ?>
            <?= $form->field($model, 'skuPrefix')->textInput(['maxlength' => 6]) ?>
            <?= $form->field($model, 'insertIdSku')->checkbox() ?>
            <?= $form->field($model, 'leadingZeros')->textInput([
                'type' => 'number',
                'min' => 0,
                'max' => 10
            ]) ?>
        </div>
    </div>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <?= $form->field($model, 'compareStatus')->checkbox(['disabled' => true]) ?>
            <div class="uk-alert uk-alert-warning">
                <p>Будет добавлено в новых версиях</p>
            </div>
        </div>
    </div>
</div>
<h3><?= Html::encode(Yii::t('shop', 'Site')) ?></h3>
<div class="uk-grid-match uk-child-width-1-2@m uk-grid-match" uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <h4>Список товаров</h4>
            <?= $form->field($model, 'showCharacteristicsInListProduct')->checkbox() ?>
            <?= $form->field($model, 'valuesInListProducts')->dropDownList($model->viewTypesCharacteristics) ?>
            <?= $form->field($model, 'showModificationsInListProduct')->checkbox() ?>
        </div>
    </div>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <h4>Карточка товара</h4>
            <?= $form->field($model, 'hideProductsAreNotInStock')->checkbox() ?>
            <?= $form->field($model, 'allowOrder')->checkbox() ?>
            <?= $form->field($model, 'siteShowSku')->checkbox() ?>
            <?= $form->field($model, 'siteShowStock')->checkbox() ?>
            <?= $form->field($model, 'modificationsInProductCard')->dropDownList($model->viewTypesModifications) ?>
        </div>
    </div>
</div>
<div class="uk-grid-match uk-child-width-1-2@m uk-grid-match" uk-grid>
    <div>
        <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <h4>Категории товаров</h4>
            <?= $form->field($model, 'showCategoryWidget')->checkbox() ?>
            <?= $form->field($model, 'positionCategoryWidget')->textinput() ?>
        </div>
    </div>
</div>
