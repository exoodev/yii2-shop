<?php
use exoo\joditeditor\JoditEditor;

?>

<?= $form->field($model, 'reviews')->checkbox() ?>
<?= $form->field($model, 'canAddReviewOnlyBuyer')->checkbox() ?>
<?= $form->field($model, 'maxRating')->textInput([
    'type' => 'number',
    'min' => 0,
    'class' => 'uk-width-1-6@m',
    'disabled' => !$model->canChangeMaxRating(),
]) ?>
<?= $form->field($model, 'plusMinusReview')->checkbox() ?>
<?= $form->field($model, 'reviewsNote')->widget(JoditEditor::class) ?>
