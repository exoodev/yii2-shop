<div class="uk-width-1-2@m">
    <?= $form->field($model, 'titleCatalog') ?>
    <?= $form->field($model, 'showTitleCatalog')->checkbox() ?>
    <?= $form->field($model, 'deliveryStatus')->checkbox() ?>
    <br>
    <?= $form->field($model, 'currency')->dropDownList($model->currencyList) ?>
    <?= $form->field($model, 'decimalSeparator')->dropDownList($model->numberSeparators) ?>
    <?= $form->field($model, 'thousandSeparator')->dropDownList($model->numberSeparators) ?>
    <?= $form->field($model, 'minFractionDigits')->textInput([
        'type' => 'number',
        'min' => 0,
        'max' => 2,
    ]) ?>
    <?= $form->field($model, 'maxFractionDigits')->textInput([
        'type' => 'number',
        'min' => 0,
        'max' => 2,
    ]) ?>

</div>
