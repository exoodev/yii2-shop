<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Characteristic */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Settings');
?>
<?php $form = ActiveForm::begin([
    'id' => 'settingsForm',
    'enableAjaxSubmit' => true
]); ?>
<div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-margin-remove"><?=  Html::encode($this->title) ?></h3>
    </div>
    <div>
        <?=  Html::submitButton(Yii::t('shop', 'Save'), [
            'class' => 'uk-button uk-button-primary',
        ]) ?>
    </div>
</div>
<ul uk-tab>
    <li><a href="#"><?= Yii::t('shop', 'Main') ?></a></li>
    <li><a href="#"><?= Yii::t('shop', 'Products') ?></a></li>
    <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
    <li><a href="#"><?= Yii::t('shop', 'Warehouse') ?></a></li>
    <li><a href="#"><?= Yii::t('shop', 'Reviews') ?></a></li>
</ul>

<ul class="uk-switcher uk-margin">
    <li><?= $this->render('settings/_main', [
        'form' => $form,
        'model' => $model,
    ]) ?></li>
    <li><?= $this->render('settings/_product', [
        'form' => $form,
        'model' => $model,
    ]) ?></li>
    <li><?= $this->render('settings/_characteristics', [
        'form' => $form,
        'model' => $model,
    ]) ?></li>
    <li><?= $this->render('settings/_warehouse', [
        'form' => $form,
        'model' => $model,
    ]) ?></li>
    <li><?= $this->render('settings/_reviews', [
        'form' => $form,
        'model' => $model,
    ]) ?></li>
</ul>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
$('#settingsPjax').on('pjax:end', function() {
    UIkit.update(element = document.body, event = 'update');
});
JS;
$this->registerJs($js);
