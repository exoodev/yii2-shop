<?php

use yii\helpers\Html;
use exoo\uikit\Nav;

$this->title = Yii::t('shop', 'Directories');
$menu = include Yii::getAlias('@shop') . '/config/backend/menu.php';
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= Nav::widget([
    'items' => $menu['directories']
]) ?>
