<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;
use exoo\joditeditor\JoditEditor;
use exoo\storage\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Category */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Category');
$module = $this->context->module;
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?=  Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?=  Html::a(Yii::t('shop', 'Close'), ['index'], [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <ul uk-tab="animation: uk-animation-fade">
        <li><a href="#"><?= Yii::t('shop', 'Main') ?></a></li>

        <?php if (Yii::$app->settings->get('shop', 'characteristicsToCategory')): ?>
            <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
        <?php endif; ?>

        <li><a href="#"><?= Yii::t('shop', 'SEO') ?></a></li>

        <?php if (isset($category)): ?>
            <li><a href="#"><?= Yii::t('shop', 'Images') ?></a></li>
        <?php endif; ?>
        
    </ul>

    <ul class="uk-switcher uk-margin">
        <li>
            <div class="uk-child-width-expand@m uk-grid-small" uk-grid>
                <div>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'parentId')->dropdownList($model->getCategoriesList(), [
                'prompt' => ''
            ]) ?>
            <?= $form->field($model, 'description')->widget(JoditEditor::className(), [
                'clientOptions' => [
                    'height' => '400px',
                ]
            ]) ?>
        </li>
        <?php if (Yii::$app->settings->get('shop', 'characteristicsToCategory')): ?>
        <li>
            <div class="uk-panel uk-panel-scrollable" style="height:500px">
                <?=
                $form
                ->field($model, 'characteristicIds')
                ->checkboxList($model->characteristicsList)
                ->label(false);
                ?>
            </div>
        </li>
        <?php endif; ?>
        <li>
            <?= $form->field($model->meta, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 7]) ?>
            <?= $form->field($model->meta, 'keywords')->textarea(['rows' => 7]) ?>
        </li>
        <li>
            <?php if (isset($category)): ?>
                <?= $form
                    ->field($category, 'filename')
                    ->widget(FileInput::className(), [
                        'multiple' => true,
                        'url' => ['images'],
                        'relation' => 'images',
                        'sortable' => true,
                        'clientOptions' => ArrayHelper::getValue($module->params, 'category.images.widgetOptions')
                    ])
                    ->label(false) ?>
            <?php endif; ?>
        </li>
    </ul>

    <?php ActiveForm::end(); ?>
</div>
