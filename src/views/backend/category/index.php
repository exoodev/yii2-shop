<?php

use yii\helpers\Html;
use exoo\nested\NestedView;

$this->title = Yii::t('shop', 'Categories');
?>
<div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h3 class="uk-card-title uk-margin-remove"><?= $this->title ?></h3>
    </div>
    <div>
        <?= Html::a(Yii::t('shop', 'Create'), ['create'], [
            'class' => 'uk-button uk-button-success'
        ]) ?>
    </div>
</div>
<?= NestedView::widget([
    'modelClass' => $modelClass,
    'itemContent' => function($id, $item) {
        return [
            'label' => $item->name,
            'encode' => false,
            'url' => ['update', 'id' => $id],
            'actions' => [
                [
                    'options' => [
                        'uk-tooltip' => Yii::t('shop', 'Add subcategory'),
                        'class' => 'fas fa-indent uk-margin-small-right',
                    ],
                    'url' => ['create', 'parent' => $id],
                ],
                // [
                //     'options' => [
                //         'uk-tooltip' => Yii::t('shop', 'Add product'),
                //         'class' => 'far fa-plus-square uk-margin-small-right',
                //     ],
                //     'url' => ['/shop/product/create', 'category' => $id],
                // ],
                [
                    'url' => ['delete', 'id' => $id],
                    'options' => [
                        'data' => [
                            'method' => 'post',
                            'confirm' => Yii::t('shop', 'Are you sure you want to delete the {item}?', [
                                'item' => $item->name
                            ]),
                        ],
                        'class' => 'far fa-trash-alt',
                        'uk-tooltip' => Yii::t('shop', 'Delete'),
                    ],
                ],
            ],
        ];
    }
]); ?>
