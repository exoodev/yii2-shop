<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\joditEditor\JoditEditor;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\PaymentMethod */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Payment method');

?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'shortDescription')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'description')->widget(JoditEditor::className(), [
        'clientOptions' => [
            'height' => '300px',
        ]
    ]) ?>
    <?= $form->field($model, 'transactionLimit')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'status')->dropDownList($method->statusList) ?>
    <?php ActiveForm::end(); ?>
</div>
