<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\shop\entities\PaymentMethod;
use exoo\position\PositionColumn;
use exoo\status\StatusColumn;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\search\PaymentMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Payment methods');
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a(Yii::t('shop', 'Create'), ['create'], [
                'class' => 'uk-button uk-button-primary',
                'data-pjax' => 0
            ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    ],
                'ex-selected' => true
            ]),
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => CheckboxColumn::class],

            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function(PaymentMethod $model) {
                    $link = Html::a(Html::encode($model->name), ['update', 'id' => $model->id]);
                    if ($model->short_description) {
                        $link .= PHP_EOL . Html::tag('div', HtmlPurifier::process($model->short_description), [
                            'class' => 'uk-text-small'
                        ]);
                    }
                    return $link;
                },
            ],
            [
                'attribute' => 'icon',
                'format' => 'html',
                'value' => function(PaymentMethod $model) {
                    if ($model->icon) {
                        return Html::icon($model->icon);
                    }
                    return '';
                },
            ],

            'transaction_limit',

            ['class' => PositionColumn::class],
            ['class' => StatusColumn::class],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}'
            ],
        ],
    ]) ?>
