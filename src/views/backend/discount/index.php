<?php

use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\status\StatusColumn;
use exoo\shop\entities\Discount;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Discounts');
?>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a(Yii::t('shop', 'Create'), ['create'], [
                'class' => 'uk-button uk-button-primary',
                'data-pjax' => 0
            ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                ],
                'ex-selected' => true
            ]),
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => CheckboxColumn::class],

            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function(Discount $model) {
                    return Html::a(Html::encode($model->name), ['update', 'id' => $model->id]);
                },
            ],
            'percent',
            'from_date',
            'to_date',
            ['class' => StatusColumn::class],
            ['class' => PositionColumn::class],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}'
            ],
        ],
    ]) ?>
<?php Pjax::end(); ?>
