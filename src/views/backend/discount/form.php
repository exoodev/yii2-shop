<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Discount */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('shop', 'Discount');
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?=  Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?=  Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>

    <div uk-grid>
        <div class="uk-width-expand@m">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'percent')->textInput() ?>
            <?= $form->field($model, 'fromDate')->textInput(['type' => 'date']) ?>
            <?= $form->field($model, 'toDate')->textInput(['type' => 'date']) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
        <div class="uk-width-1-3@m">
            <?= $form->field($model, 'status')->dropDownList($discount->statusList) ?>
            <?php if (!$discount->isNewRecord): ?>
                <?= DetailView::widget([
                    'model' => $discount,
                    'attributes' => [
                        'created_at:datetime',
                        'updated_at:datetime',
                        [
                            'label' => $discount->getAttributeLabel('created_by'),
                            'value' => $discount->createdBy->username,
                        ],
                        [
                            'label' => $discount->getAttributeLabel('updated_by'),
                            'value' => $discount->updatedBy->username,
                        ],
                    ],
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
