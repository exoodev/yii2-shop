<?php

use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\shop\entities\Brand;
use yii\widgets\Pjax;

$this->title = Yii::t('shop', 'Brands');
?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'buttons' => [
        Html::a(Yii::t('shop', 'Add'), ['create'], [
            'class' => 'uk-button uk-button-primary'
        ]),
        Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'exoo\grid\CheckboxColumn'],

        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function(Brand $model) {
                return Html::a(Html::encode($model->name), ['update', 'id' => $model->id]);
            },
        ],
        'slug',

        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}'
        ],
    ],
]) ?>
<?php Pjax::end(); ?>
