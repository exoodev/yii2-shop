<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Warehouse */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Warehouse');
?>

<div class="uk-card uk-card-default uk-card-body uk-card-small">
    <?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?=  Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?=  Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?=  Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?php ActiveForm::end(); ?>
</div>
