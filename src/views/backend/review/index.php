<?php

use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\status\StatusColumn;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\search\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Reviews');
//$this->params['breadcrumbs'][] = $this->title;
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            // Html::a(Yii::t('shop', 'Create'), ['create'], [
            //     'class' => 'uk-button uk-button-primary',
            //     'data-pjax' => 0
            // ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    ],
                'ex-selected' => true
            ]),
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => CheckboxColumn::class],

            // [
            //     'attribute' => 'id',
            //     'format' => 'html',
            //     'value' => function($model) {
            //         return Html::a($model->id, ['update', 'id' => $model->id]);
            //     },
            //     'headerOptions' => ['class' => 'uk-table-shrink'],
            // ],
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'product_id',
                'format' => 'html',
                'value' => function($model) {
                    $result = [
                        Html::a(Html::encode($model->product->name), ['/shop/product/view', 'id' => $model->product_id]),
                        Html::tag('div', Yii::t('shop', 'SKU') . ': ' . Html::encode($model->product->sku), [
                            'class' => 'uk-text-small uk-text-muted'
                        ]),
                        Html::tag('div', Yii::t('shop', 'Category') . ': ' . Html::encode($model->product->category->name), [
                            'class' => 'uk-text-small uk-text-muted'
                        ]),
                    ];

                    return implode('', $result);
                },
                'filter' => false,
                'headerOptions' => ['class' => 'uk-width-1-6@m'],
            ],

            [
                'attribute' => 'comment',
                'format' => 'html',
                'value' => function($model) {
                    $result = [];

                    if ($model->plus) {
                        $result[] = Html::tag('dt', $model->getAttributeLabel('plus'), [
                            'class' => 'uk-text-muted',
                            'style' => ['font-size' => '70%'],
                        ]);
                        $result[] = Html::tag('dd', $model->plus);
                    }

                    if ($model->minus) {
                        $result[] = Html::tag('dt', $model->getAttributeLabel('minus'), [
                            'class' => 'uk-text-muted',
                            'style' => ['font-size' => '70%'],
                        ]);
                        $result[] = Html::tag('dd', $model->minus);
                    }

                    $result[] = Html::tag('dt', $model->getAttributeLabel('comment'), [
                        'class' => 'uk-text-muted',
                        'style' => ['font-size' => '70%'],
                    ]);
                    $result[] = Html::tag('dd', $model->comment);

                    return Html::tag('dl', implode(PHP_EOL, $result), ['class' => 'uk-description-list uk-text-small']);
                },
            ],
            [
                'attribute' => 'vote',
                'format' => 'html',
                'value' => function($model) {
                    return Html::tag('div', $model->vote, ['class' => 'uk-label']);
                },
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            'created_at:datetime',
            [
                'attribute' => 'created_by',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a(Html::encode($model->createdBy->username), ['/system/user/view', 'id' => $model->created_by]);
                },
                'headerOptions' => ['class' => 'uk-table-shrink'],
                'filter' => false
            ],

            ['class' => StatusColumn::class],
            [
                'class' => ActionColumn::class,
                'template' => '{delete}'
            ],
        ],
    ]) ?>
