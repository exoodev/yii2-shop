<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $model exoo\shop\entities\Product\Review */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Review');

?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
            <?php if (!$model->isNewRecord): ?>
                <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $model->id], [
                    'data-method' => 'post',
                    'data-confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    'class' => 'uk-button uk-button-danger',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div uk-grid>
        <div class="uk-width-expand@m">
            <div class="uk-card uk-card-default uk-card-body uk-card-small">
                <?= $form->field($model, 'vote')->dropDownList([1,2,3,4,5]) ?>
                <?= $form->field($model, 'text')->textarea(['rows' => 10]) ?>
            </div>
        </div>
        <div class="uk-width-1-3@m">
            <div class="uk-card uk-card-default uk-card-body uk-card-small">
            <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'label' => $model->getAttributeLabel('product_id'),
                        'value' => Html::a(Html::encode($model->product->name), ['/shop/product/view', 'id' => $model->product_id]),
                        'format' => 'html'
                    ],
                    [
                        'label' => $model->getAttributeLabel('created_by'),
                        'value' => Html::encode($model->createdBy->username),
                    ],
                    [
                        'label' => $model->getAttributeLabel('updated_by'),
                        'value' => Html::encode($model->updatedBy->username),
                    ],
                    'created_at:datetime',
                    'updated_at:datetime',
                ],
                ]) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
