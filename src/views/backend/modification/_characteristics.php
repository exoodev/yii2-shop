<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(['id' => 'characteristicsPjax']); ?>
<div class="uk-width-1-2@m">
    <?php foreach ($model->values as $characteristic_id => $value) {
        $attribute = '[' . $characteristic_id . ']value';
        if ($list = $value->variantsList()) {
            echo $form->field($value, $attribute)->dropDownList($list, ['prompt' => '']);
        } else {
            echo $form->field($value, $attribute)->textInput();
        }
    } ?>
</div>
<?php Pjax::end(); ?>
