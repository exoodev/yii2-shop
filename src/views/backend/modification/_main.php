<?php

use yii\helpers\Html;

?>
<div class="uk-child-width-1-2@m" uk-grid>
    <div>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model->price, 'new')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model->price, 'old')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'quantity')->textInput(['type' => 'number', 'min' => 0]) ?>
    </div>
    <div>
        <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'weight')->textInput(['min' => 0]) ?>
        <div class="uk-margin">
            <?= Html::activeLabel($model, 'dimensions', ['class' => 'uk-form-label']) ?>
            <div class="uk-child-width-1-3@m uk-grid-small" uk-grid>
                <div><?= $form->field($model, 'length')->textInput(['min' => 0])->label(false) ?></div>
                <div><?= $form->field($model, 'width')->textInput(['min' => 0])->label(false) ?></div>
                <div><?= $form->field($model, 'height')->textInput(['min' => 0])->label(false) ?></div>
            </div>
        </div>
        <?= $form->field($model, 'color')->textInput(['type' => 'color', 'class' => 'uk-width-1-4']) ?>
    </div>
</div>
