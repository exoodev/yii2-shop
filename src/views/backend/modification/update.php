<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;
use exoo\storage\widgets\FileInput;

$this->title = Yii::t('shop', 'Product') . ': (' . $product->name . ')';
?>
<?php $form = ActiveForm::begin(['id' => 'modificationForm']); ?>
    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
                ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), ['product/view', 'id' => $product->id, '#' => 'modifications'], [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body uk-card-small">
        <h3 class="uk-card-title"><?= Html::encode(Yii::t('shop', 'Modification')) ?></h3>
        <div class="uk-margin" uk-grid>
            <div class="uk-width-auto@m">
                <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                    <li><a href="#"><?= Yii::t('shop', 'Main') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Images') ?></a></li>
                </ul>
            </div>
            <div class="uk-width-expand@m">
                <ul id="component-tab-left" class="uk-switcher">
                    <li><?= $this->render('_main', [
                        'form' => $form,
                        'model' => $model,
                    ]) ?></li>
                    <li><?= $this->render('_characteristics', [
                        'form' => $form,
                        'model' => $model,
                        'product' => $product,
                    ]) ?></li>
                    <li><?= $form
                        ->field($modification, 'filename')
                        ->widget(FileInput::class, [
                            'multiple' => true,
                            'url' => ['images'],
                            'relation' => 'images',
                            'sortable' => true,
                            'clientOptions' => ArrayHelper::getValue($this->context->module->params, 'product.images.widgetOptions')
                        ])
                        ->label(false) ?></li>
                </ul>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
