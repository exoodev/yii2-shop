<?php

use yii\helpers\Url;
use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\helpers\PriceHelper;
use exoo\shop\helpers\ImageHelper;

$this->title = Yii::t('shop', 'Nomenclature') . ': ' . $product->name;
?>

<?= GridView::widget([
    'id' => 'modificationsGrid',
    'dataProvider' => $modificationsProvider,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'summary' => false,
    'pjax' => true,
    'pjaxOptions' => ['id' => 'modificationsPjax'],
    'rowOptions' => function (Modification $model) {
        if (Yii::$app->settings->get('shop', 'allowStock')) {
            if ($model->quantity <= 0) {
                return ['style' => ['border-left' => '10px solid #f5607c']];
            }
        }
    },
    'columns' => [
        ['class' => CheckboxColumn::class],

        [
            'attribute' => 'image',
            'format' => 'raw',
            'value' => function(Modification $model) {
                if ($model->mainImage) {
                    return Html::img($model->mainImage->url('small'));
                }
                return ImageHelper::default();
            },
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'sku',
            'headerOptions' => ['class' => 'uk-table-shrink uk-text-nowrap'],
            'contentOptions' => ['class' => 'uk-text-nowrap'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function(Modification $model) use ($product) {
                return Html::a(
                    Html::encode($model->name),
                    [
                        'modification/update',
                        'id' => $model->id,
                        'product_id' => $product->id
                    ],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'quantity',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'price_new',
            'value' => function (Modification $model) {
                return PriceHelper::format($model->price_new);
            },
            'contentOptions' => ['class' => 'uk-text-nowrap uk-text-right'],
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],

        [
            'class' => PositionColumn::class,
            'controller' => 'modification',
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}',
            'urlCreator' => function($action, $model, $key, $index) use ($product) {
                return Url::to(['modification/' . $action, 'id' => $model->id, 'product_id' => $product->id]);
            },
        ],
    ],
]) ?>

<div class="uk-margin-top">
    <?= Html::a(Yii::t('shop', 'Add'), ['modification/create', 'product_id' => $product->id], [
        'class' => 'uk-button uk-button-primary',
        'ex' => [
            'modal' => true,
            'pjax-container' => '#modificationsPjax',
        ],
    ]) ?>
    <?= Html::a(Yii::t('shop', 'Delete'), ['modification/batch-delete', 'product_id' => $product->id], [
        'class' => 'uk-button uk-button-danger uk-hidden',
        'ex' => [
            'ajax' => true,
            'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
            'selected' => '#modificationsGrid',
            'pjax-container' => '#modificationsPjax'
        ]
    ]) ?>
</div>
