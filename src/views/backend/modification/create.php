<?php
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'modificationForm',
    'enableAjaxSubmit' => true,
    'ajaxSubmitOptions' => [
        'pjaxContainer' => '#modificationsPjax',
        'pjaxReload' => true,
        'closeModal' => true,
    ],
]); ?>
    <div class="uk-modal-header">
        <h3><?= Yii::t('shop', 'Modification') ?></h3>
    </div>
    <div class="uk-modal-body">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <div class="uk-grid-small uk-child-width-1-2@m" uk-grid>
            <div>
                <?= $form->field($model->price, 'new')->textInput(['maxlength' => true]) ?>
            </div>
            <div>
                <?= $form->field($model->price, 'old')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="uk-margin-top uk-width-1-2@m">
            <?= $form->field($model, 'quantity')->textInput(['type' => 'number', 'min' => 0]) ?>
            <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="uk-modal-footer">
        <?= Html::submitButton(Yii::t('shop', 'Save'), [
            'class' => 'uk-button uk-button-primary',
            'action' => 'close'
        ]) ?>
        <?= Html::a(Yii::t('shop', 'Close'), '#', [
            'class' => 'uk-button uk-button-default uk-modal-close',
        ]) ?>
    </div>
<?php ActiveForm::end(); ?>
