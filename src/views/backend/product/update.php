<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\select2\Select2;
use exoo\joditeditor\JoditEditor;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Nom */
/* @var $form yii\widgets\ActiveForm */

$this->title = $product->name ?: Yii::t('shop', 'New');

?>
<?php $form = ActiveForm::begin([
    'id' => 'productForm',
    // 'action' => Url::to(['update', 'id' => $product->id]),
]); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h1 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h1>
        </div>
        <div>
            <?= Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $product->id], [
                'class' => 'uk-button uk-button-default',
                'uk-tooltip' => Yii::t('shop', 'Preview'),
            ]) ?>
            <?= Html::a('<i class="far fa-trash-alt fa-lg fa-fw"></i>', ['delete', 'id' => $product->id], [
                'class' => 'uk-button uk-button-danger',
                'uk-tooltip' => Yii::t('shop', 'Remove'),
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure you want to delete?'),
                ],
            ]) ?>
            <?= Html::submitButton(Yii::t('shop', 'Save'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?= Html::submitButton(Yii::t('shop', 'Save and close'), [
                'class' => 'uk-button uk-button-primary js-action',
                'action' => 'close',
            ]) ?>
            <?php Html::a('<i class="far fa-copy fa-lg fa-fw"></i>', ['copy', 'id' => $product->id], [
                'class' => 'uk-button uk-button-success',
                'uk-tooltip' => Yii::t('shop', 'Copy'),
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>

    <div class="uk-card uk-card-default uk-card-body uk-card-small">
        <div uk-grid>
            <div class="uk-width-auto@m">
                <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                    <li><a href="#"><?= Yii::t('shop', 'Main') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Warehouse') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Images') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Files') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Related products') ?></a></li>
                    <li><a href="#"><?= Yii::t('shop', 'Settings') ?></a></li>
                </ul>
            </div>
            <div class="uk-width-expand@m">
                <ul id="component-tab-left" class="uk-switcher">
                    <li>
                        <div class="uk-child-width-1-2@m" uk-grid>
                            <div>
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'brandId')->dropdownList($model->brandsList, [
                                    'prompt' => ''
                                ]) ?>
                                <?= $form->field($model->price, 'new')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model->price, 'old')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'modificationName')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div>
                                <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model->categories, 'main')->widget(Select2::className(), [
                                    'items' => $model->categories->getCategoriesList(),
                                    'options' => [
                                        'class' => 'js-category',
                                        'prompt' => '',
                                    ],
                                    'clientOptions' => [
                                        'allowClear' => true,
                                    ]
                                ]) ?>
                                <?= $form->field($model->categories, 'others')->widget(Select2::className(), [
                                    'items' => $model->categories->getCategoriesList(),
                                    'options' => [
                                        'multiple' => 'multiple',
                                        'class' => 'js-categories',
                                    ],
                                ]) ?>
                                <?php
                                // $form->field($model, 'groupIds')->widget(Select2::className(), [
                                //     'items' => $model->groupsList,
                                //     'options' => [
                                //         'multiple' => 'multiple',
                                //         'class' => 'js-group',
                                //     ],
                                // ])
                                ?>
                                <?= $form->field($model->tags, 'existing')->widget(Select2::className(), [
                                    'items' => $model->tags->getTagsList(),
                                    'options' => ['multiple' => 'multiple'],
                                ]) ?>
                                <?= $form->field($model->tags, 'textNew')->textInput() ?>
                                <?= $form->field($model, 'status')->dropDownList($product->getStatusList()) ?>
                            </div>
                        </div>
                        <?= $form->field($model, 'shortDescription')->textarea(['rows' => 7]) ?>
                        <?= $form->field($model, 'description')->widget(JoditEditor::className(), [
                            'clientOptions' => [
                                'height' => '400px',
                            ]
                        ]) ?>
                    </li>
                    <li><?= $this->render('form/_warehouse', [
                        'form' => $form,
                        'model' => $model,
                    ]) ?></li>
                    <li class="characteristics"><?= $this->render('form/_characteristics', [
                        'form' => $form,
                        'model' => $model,
                        'product' => $product,
                    ]) ?></li>
                    <li><?= $this->render('form/_images', [
                        'form' => $form,
                        'product' => $product,
                    ]) ?></li>
                    <li><?= $this->render('form/_files', [
                        'form' => $form,
                        'product' => $product,
                    ]) ?></li>
                    <li><?= $this->render('form/_related', [
                        'form' => $form,
                        'product' => $product,
                        'relatedProvider' => $relatedProvider,
                    ]) ?></li>
                    <li><?= $this->render('form/_settings', [
                        'form' => $form,
                        'model' => $model,
                    ]) ?></li>
                </ul>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$js = <<<JS
$(document).one('click', '.js-action', function() {
    var form = $('#productForm');
    if (!form.find('.has-error').length) {
        $('<input>').attr({
            type: 'hidden',
            name: 'action',
            value: $(this).attr('action')
        }).appendTo(form);
    }
});
JS;
$this->registerJs($js);
