<?php

use exoo\shop\entities\Product\Product;
use exoo\shop\helpers\PriceHelper;
use exoo\shop\helpers\ImageHelper;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\status\StatusColumn;
use exoo\position\PositionColumn;
use yii\helpers\Html;
use exoo\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\forms\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Products');
Url::remember(Yii::$app->request->url, 'product-index');
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'rowOptions' => function (Product $model) {
        if (Yii::$app->settings->get('shop', 'allowStock')) {
            if ($model->quantity <= 0) {
                return ['style' => ['border-left' => '10px solid #f5607c']];
            }
        }
    },
    'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
    'pjax' => true,
    'buttons' => [
        Html::a(Yii::t('shop', 'Add'), ['create'], [
            'class' => 'uk-button uk-button-primary',
            'data-pjax' => 0,
        ]),
        Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
            'class' => 'uk-button uk-button-danger uk-hidden',
            'data' => [
                'method' => 'post',
                'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
            ],
            'ex-selected' => true
        ]),
    ],
    'columns' => [
        ['class' => CheckboxColumn::class],

        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'image',
            'format' => 'raw',
            'value' => function(Product $model) {
                if ($model->mainImage) {
                    return Html::img($model->mainImage->url('small'));
                }
                return ImageHelper::default();
            },
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($model) {
                return Html::a(Html::encode($model->name), ['view', 'id' => $model->id], ['data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'category_id',
            'filter' => $searchModel->categoriesList(),
            'value' => 'category.name',
        ],
        [
            'attribute' => 'price_new',
            'value' => function (Product $model) {
                return PriceHelper::format($model->price_new);
            },
            'contentOptions' => ['class' => 'uk-text-nowrap uk-text-right'],
            'headerOptions' => ['class' => 'uk-text-center'],
        ],
        [
            'attribute' => 'quantity',
            'contentOptions' => ['class' => 'uk-text-right'],
            'headerOptions' => ['class' => 'uk-text-center'],
        ],
        [
            'attribute' => 'sku',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'class' => StatusColumn::class,
        ],
        ['class' => PositionColumn::class],
        ['class' => ActionColumn::class],
    ],
]); ?>
