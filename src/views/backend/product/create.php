<?php

use yii\helpers\Html;
use exoo\uikit\ActiveForm;

/* @var $this yii\web\View */
/* @var $model exoo\shop\models\Nom */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('shop', 'Product') . ': (' . Yii::t('shop', 'New') . ')';

?>

<?php $form = ActiveForm::begin(); ?>
    <div class="tm-sticky-subnav uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h1 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h1>
        </div>
        <div>
            <?= Html::submitButton(Yii::t('shop', 'Next'), [
                'class' => 'uk-button uk-button-primary',
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>
    <div class="uk-card uk-card-default uk-card-body uk-card-small">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'warehouseId')->dropdownList($model->warehousesList) ?>
        <?= $form->field($model, 'brandId')->dropdownList($model->brandsList, ['prompt' => '']) ?>
        <?= $form->field($model, 'unitId')->dropdownList($model->unitsList) ?>
    </div>
<?php ActiveForm::end(); ?>
