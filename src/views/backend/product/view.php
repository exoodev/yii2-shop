<?php
use yii\helpers\Html;

$this->title = Yii::t('shop', 'Product') . ': (' . $product->name . ')';
?>
<div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
    <div>
        <h1 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h1>
    </div>
    <div>
        <?= Html::a(Yii::t('shop', 'Update'), ['update', 'id' => $product->id], [
            'class' => 'uk-button uk-button-primary',
        ]) ?>
        <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->request->referrer, [
            'class' => 'uk-button uk-button-default',
        ]) ?>
    </div>
</div>
<div class="uk-card uk-card-default uk-card-body uk-card-small">
<div class="uk-grid-medium" uk-grid>
    <div class="uk-width-1-4@m">
        <?= $this->render('_view/slideshow', ['product' => $product]) ?>
    </div>
    <div class="uk-width-expand@m">
            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-auto@m">
                    <ul class="uk-tab-left" uk-tab="connect: #component-tab-left; animation: uk-animation-fade">
                        <li><a href="#"><?= Yii::t('shop', 'Main') ?></a></li>
                        <li><a href="#"><?= Yii::t('shop', 'Warehouse') ?></a></li>
                        <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
                        <li><a href="#"><?= Yii::t('shop', 'Files') ?></a></li>
                        <li><a href="#"><?= Yii::t('shop', 'Statistics') ?></a></li>
                    </ul>
                </div>
                <div class="uk-width-expand@m">
                    <ul id="component-tab-left" class="uk-switcher">
                        <li><?= $this->render('_view/main', ['product' => $product]) ?></li>
                        <li><?= $this->render('_view/warehouse', ['product' => $product]) ?></li>
                        <li><?= $this->render('_view/characteristics', ['product' => $product]) ?></li>
                        <li><?= $this->render('_view/files', ['product' => $product]) ?></li>
                        <li><?= $this->render('_view/statistics', ['product' => $product]) ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modifications" class="uk-margin">
    <h3><?= Yii::t('shop', 'Modifications') ?></h3>
    <?= $this->render('/modification/index', [
        'product' => $product,
        'modificationsProvider' => $modificationsProvider
    ]) ?>
</div>


<?php
$css = <<<CSS
.uk-table-small td,
.uk-table-small th {
    padding: 5px 12px;
}
CSS;
$this->registerCss($css);
