<?php

use exoo\storage\widgets\FileInput;
use yii\helpers\ArrayHelper;

echo $form
    ->field($product, 'filename')
    ->widget(FileInput::className(), [
        'multiple' => true,
        'url' => ['images'],
        'relation' => 'images',
        'sortable' => true,
        'clientOptions' => ArrayHelper::getValue($this->context->module->params, 'product.images.widgetOptions', [])
    ])
    ->label(false);
