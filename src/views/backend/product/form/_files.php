<?php

use exoo\storage\widgets\FileInput;
use yii\helpers\ArrayHelper;

echo $form
    ->field($product, 'filename')
    ->widget(FileInput::className(), [
        'multiple' => true,
        'url' => ['files'],
        'relation' => 'files',
        'sortable' => true,
        'clientOptions' => ArrayHelper::getValue($this->context->module->params, 'product.files.widgetOptions', [])
    ])
    ->label(false);
