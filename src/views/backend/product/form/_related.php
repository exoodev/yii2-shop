<?php
use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\shop\entities\Product\RelatedAssignment;
use exoo\shop\helpers\PriceHelper;
use exoo\shop\helpers\ImageHelper;

/* @var $this yii\web\View */
/* @var $product exoo\shop\entities\Product\Product */
/* @var $form yii\widgets\ActiveForm */

// echo \yii\helpers\VarDumper::dumpAsString($relatedProvider->models, 10, true); die();

?>
<?= GridView::widget([
    'id' => 'relatedGrid',
    'dataProvider' => $relatedProvider,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'rowOptions' => function (RelatedAssignment $model) {
        if (Yii::$app->settings->get('shop', 'allowStock')) {
            if ($model->product->quantity <= 0) {
                return ['style' => ['border-left' => '10px solid #f5607c']];
            }
        }
    },
    'summary' => false,
    'pjax' => true,
    'pjaxOptions' => ['id' => 'relatedPjax'],
    'columns' => array_filter([
        ['class' => CheckboxColumn::class],

        [
            'attribute' => 'related_id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],

        [
            'attribute' => 'product.image',
            'format' => 'raw',
            'value' => function(RelatedAssignment $model) {
                if ($model->product->mainImage) {
                    return Html::img($model->product->mainImage->url('small'));
                }
                return ImageHelper::default();
            },
        ],
        [
            'attribute' => 'product.name',
            'format' => 'raw',
            'value' => function(RelatedAssignment $model) {
                $result = [
                    Html::a(Html::encode($model->product->name), ['view', 'id' => $model->related_id], ['data-pjax' => 0])
                ];

                if ($model->product->category) {
                    $result[] = Html::tag('div', Yii::t('shop', 'Category') . ': ' . Html::encode($model->product->category->name), [
                        'class' => 'uk-text-muted uk-text-small'
                    ]);
                }

                if ($model->product->sku) {
                    $result[] = Html::tag('div', Yii::t('shop', 'SKU') . ': ' . Html::encode($model->product->sku), [
                        'class' => 'uk-text-muted uk-text-small'
                    ]);
                }

                return implode(PHP_EOL, $result);
            },
        ],
        Yii::$app->settings->get('shop', 'allowStock') ? [
            'attribute' => 'product.quantity',
            'headerOptions' => ['class' => 'uk-text-nowrap'],
        ] : false,
        [
            'attribute' => 'product.price_new',
            'value' => function (RelatedAssignment $model) {
                return PriceHelper::format($model->product->price_new);
            },
            'contentOptions' => ['class' => 'uk-text-nowrap'],
        ],

        [
            'class' => PositionColumn::class,
            'controller' => 'related',
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{delete}',
            'controller' => 'related',
        ],
    ]),
]); ?>

<div class="uk-margin-top">
    <?= Html::a(Yii::t('shop', 'Add'), ['related/products', 'id' => $product->id], [
        'class' => 'uk-button uk-button-primary',
        'ex' => [
            'modal' => true,
            'modalOptions' => [
                'large' => true,
                'overflow' => true,
                // 'full' => true
            ],
            'pjax-container' => '#relatedPjax',
        ],
    ]) ?>
    <?= Html::a(Yii::t('shop', 'Delete'), ['related/batch-delete', 'product_id' => $product->id], [
        'class' => 'uk-button uk-button-danger uk-hidden',
        'ex' => [
            'ajax' => true,
            'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
            'selected' => '#relatedGrid',
            'pjax-container' => '#relatedPjax',
        ]
    ]) ?>
</div>
