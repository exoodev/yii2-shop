<?php

use yii\helpers\Html;

?>

<div class="uk-width-1-2@m">
    <?= $form->field($model, 'warehouseId')->dropdownList($model->warehousesList) ?>
    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
        <div><?= $form->field($model->quantity, 'quantity')->textInput(['type' => 'number', 'min' => 0]) ?></div>
        <div><?= $form->field($model, 'unitId')->dropdownList($model->unitsList) ?></div>
    </div>
    <?= $form->field($model, 'weight')->textInput(['min' => 0]) ?>
    <div class="uk-margin">
        <?= Html::activeLabel($model, 'dimensions', ['class' => 'uk-form-label']) ?>
        <div class="uk-child-width-1-3@m uk-grid-small" uk-grid>
            <div><?= $form->field($model, 'length')->textInput(['min' => 0])->label(false) ?></div>
            <div><?= $form->field($model, 'width')->textInput(['min' => 0])->label(false) ?></div>
            <div><?= $form->field($model, 'height')->textInput(['min' => 0])->label(false) ?></div>
        </div>
    </div>
</div>
