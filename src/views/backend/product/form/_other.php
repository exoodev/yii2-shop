<?= $form->field($model, 'status_id')->dropdownList($model->statusesList) ?>
<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'price_old')->textInput(['maxlength' => true]) ?>
