<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'brand_id')->dropdownList($model->brandsList, ['prompt' => '']) ?>
<?= $form->field($model, 'type_id')->dropdownList($model->typesList) ?>
<?= $form->field($model, 'warehouse_id')->dropdownList($model->warehousesList) ?>
<?= $form->field($model, 'unit_id')->dropdownList($model->unitsList) ?>
