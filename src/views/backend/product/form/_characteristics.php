<?php
use yii\helpers\Json;
use yii\helpers\Html;
use yii\widgets\Pjax;

$ids = [];

if (empty($model->values)) {
    $types = [];
    if (Yii::$app->settings->get('shop', 'characteristicsToCategory')) {
        $types[] = Yii::t('shop', 'category');
    }
    if (Yii::$app->settings->get('shop', 'characteristicsToGroup')) {
        $types[] = Yii::t('shop', 'group');
    }
    if ($types) {
        $types = implode(count($types) == 2 ? ' ' . Yii::t('shop', 'or') . ' ' : '', $types);
    }
}
?>
<?php Pjax::begin(['id' => 'characteristicsPjax']); ?>
    <?php if (!empty($types)): ?>
        <div class="uk-alert uk-alert-primary">
            <p>
                <?= Html::encode(Yii::t('shop', 'Characteristics are tied to a {types}.', ['types' => $types])) ?><br>
                <?= Html::encode(Yii::t('shop', 'Specify the necessary characteristics in the {types}.', ['types' => $types])) ?>
            </p>
        </div>
    <?php endif; ?>
    <div class="uk-width-1-2@m">
        <?php foreach ($model->values as $i => $value) {
            $attribute = '[' . $i . ']value';
            $ids[] = Html::getInputId($value, $attribute);
            $class = 'uk-button uk-button-danger';
            if ($value->value === null) {
                $class .= ' uk-invisible';
            }
            $clearLink = Html::a(null, ['remove-value', 'id' => $product->id, 'characteristic_id' => $value->id], [
                'class' => $class,
                'ex-icon' => 'fas fa-times fa-lg',
                'uk-tooltip' => Yii::t('shop', 'Clear'),
                'ex-ajax' => true,
            ]);

            if ($variantsList = $value->variantsList()) {
                echo $form->field($value, $attribute)->dropDownList($variantsList, ['prompt' => ''])->group($clearLink);
            } else {
                echo $form->field($value, $attribute)->textInput()->group($clearLink);
            }
        } ?>
    </div>
    <?php if (Yii::$app->request->isPjax): ?>
        <?php foreach ($form->attributes as $i => $attribute) {
            if (in_array($attribute['id'], $ids)) {
                $attribute = Json::htmlEncode($attribute);
                $this->registerJs("jQuery('#productForm').yiiActiveForm('add', $attribute);");
            }
        } ?>
    <?php endif; ?>
<?php Pjax::end(); ?>

<?php if (Yii::$app->settings->get('shop', 'characteristicsToCategory')): ?>
<?php $js = <<<JS
var category = '.js-category',
    categories = '.js-categories';

$(document).on('change', category + ',' + categories, function() {
    reloadPjax();
});
function reloadPjax() {
    $.pjax.reload({
        data: {
            main: $(category).val(),
            others: $(categories).val(),
            charPjax: true
        },
        container: '#characteristicsPjax',
        replace: false
    });
}
JS;
$this->registerJs($js);
?>
<?php endif; ?>
