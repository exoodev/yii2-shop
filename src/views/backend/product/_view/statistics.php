<?php
use exoo\uikit\DetailView;
?>

<?= DetailView::widget([
    'model' => $product,
    'options' => [
        'class' => 'uk-table uk-table-small uk-table-divider'
    ],
    'attributes' => [
        'rating',
        'hits',
        'created_at:datetime',
        'updated_at:datetime',
        [
            'attribute' => 'created_by',
            'value' => $product->createdBy->username,
        ],
        [
            'attribute' => 'updated_by',
            'value' => $product->updatedBy->username,
        ],
        [
            'attribute' => 'status_id',
            'value' => $product->status,
        ],
        // 'admin_note',
        // 'short_description',
        // 'description',
    ],
]) ?>
