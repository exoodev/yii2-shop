<?php
use yii\helpers\Html;
?>

<div uk-slideshow="animation: pull; ratio: 1:1">

    <div class="uk-position-relative uk-visible-toggle">

        <div class="uk-slideshow-items uk-card uk-card-default" uk-lightbox>
            <?php foreach ($product->images as $index => $image) : ?>
                <a href="<?= $image->url('small') ?>" data-caption="<?= Html::encode($product->name) ?>">
                    <img src="<?= $image->url('medium') ?>" alt="<?= Html::encode($product->name) ?>" uk-cover>
                </a>
            <?php endforeach; ?>
        </div>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

    </div>

    <?php if (count($product->images) > 1) : ?>
        <div uk-slider="finite: true">
            <div class="uk-position-relative">

                <div class="uk-slider-container" style="padding:20px 0">
                    <ul class="uk-slider-items uk-thumbnav uk-flex uk-grid uk-child-width-1-5 uk-grid-small">
                        <?php foreach ($product->images as $index => $image) : ?>
                            <li uk-slideshow-item="<?= $index ?>">
                                <a href="#">
                                    <img class="uk-card uk-card-default uk-box-shadow-small uk-box-shadow-hover-medium" src="<?= $image->url('small') ?>" alt="<?= Html::encode($product->name) ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <a class="uk-position-center-left-out" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-right-out" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>
        </div>
    <?php endif; ?>

</div>