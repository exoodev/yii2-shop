<?php
use yii\helpers\Html;
use exoo\uikit\DetailView;
?>

<?= DetailView::widget([
    'model' => $product,
    'options' => [
        'class' => 'uk-table uk-table-small uk-table-divider'
    ],
    'attributes' => [
        'sku',
        [
            'attribute' => 'unit_id',
            'value' => $product->unit_id ? $product->unit->short_name : null,
        ],
        'quantity',
        'weight',
        'length',
        'width',
        'height',
        // 'admin_note',
        // 'short_description',
        // 'description',
    ],
]) ?>
