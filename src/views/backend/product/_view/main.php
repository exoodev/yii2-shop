<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\DetailView;
use exoo\shop\helpers\PriceHelper;

?>

<?= DetailView::widget([
    'model' => $product,
    'options' => [
        'class' => 'uk-table uk-table-small uk-table-divider'
    ],
    'attributes' => [
        [
            'attribute' => 'category_id',
            'format' => 'html',
            'value' => $product->category_id ? Html::a(Html::encode($product->category->name), ['index', 'NomSearch[category_id]' => $product->category_id]) : null,
        ],
        [
            'label' => Yii::t('shop', 'Other categories'),
            'value' => implode(', ', ArrayHelper::getColumn($product->categories, 'name'))
        ],
        [
            'attribute' => 'warehouse_id',
            'value' => ArrayHelper::getValue($product, 'warehouse.name'),
        ],
        [
            'attribute' => 'brand_id',
            'value' => ArrayHelper::getValue($product, 'brand.name'),
        ],
        'slug',
        [
            'attribute' => 'price',
            'value' => PriceHelper::format($product->price_new),
        ],
        [
            'attribute' => 'price_old',
            'value' => PriceHelper::format($product->price_old),
        ],
    ],
]) ?>
