<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$groups = ArrayHelper::index($product->values, null, 'characteristic.group.name');
?>

<table id="nomCharacteristics" class="uk-table uk-table-small uk-table-divider uk-table-middle uk-table-justify uk-table-responsive">
    <tbody>
        <?php foreach ($groups as $groupName => $values): ?>
            <?php if ($groupName): ?>
                <tr>
                    <td class="uk-text-bold" colspan="2"><?= Html::encode($groupName) ?></td>
                </tr>
            <?php endif; ?>
            <?php foreach ($values as $value): ?>
            <tr>
                <th>
                    <span><?= Html::encode($value->characteristic->name) ?></span>
                    <?php if ($value->characteristic->description): ?>
                        <span ratio="0.5" class="far fa-question-circle" uk-tooltip="<?= Html::encode($value->characteristic->description) ?>"></span>
                    <?php endif; ?>
                </th>
                <td><?= Html::encode($value->value) ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>
