<?php

use yii\helpers\Html;
use exoo\grid\GridView;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\position\PositionColumn;
use exoo\shop\entities\Characteristic\CharacteristicGroup;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\CharacteristicGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Characteristics groups');
?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a(Yii::t('shop', 'Create'), ['create'], [
                'class' => 'uk-button uk-button-primary',
                'data-pjax' => 0
            ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    ],
                'ex-selected' => true
            ]),
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => CheckboxColumn::class],

            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'uk-table-shrink'],
            ],
            [
                'attribute' => 'name',
                'format' => 'html',
                'value' => function(CharacteristicGroup $model) {
                    return Html::a(Html::encode($model->name), ['update', 'id' => $model->id]);
                },
            ],

            ['class' => PositionColumn::class],

            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}'
            ],
        ],
    ]) ?>
