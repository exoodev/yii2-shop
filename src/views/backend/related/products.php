<?php

use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use exoo\grid\GridView;
use exoo\position\PositionColumn;
use exoo\shop\entities\Product\Product;
use exoo\shop\helpers\ImageHelper;
use exoo\shop\helpers\PriceHelper;
use exoo\shop\helpers\ProductHelper;
use exoo\status\StatusColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\forms\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<?= GridView::widget([
    'id' => 'relatedProductsGrid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'containerOptions' => ['class' => 'uk-card uk-card-default'],
    'rowOptions' => function (Product $model) {
        if (Yii::$app->settings->get('shop', 'allowStock')) {
            if ($model->quantity <= 0) {
                return ['style' => ['border-left' => '10px solid #f5607c']];
            }
        }
    },
    'title' => Html::tag('h3', Html::encode(Yii::t('shop', 'Select products')), ['class' => 'uk-margin-remove']),
    'pjax' => true,
    'pjaxOptions' => [
        'id' => 'relatedProductsPjax',
        'enablePushState' => false,
    ],
    'columns' => [
        ['class' => CheckboxColumn::class],

        [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'attribute' => 'image',
            'format' => 'raw',
            'value' => function(Product $model) {
                if ($model->mainImage) {
                    return Html::img($model->mainImage->url('small'));
                }
                return ImageHelper::default();
            },
        ],
        // [
        //     'attribute' => 'name',
        //     'format' => 'raw',
        //     'value' => function($model) {
        //         return Html::a(Html::encode($model->name), ['view', 'id' => $model->id], ['data-pjax' => 0]);
        //     },
        // ],
        'name',
        [
            'attribute' => 'category_id',
            'filter' => $searchModel->categoriesList(),
            'value' => 'category.name',
        ],
        [
            'attribute' => 'price_new',
            'value' => function (Product $model) {
                return PriceHelper::format($model->price_new);
            },
            'contentOptions' => ['class' => 'uk-text-nowrap uk-text-right'],
            'headerOptions' => ['class' => 'uk-text-center'],
        ],
        // [
        //     'attribute' => 'quantity',
        //     'contentOptions' => ['class' => 'uk-text-right'],
        //     'headerOptions' => ['class' => 'uk-text-center'],
        // ],
        [
            'attribute' => 'sku',
            'headerOptions' => ['class' => 'uk-table-shrink'],
        ],
        [
            'class' => StatusColumn::class,
            'toggle' => false
        ],
    ],
]); ?>

<div class="uk-margin-top">
    <?= Html::a(Yii::t('shop', 'Select'), ['related/add', 'product_id' => $product->id], [
        'class' => 'uk-button uk-button-primary uk-hidden uk-modal-close',
        'ex' => [
            'ajax' => true,
            'selected' => '#relatedProductsGrid',
            'pjax-container' => '#relatedPjax',
        ],
    ]) ?>
    <a class="uk-button uk-button-default uk-modal-close" href="#"><?= Yii::t('shop', 'Close') ?></a>
</div>
