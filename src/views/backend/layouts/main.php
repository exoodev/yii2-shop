<?php
use exoo\uikit\Nav;
?>

<?php $this->beginContent(($this->theme->basePath ?: '@app') . '/views/layouts/main.php'); ?>

<?= Nav::widget([
    'items' => $this->context->module->getMenu('subnav'),
    'type' => 'subnav',
    'options' => ['class' => 'uk-subnav-pill'],
]) ?>

<?= $content ?>

<?php $this->endContent(); ?>