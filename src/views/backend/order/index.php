<?php

use exoo\shop\entities\Order\Order;
use exoo\shop\helpers\OrderHelper;
use exoo\grid\ActionColumn;
use exoo\grid\CheckboxColumn;
use yii\helpers\Html;
use exoo\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel exoo\shop\models\backend\forms\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Orders');
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'containerOptions' => ['class' => 'uk-card uk-card-default'],
        'title' => Html::tag('h3', Html::encode($this->title), ['class' => 'uk-margin-remove']),
        'buttons' => [
            Html::a('Export', ['export'], [
                'class' => 'uk-button uk-button-primary',
                'data-method' => 'post', 'data-confirm' => 'Export?'
            ]),
            Html::a(Yii::t('shop', 'Delete'), ['batch-delete'], [
                'class' => 'uk-button uk-button-danger uk-hidden',
                'data' => [
                    'method' => 'post',
                    'confirm' => Yii::t('shop', 'Are you sure want to delete?'),
                    ],
                'ex-selected' => true
            ]),
        ],
        'columns' => [
            ['class' => CheckboxColumn::class],
            [
                'attribute' => 'id',
                'value' => function (Order $model) {
                    return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
                },
                'format' => 'raw',
            ],
            'created_at:datetime',
            [
                'attribute' => 'status',
                'filter' => $searchModel->statusList(),
                'value' => function (Order $model) {
                    return OrderHelper::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            ['class' => ActionColumn::class],
        ],
    ]); ?>
</div>
