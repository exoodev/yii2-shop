<?php

use exoo\shop\helpers\OrderHelper;
use exoo\shop\helpers\PriceHelper;
use yii\helpers\Html;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $order shop\entities\Shop\Order\Order */

$this->title = Yii::t('shop', 'Order') . ' ' . $order->id;
// $this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="uk-margin-bottom uk-flex uk-flex-between uk-flex-wrap uk-flex-middle" uk-margin>
        <div>
            <h3 class="uk-card-title uk-margin-remove"><?= Html::encode($this->title) ?></h3>
        </div>
        <div>
            <?= Html::a(Yii::t('shop', 'Update'), ['update', 'id' => $order->id], [
                'class' => 'uk-button uk-button-primary'
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Delete'), ['delete', 'id' => $order->id], [
                'class' => 'uk-button uk-button-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a(Yii::t('shop', 'Close'), Yii::$app->user->returnUrl, [
                'class' => 'uk-button uk-button-default',
            ]) ?>
        </div>
    </div>

    <div class="uk-grid-small" uk-grid uk-height-match="target: .uk-card">
        <div class="uk-width-expand@m">

            <div class="uk-card uk-card-default">
                <div class="uk-card-header">
                    <h3><?= Yii::t('shop', 'Details') ?></h3>
                </div>
                <div class="uk-card-body">
                    <?= DetailView::widget([
                        'model' => $order,
                        'attributes' => [
                            'id',
                            'created_at:datetime',
                            [
                                'attribute' => 'status',
                                'value' => OrderHelper::statusLabel($order->status),
                                'format' => 'raw',
                            ],
                            'created_by',
                            'delivery_method_name',
                            'deliveryData.index',
                            'deliveryData.address',
                            'cost',
                            'note:ntext',
                        ],
                    ]) ?>
                </div>
            </div>

        </div>
        <div class="uk-width-1-2@m">

            <div class="uk-card uk-card-default uk-margin">
                <div class="uk-card-header">
                    <h3><?= Yii::t('shop', 'History') ?></h3>
                </div>
                <div class="uk-card-body">
                    <div class="uk-overflow-auto">
                        <table class="uk-table uk-table-divider uk-table-small">
                            <thead>
                            <tr>
                                <th class="text-left">Date</th>
                                <th class="text-left">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($order->statuses as $status): ?>
                                <tr>
                                    <td class="text-left">
                                        <?= Yii::$app->formatter->asDatetime($status->created_at) ?>
                                    </td>
                                    <td class="text-left">
                                        <?= OrderHelper::statusLabel($status->value) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="uk-card uk-card-default uk-margin">
        <div class="uk-card-header">
            <h3><?= Yii::t('shop', 'Products') ?></h3>
        </div>
        <div class="uk-card-body">
            <div class="uk-overflow-auto">
                <table class="uk-table uk-table-divider uk-table-striped uk-table-small">
                    <thead>
                    <tr>
                        <th class="text-left">Product Name</th>
                        <th class="text-left">Model</th>
                        <th class="text-left">Quantity</th>
                        <th class="text-right">Unit Price</th>
                        <th class="text-right">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($order->items as $item): ?>
                        <tr>
                            <td class="text-left">
                                <?= Html::encode($item->product_sku) ?><br />
                                <?= Html::encode($item->product_name) ?>
                            </td>
                            <td class="text-left">
                                <?= Html::encode($item->modification_sku) ?><br />
                                <?= Html::encode($item->modification_name) ?>
                            </td>
                            <td class="text-left">
                                <?= $item->quantity ?>
                            </td>
                            <td class="text-right"><?= PriceHelper::format($item->price) ?></td>
                            <td class="text-right"><?= PriceHelper::format($item->getCost()) ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>




</div>
