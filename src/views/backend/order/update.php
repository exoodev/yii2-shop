<?php

/* @var $this yii\web\View */
/* @var $order shop\entities\Shop\Order\Order */
/* @var $model exoo\shop\models\backend\forms\Order\OrderEditForm */

use exoo\uikit\ActiveForm;
use yii\helpers\Html;

$this->title = 'Update Order: ' . $order->id;
// $this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $order->id, 'url' => ['view', 'id' => $order->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="order-update">

    <?php $form = ActiveForm::begin() ?>

    <div class="uk-card uk-card-default">
        <div class="uk-card-header">Customer</div>
        <div class="uk-card-body">
            <?= $form->field($model->customer, 'phone')->textInput() ?>
            <?= $form->field($model->customer, 'name')->textInput() ?>
        </div>
    </div>

    <div class="uk-card uk-card-default uk-margin">
        <div class="uk-card-header">Delivery</div>
        <div class="uk-card-body">
            <?= $form->field($model->delivery, 'method')->dropDownList($model->delivery->deliveryMethodsList(), ['prompt' => '--- Select ---']) ?>
            <?= $form->field($model->delivery, 'index')->textInput() ?>
            <?= $form->field($model->delivery, 'address')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="uk-card uk-card-default uk-margin">
        <div class="uk-card-header">Note</div>
        <div class="uk-card-body">
            <?= $form->field($model, 'note')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="uk-margin">
        <?= Html::submitButton('Save', ['class' => 'uk-button uk-button-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
