<?php

use exoo\shop\assets\frontend\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginContent(($this->theme->basePath ?: '@app') . '/views/layouts/main.php') ?>
<?= $content ?>
<?php $this->endContent() ?>
