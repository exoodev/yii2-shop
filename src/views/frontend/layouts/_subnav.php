<?php
use exoo\shop\widgets\frontend\Categories;
use exoo\shop\widgets\frontend\CartWidget;
use yii\helpers\Html;

?>

<?php $this->beginBlock('subnav') ?>
<div class="uk-inline">
    <button class="uk-button uk-button-default" type="button"><?= Yii::t('shop', 'Catalog') ?> <span class="uk-margin-left" uk-icon="triangle-down"></span></button>
    <div class="shop-catalog" uk-dropdown>
        <?= Categories::widget([
            'active' => $this->params['active_category'] ?? null,
            'options' => [
                'class' => 'uk-nav uk-dropdown-nav'
            ]
        ]) ?>
    </div>
</div>

<?= Html::beginForm(['/shop/catalog/search'], 'get', [
    'id' => 'shopSearch',
    'class' => 'uk-search uk-search-default uk-width-1-3@m',
]) ?>
    <div id="search" class="input-group">
        <span uk-search-icon></span>
        <input class="uk-search-input" type="search" name="text" value="" placeholder="Поиск товаров...">
    </div>
<?= Html::endForm() ?>

<div class="uk-align-right@m">
    <?= CartWidget::widget() ?>
</div>
<?php $this->endBlock() ?>
