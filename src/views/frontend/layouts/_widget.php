<?php

use exoo\shop\widgets\frontend\Categories;

/* @var $this \yii\web\View */
?>

<div class="category-widget uk-card uk-card-default uk-card-small uk-card-body">
    <?= Categories::widget([
        'active' => $this->params['active_category'] ?? null
    ]) ?>
</div>