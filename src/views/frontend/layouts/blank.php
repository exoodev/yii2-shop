<?php

/* @var $this \yii\web\View */
/* @var $content string */

?>
<?php $this->beginContent('@shop/views/frontend/layouts/main.php') ?>
<?= $content ?>
<?php $this->endContent() ?>
