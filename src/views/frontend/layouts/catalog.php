<?php

/* @var $this \yii\web\View */

?>

<?php $this->beginContent('@shop/views/frontend/layouts/main.php') ?>

<?php if (Yii::$app->settings->get('shop', 'showCategoryWidget')): ?>
    <?php if ($position = Yii::$app->settings->get('shop', 'positionCategoryWidget')): ?>

        <?php $this->beginBlock($position) ?>
            <?= $this->render('_widget') ?>
        <?php $this->endBlock() ?>
        <?= $content ?>

    <?php else: ?>

        <div class="uk-grid-medium" uk-grid>
            <div class="uk-width-auto@m">
                <?= $this->render('_widget') ?>
            </div>
            <div class="uk-width-expand@m">
                <?= $content ?>
            </div>
        </div>

    <?php endif; ?>
<?php else: ?>
    <?= $content ?>
<?php endif; ?>

<?php $this->endContent() ?>





