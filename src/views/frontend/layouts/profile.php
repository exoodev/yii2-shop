<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use exoo\uikit\Nav;

$items = [
    [
        'label' => Yii::t('shop', 'Profile'),
        'url' => ['profile/default/index'],
    ],
    [
        'label' => Yii::t('shop', 'Orders'),
        'url' => ['profile/order/index'],
    ],
    [
        'label' => Yii::t('shop', 'Wish List'),
        'url' => ['profile/wishlist/index'],
    ],
];

?>
<?php $this->beginContent('@exoo/shop/views/frontend/layouts/main.php') ?>

<div class="" uk-grid>
    <div class="uk-width-expand@m">
        <?= $content ?>
    </div>
    <div class="uk-width-1-4@m">
        <div class="uk-card uk-card-default uk-card-body">
            <?= Nav::widget(['items' => $items]) ?>
        </div>
    </div>
</div>

<?php $this->endContent() ?>
