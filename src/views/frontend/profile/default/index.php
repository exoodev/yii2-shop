<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('shop', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Hello!</p>
</div>
