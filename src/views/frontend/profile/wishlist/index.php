<?php

/* @var $this yii\web\View */

use exoo\shop\entities\Product\Product;
use exoo\shop\helpers\PriceHelper;
use exoo\grid\ActionColumn;
use exoo\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('shop', 'Wish List');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Profile'), 'url' => ['profile/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cabinet-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'value' => function (Product $model) {
                    return $model->mainImage ? Html::img($model->mainImage->url('small')) : null;
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 100px'],
            ],
            'id',
            [
                'attribute' => 'name',
                'value' => function (Product $model) {
                    return Html::a(Html::encode($model->name), ['/shop/catalog/product', 'id' => $model->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'price_new',
                'value' => function (Product $model) {
                    return PriceHelper::format($model->price_new);
                },
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{delete}',
            ],
        ],
    ]); ?>

</div>
