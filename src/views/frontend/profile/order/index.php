<?php

use exoo\shop\entities\Order\Order;
use exoo\shop\helpers\OrderHelper;
use yii\helpers\Html;
use exoo\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Orders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Profile'), 'url' => ['profile/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="uc-card uk-card-default uk-card-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Order $model) {
                        return Html::a(Html::encode($model->id), ['view', 'id' => $model->id]);
                    },
                    'format' => 'raw',
                ],
                'created_at:datetime',
                [
                    'attribute' => 'status',
                    'value' => function (Order $model) {
                        return OrderHelper::statusLabel($model->status);
                    },
                    'format' => 'raw',
                ],
            ],
        ]); ?>
    </div>
</div>
