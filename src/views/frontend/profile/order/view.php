<?php

use exoo\shop\helpers\OrderHelper;
use exoo\shop\helpers\PriceHelper;
use yii\helpers\Html;
use exoo\uikit\DetailView;

/* @var $this yii\web\View */
/* @var $order shop\entities\Shop\Order\Order */

$this->title = Yii::t('shop', 'Order') . ' ' . $order->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Profile'), 'url' => ['profile/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="uk-card uk-card-default uk-card-small">
        <div class="uk-card-header">
            <h3><?= Yii::t('shop', 'Order details') ?></h3>
        </div>
        <div class="uk-card-body">
            <?= DetailView::widget([
                'model' => $order,
                'attributes' => [
                    'id',
                    'created_at:datetime',
                    [
                        'attribute' => 'status',
                        'value' => OrderHelper::statusLabel($order->status),
                        'format' => 'raw',
                    ],
                    'delivery_method_name',
                    'deliveryData.index',
                    'deliveryData.address',
                    'cost',
                    'note:ntext',
                ],
            ]) ?>
        </div>
    </div>

    <div class="uk-card uk-card-default uk-card-small uk-margin">
        <div class="uk-card-header">
            <h3><?= Yii::t('shop', 'Order list') ?></h3>
        </div>
        <div class="uk-card-body">
            <table class="uk-table uk-table-small uk-table-responsive uk-table-divider">
                <thead>
                    <tr>
                        <th class="uk-table-expand"><?= Yii::t('shop', 'Product Name') ?></th>
                        <th class="uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Quantity') ?></th>
                        <th class="uk-text-right uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Unit Price') ?></th>
                        <th class="uk-text-right uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Total') ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($order->items as $item): ?>
                    <tr>
                        <td>
                            <?= Html::encode($item->product_name) ?>
                            <?php if ($item->modification_sku): ?>
                                <div class="uk-text-small uk-text-muted">
                                    <span><?= Yii::t('shop', 'SKU') ?>:</span>
                                    <span><?= Html::encode($item->modification_sku) ?></span>
                                </div>
                            <?php endif; ?>
                            <?php if ($item->modification_name): ?>
                                <div class="uk-text-small uk-text-muted">
                                    <span><?= Html::encode($item->modification_name) ?></span>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="uk-text-nowrap">
                            <span class="uk-hidden@m"><?= Yii::t('shop', 'Quantity') ?>:</span>
                            <span><?= $item->quantity ?></span>
                            <span><?= Html::encode('unit') ?></span>
                        </td>
                        <td class="uk-text-right@m uk-text-nowrap">
                            <span class="uk-hidden@m"><?= Yii::t('shop', 'Unit Price') ?>:</span>
                            <span><?= PriceHelper::format($item->price) ?></span>
                        </td>
                        <td class="uk-text-right@m uk-text-nowrap">
                            <span class="uk-hidden@m"><?= Yii::t('shop', 'Total') ?>:</span>
                            <span><?= PriceHelper::format($item->getCost()) ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php if ($order->canBePaid()): ?>
        <p>
            <?= Html::a('Pay via Robokassa', ['payment/robokassa/invoice', 'id' => $order->id], ['class' => 'uk-button uk-button-primary']) ?>
        </p>
    <?php endif; ?>

</div>
