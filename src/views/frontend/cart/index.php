<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use exoo\shop\helpers\PriceHelper;

$this->title = Yii::t('shop', 'Shopping Cart');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Catalog'), 'url' => ['/shop/catalog/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="uk-text-center"><?= Html::encode($this->title) ?></h1>
<?php if ($items = $cart->getItems()): ?>
    <?php Pjax::begin([
        'id' => 'cartPjax',
        'scrollTo' => false,
    ]); ?>
    <div class="uk-grid-small" uk-grid>
        <div class="uk-width-expand@m">
            <?= $this->render('index/item', ['cart' => $cart, 'items' => $items]) ?>
        </div>
        <div class="uk-width-1-4@m">
            <?= $this->render('index/total', ['cart' => $cart]) ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
<?php else: ?>
    <div class="uk-text-center uk-margin-large">
        <i class="fas fa-cart-arrow-down fa-6x uk-text-muted"></i>
        <div class="uk-h2"><?= Html::encode(Yii::t('shop', 'No products')) ?></div>
        <p><?= Yii::t('shop', 'To add products to the cart, {link}', ['link' => Html::a(Yii::t('shop', 'go to the catalog'), ['/shop/catalog/index'])]) ?></p>
    </div>
<?php endif; ?>

<?php
$js = <<<JS
$(document).on('change', '.js-quantity', function(e) {
    var form = $(this).closest('form');
    form.on('submit', function() {
        $.post(form.attr('action'), form.serialize())
            .done(function(res) {
                ex.reloadPjax(form);
                cart.update();
            });
        return false;
    });
    form.submit();
});
JS;
$this->registerJs($js);
