<?php
use yii\helpers\Html;
use exoo\shop\helpers\PriceHelper;
?>

<?php foreach ($items as $item): ?>
    <?php
    $product = $item->getProduct();
    $modification = $item->getModification();
    ?>
    <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin">

        <div uk-grid>
            <?php if ($product->mainImage): ?>
                <div class="uk-width-1-5@m">
                    <?= Html::a(
                        Html::img($product->mainImage->url('medium')),
                        ['/shop/catalog/product', 'slug' => $product->slug],
                        ['data-pjax' => 0]
                    ) ?>
                </div>
            <?php endif; ?>

            <div class="uk-width-expand@m">
                <p><?= Html::a(Html::encode($product->name), ['/shop/catalog/product', 'slug' => $product->slug], [
                    'class' => 'uk-link-heading',
                    'data-pjax' => 0
                ]) ?></p>
                <?php if (Yii::$app->settings->get('shop', 'siteShowSku')): ?>
                    <p><?= Html::encode($product->sku) ?></p>
                <?php endif; ?>

                <?php if ($modification): ?>
                    <?= Html::encode($product->modification_name) . ': ' . Html::encode($modification->name) ?>
                <?php endif; ?>
            </div>

            <div class="uk-width-1-4@m">

                <p class="uk-h5 uk-text-bold">
                    <span><?= PriceHelper::format($item->getCost(), false) ?></span>
                    <span class="uk-text-muted"><?= PriceHelper::currency($item->getCost()) ?></span>
                </p>

                <?= Html::beginForm(['quantity', 'id' => $item->getId()]) ?>
                    <div class="uk-child-width-expand@s uk-grid-small uk-flex uk-flex-middle" uk-grid>
                        <div>
                            <?= Html::textInput('quantity', $item->getQuantity(), [
                                'type' => 'number',
                                'class' => 'js-quantity',
                                'min' => 1,
                                'max' => $item->getMaxQuantity()
                            ]) ?>
                        </div>
                        <div>
                            <span><?= Html::encode($product->unit->short_name) ?></span>
                        </div>
                    </div>
                <?= Html::endForm() ?>

                <div class="uk-button-group uk-margin">
                    <?= Html::a(null, ['remove', 'id' => $item->getId()], [
                        'class' => 'uk-button uk-button-danger uk-button-small',
                        'uk-tooltip' => Yii::t('shop', 'Remove'),
                        'ex-icon' => 'far fa-trash-alt fa-lg',
                        'ex' => ['ajax' => true],
                    ]) ?>
                </div>

            </div>
        </div>

        <?php if (false): ?>
            <div class="uk-alert uk-alert-danger"><?php //Yii::t('shop', 'The selected quantity exceeds the stock') ?></div>
        <?php endif; ?>

    </div>
<?php endforeach; ?>
