<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \shop\forms\Shop\AddToCartForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\uikit\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'id' => 'addToCartForm',
    'action' => ['api/cart/add', 'id' => $product->id],
    'enableAjaxSubmit' => true,
    'ajaxSubmitOptions' => [
        'closeModal' => true,
    ],
    'enableAjaxValidation' => true,
    'validationUrl' => ['api/cart/validation', 'id' => $product->id],
]) ?>

    <div class="uk-modal-header">
        <h3><?= Yii::t('shop', 'Add') ?></h3>
    </div>
    <div class="uk-modal-body">
        <?php if ($modifications = $model->getModifications()): ?>
            <?= $form->field($model, 'modificationId')->dropDownList(ArrayHelper::map($modifications, 'id', 'name'), ['prompt' => '']) ?>
        <?php endif; ?>
        <?= $form->field($model, 'quantity')->textInput(['type' => 'number', 'class' => 'uk-form-width-small']) ?>
    </div>
    <div class="uk-modal-footer">
        <?= Html::submitButton(Yii::t('shop', 'Add to Cart'), [
            'class' => 'uk-button uk-button-primary',
            'action' => 'close'
        ]) ?>
        <?= Html::a(Yii::t('shop', 'Close'), '#', [
            'class' => 'uk-button uk-button-default uk-modal-close',
        ]) ?>
    </div>

<?php ActiveForm::end() ?>

<?php
$js = <<<JS
console.log(123);
JS;
$this->registerJs($js);
