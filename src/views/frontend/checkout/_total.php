<?php
use exoo\shop\helpers\PriceHelper;
use yii\helpers\Html;

$cost = $cart->getCost();
?>

<div class="uk-card uk-card-secondary uk-card-small">

    <div class="uk-card-header">
        <div class="uk-flex uk-flex-middle uk-flex-between">
            <div>
                <span class="uk-h5 uk-text-muted"><?= Yii::t('shop', 'Total for payment') ?>:</span>
            </div>
            <div>
                <span class="uk-h4"><?= PriceHelper::format($cost->getTotal(), false) ?></span>
                <span class="uk-h4 uk-text-muted"><?= PriceHelper::currency($cost->getTotal()) ?></span>
            </div>
        </div>
    </div>
    <div class="uk-card-body">
        <?php if ($discounts = $cost->getDiscounts()): ?>
            <?php foreach ($discounts as $discount): ?>

                <div class="uk-placeholder uk-padding-small">
                    <div class="uk-flex uk-flex-middle uk-flex-between uk-grid-small" uk-grid>
                        <div>
                            <span class="uk-label uk-label-success"><?= Html::encode($discount->getName()) ?></span>
                        </div>
                        <div>
                            <span>- <?= PriceHelper::format($discount->getValue()) ?></span>
                        </div>
                    </div>
                    <div class="uk-text-small uk-text-muted uk-margin-small-top"><?= Html::encode($discount->getDescription()) ?></div>
                </div>
            <?php endforeach; ?>
            <p><?= Yii::t('shop', 'Total without discount') ?>: <?= PriceHelper::format($cost->getOrigin()) ?></p>
        <?php endif; ?>

        <?php if ($weight = $cart->getWeight()): ?>
            <p><?= Yii::t('shop', 'Weight') ?>: <?= $weight ?></p>
        <?php endif; ?>
    </div>

</div>
