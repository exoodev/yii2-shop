<?php

/* @var $this yii\web\View */
/* @var $cart \shop\cart\Cart */
/* @var $model \shop\forms\Shop\Order\OrderForm */

use exoo\shop\helpers\PriceHelper;
use exoo\shop\helpers\WeightHelper;
use exoo\uikit\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('shop', 'Ordering');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Catalog'), 'url' => ['/shop/catalog/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Shopping Cart'), 'url' => ['/shop/cart/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="uk-grid-small" uk-grid>
    <div class="uk-width-expand@m">
        <?= $this->render('_products', ['cart' => $cart]) ?>

        <?php $form = ActiveForm::begin() ?>

        <div class="uk-card uk-card-default uk-card-body uk-margin">
            <h3 class="uk-card-title"><?= Yii::t('shop', 'Contact Information') ?></h3>
            <?= $form->field($model->customer, 'phone')->textInput() ?>
            <?= $form->field($model->customer, 'name')->textInput() ?>
        </div>

        <?php if (Yii::$app->settings->get('shop', 'deliveryStatus') && $methods = $model->delivery->deliveryMethodsList()): ?>
            <div class="uk-card uk-card-default uk-card-body uk-margin">
                <h3 class="uk-card-title"><?= Yii::t('shop', 'Delivery') ?></h3>
                <?= $form->field($model->delivery, 'method')->dropDownList($methods, ['prompt' => '']) ?>
                <?= $form->field($model->delivery, 'index')->textInput() ?>
                <?= $form->field($model->delivery, 'address')->textarea(['rows' => 3]) ?>
            </div>
        <?php endif; ?>

        <div class="uk-card uk-card-default uk-card-body uk-margin">
            <h3 class="uk-card-title"><?= Yii::t('shop', 'Note') ?></h3>
            <?= $form->field($model, 'note')->textarea(['rows' => 3]) ?>
        </div>

        <div class="uk-margin">
            <?= Html::submitButton(Yii::t('shop', 'Checkout'), ['class' => 'uk-button uk-button-primary uk-button-large']) ?>
        </div>

        <?php ActiveForm::end() ?>
    </div>
    <div class="uk-width-1-4@m">
        <?= $this->render('_total', ['cart' => $cart]) ?>
    </div>
</div>
