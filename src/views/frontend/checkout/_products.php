<?php
use yii\helpers\Html;
use exoo\shop\helpers\PriceHelper;
?>

<div class="uk-card uk-card-default uk-card-small">
    <div class="uk-card-header">
        <button class="uk-button uk-button-default uk-flex uk-flex-middle" type="button" uk-toggle="target: #productList; animation: uk-animation-fade">
            <span class="uk-margin-right" uk-icon="menu"></span>
            <span><?= Yii::t('shop', 'Order list') ?></span>
        </button>
    </div>
    <div id="productList" class="uk-card-body" hidden>
        <table class="uk-table uk-table-small uk-table-responsive uk-table-divider">
            <thead>
                <tr>
                    <th class="uk-table-expand"><?= Yii::t('shop', 'Product Name') ?></th>
                    <th class="uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Quantity') ?></th>
                    <th class="uk-text-right uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Unit Price') ?></th>
                    <th class="uk-text-right uk-table-shrink uk-text-nowrap"><?= Yii::t('shop', 'Total') ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($cart->getItems() as $item): ?>
                <?php
                $product = $item->getProduct();
                $modification = $item->getModification();
                ?>
                <tr>
                    <td>
                        <?= Html::a(Html::encode($product->name), ['/shop/catalog/product', 'slug' => $product->slug]) ?>
                        <?php if ($modification): ?>
                            <div class="uk-text-small uk-text-muted">
                                <span><?= Html::encode($product->modification_name) ?>:</span>
                                <span><?= Html::encode($modification->name) ?></span>
                            </div>
                        <?php endif; ?>
                    </td>
                    <td class="uk-text-nowrap">
                        <span class="uk-hidden@m"><?= Yii::t('shop', 'Quantity') ?>:</span>
                        <span><?= $item->getQuantity() ?></span>
                        <span><?= Html::encode($product->unit->short_name) ?></span>
                    </td>
                    <td class="uk-text-right@m uk-text-nowrap">
                        <span class="uk-hidden@m"><?= Yii::t('shop', 'Unit Price') ?>:</span>
                        <span><?= PriceHelper::format($item->getPrice()) ?></span>
                    </td>
                    <td class="uk-text-right@m uk-text-nowrap">
                        <span class="uk-hidden@m"><?= Yii::t('shop', 'Total') ?>:</span>
                        <span><?= PriceHelper::format($item->getCost()) ?></span>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
