<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $tag shop\entities\Shop\Tag */

use yii\helpers\Html;

$this->title = Yii::t('shop', 'Products with tag') . ' ' . $tag->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Catalog'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $tag->name;
?>

<h1><?= Yii::t('shop', 'Products with tag') ?> &laquo;<?= Html::encode($tag->name) ?>&raquo;</h1>

<hr />

<?= $this->render('_category/products', [
    'dataProvider' => $dataProvider
]) ?>
