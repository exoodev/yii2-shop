<?php

use yii\helpers\Html;

$this->title = Yii::$app->settings->get('shop', 'titleCatalog');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->settings->get('shop', 'showTitleCatalog')): ?>
    <h1><?= Html::encode($this->title) ?></h1>
<?php endif; ?>

<?= $this->render('_category/subcategories', [
    'category' => $category
]) ?>

<?php if ($dataProvider->getCount()): ?>
    <?= $this->render('_category/products', [
        'dataProvider' => $dataProvider
    ]) ?>
<?php endif; ?>
