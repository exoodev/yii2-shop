<?php

use yii\helpers\Html;


if ($product->values) {
    $type = Yii::$app->settings->get('shop', 'valuesInListProducts');
    $result = [];

    if ($type == 'string') {
        foreach ($product->values as $item) {
            $result[] = Html::encode($item->characteristic->name) . ': ' . Html::encode($item->value);
        }
        echo Html::tag('div', implode(' | ', $result));
    } elseif ($type == 'list') {
        foreach ($product->values as $item) {
            $result[] = '<li>' . Html::encode($item->characteristic->name) . ': <span>' . Html::encode($item->value) . '</span></li>';
        }
        echo Html::tag('ul', implode("\n", $result), ['class' => 'uk-list']);
    }
}
