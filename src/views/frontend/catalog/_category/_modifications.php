<?php
use yii\helpers\Html;
use exoo\shop\widgets\frontend\Modifications;
?>

<?php if ($modifications = $product->modifications): ?>
    <div class="uk-margin-small-top">
        <?= Modifications::widget([
            'items' => $modifications,
            'name' => 'modification',
            'productSlug' => $product->slug,
        ]) ?>
        <?php foreach ($modifications as $modification): ?>
            <?php Html::a(
                Html::encode($modification->name),
                ['product', 'slug' => $product->slug, 'mId' => $modification->id],
                [
                    'class' => 'uk-button uk-button-default uk-button-small',
                    'data-pjax' => 0
            ]) ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
