<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use exoo\shop\helpers\ShopHelper;

$columns = ArrayHelper::getValue($this->context->module->params, 'category.columns');
$classes = ShopHelper::getGridClasses($columns);
?>

<?php if ($children = $category->children(1)->all()): ?>
    <div class="<?= $classes ?> uk-grid-medium shop-category-items" uk-grid uk-height-match="target: .uk-card">
        <?php foreach ($children as $category): ?>
        <div>
            <a class="uk-link-heading" href="<?= Html::encode(Url::to(['/shop/catalog/category', 'id' => $category->id])) ?>">
                
                <div class="uk-card uk-card-default uk-card-small uk-card-hover shop-category-item">
                    <?php if ($category->mainImage): ?>
                        <div class="uk-card-media-top shop-category-item-media">
                            <img src="<?= $category->mainImage->url('large') ?>" alt="<?= Html::encode($category->name) ?>">
                        </div>
                    <?php endif; ?>
                    <div class="uk-card-body shop-category-item-title">
                        <h3 class="uk-card-title"><?= Html::encode($category->name) ?></h3>
                    </div>
                </div>

            </a>
        </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php
$js = <<<JS
$(window).on('load resize', function() {
    var mobile = $(window).width() <= 960
    var card = $('.shop-category-item')
    var media = card.find('.shop-category-item-media')
    var body = card.find('.shop-category-item-title')

    if (mobile) {
        card.attr('uk-grid', '')
        media.removeClass('uk-card-media-top')
            .addClass('uk-card-media-left uk-cover-container uk-width-1-3')
        body.addClass('uk-width-expand')
    } else {
        card.removeAttr('uk-grid')
            .removeClass('uk-grid uk-grid-stack')
        media.removeClass('uk-card-media-left uk-cover-container uk-width-1-3')
            .addClass('uk-card-media-top')
        body.removeClass('uk-width-expand')
    }
    
    card
        .toggleClass('uk-grid-collapse', mobile)
        // .prop('disabled', mobile)
})

JS;
// $this->registerJs($js);
