<?php

use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use exoo\shop\helpers\ShopHelper;

$mode = Yii::$app->request->cookies->getValue('view-mode', 'block');
$columns = ArrayHelper::getValue(
    $this->context->module->params,
    'product.columns',
    ['xl' => 4, 'l' => 3, 'm' => 2]
);
?>

<?php Pjax::begin(['id' => 'catalogPjax']) ?>
<div class="uk-flex-middle uk-flex-between uk-margin-top uk-margin-bottom" uk-grid>
    <div>
        <select id="sortInput" class="uk-select uk-form-width-medium" onchange="location = this.value;">
            <?php
            $values = [
                '' => Yii::t('shop', 'Sort By'),
                'name' => Yii::t('shop', 'By name (A - Z)'),
                '-name' => Yii::t('shop', 'By name (Z - A)'),
                'price' => Yii::t('shop', 'Ascending price'),
                '-price' => Yii::t('shop', 'Descending price'),
                '-rating' => Yii::t('shop', 'Ascending rating'),
                'rating' => Yii::t('shop', 'Descending rating'),
            ];
            $current = Yii::$app->request->get('sort');
            ?>
            <?php foreach ($values as $value => $label) : ?>
                <option value="<?= Html::encode(Url::current(['sort' => $value ?: null])) ?>" <?php if ($current == $value) : ?>selected="selected" <?php endif; ?>><?= $label ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div>
        <div class="uk-inline">
            <a class="uk-icon-button" uk-icon="thumbnails" href="<?= Url::current(['mode' => 'block']) ?>"></a>
            <a class="uk-icon-button" uk-icon="menu" href="<?= Url::current(['mode' => 'list']) ?>"></a>
        </div>
    </div>
</div>

<?php
$columns = ArrayHelper::getValue($this->context->module->params, 'category.columns');
$classes = ShopHelper::getGridClasses($columns);

$itemsOptions = [
    'class' => 'product-items uk-grid-medium',
    'uk-grid' => true,
];

if ($mode !== null) {
    Html::addCssClass($itemsOptions, $mode);
    if ($mode == 'block') {
        Html::addCssClass($itemsOptions, ShopHelper::getGridClasses($columns));
    } elseif ($mode == 'list') {
        Html::addCssClass($itemsOptions, 'uk-child-width-1-1');
    }
}

$items = [];
foreach ($dataProvider->getModels() as $product) {
    $items[] = '<div>' . $this->render('_product', [
        'product' => $product,
        'mode' => $mode
    ]) . '</div>';
}

echo Html::tag('div', implode("\n", $items), $itemsOptions);
echo LinkPager::widget([
    'pagination' => $dataProvider->getPagination(),
]);
Pjax::end();

$columns = Json::htmlEncode($columns);
$this->registerJs("product.grid($columns)");
