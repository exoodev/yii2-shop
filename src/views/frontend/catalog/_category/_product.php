<?php

use yii\helpers\Html;
use exoo\shop\helpers\PriceHelper;
use yii\helpers\Url;

$grid = $mode == 'list' ? 'uk-width-1-4@m' : 'uk-width-1-1';
?>

<div class="product-item uk-box-shadow-hover-large" data-id="<?= $product->id ?>">
    <div class="uk-grid-collapse" uk-grid>

        <?php if ($product->mainImage) : ?>
            <div class="<?= $grid ?>">
                <div class="product-image">
                    <?= Html::a(
                        Html::img($product->mainImage->url('medium'), ['alt' => Html::encode($product->name)]),
                        ['product', 'slug' => $product->slug],
                        [
                            'data-pjax' => 0
                        ]
                    ) ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="uk-width-expand@m uk-flex uk-flex-column">
            <div class="product-info">
                <h3 class="product-name">
                    <?= Html::a(Html::encode($product->name), ['product', 'slug' => $product->slug], [
                        'class' => 'uk-link-heading',
                        'data-pjax' => 0
                    ]) ?>
                </h3>
                <?php if ($product->short_description) : ?>
                    <div class="product-description">
                        <?= Html::encode(strip_tags($product->short_description)) ?>
                    </div>
                <?php endif; ?>
                <?php if (
                    Yii::$app->settings->get('shop', 'showCharacteristicsInListProduct')
                    && $product->values
                ) : ?>
                    <div class="product-characteristics">
                        <?= $this->render('_characteristics', ['product' => $product]) ?>
                    </div>
                <?php endif; ?>
                <?php if (
                    Yii::$app->settings->get('shop', 'showModificationsInListProduct')
                    && $product->modifications
                ) : ?>
                    <div class="product-modifications">
                        <?= $this->render('_modifications', ['product' => $product]) ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="<?= $grid ?>">
            <div class="product-misc">
                <div class="product-price uk-flex uk-flex-middle">
                    <span class="product-price-new"><?= PriceHelper::format($product->price_new) ?></span>
                    <?php if ($product->price_old) : ?>
                        <span class="product-price-old uk-margin-left uk-text-muted">
                            <s><?= PriceHelper::format($product->price_old) ?></s>
                        </span>
                        <?php if ($percent = PriceHelper::percentDifference($product->price_new, $product->price_old)) : ?>
                            <span class="product-discount uk-label uk-label-danger uk-margin-left"><?= $percent ?></span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                <div class="product-actions uk-flex uk-flex-between uk-flex-middle">
                    <div>
                        <?php if (!Yii::$app->user->isGuest) : ?>
                            <a href="#" class="wishlist-toggle uk-margin-small-right uk-button uk-button-default" data-action="<?= $product->wishlist ? 'delete' : 'add' ?>" onclick="wishlist.toggle(event);">
                                <?php if ($product->wishlist) : ?>
                                    <i class="fa-heart fa-lg fas uk-text-danger"></i>
                                <?php else : ?>
                                    <i class="fa-heart fa-lg far uk-text-muted"></i>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>

                        <?php if (Yii::$app->settings->get('shop', 'compareStatus')) : ?>
                            <a class="uk-icon-link compare-add" href="#" onclick="compare.add(event);" data-url="<?= Url::to(['api/product/compare/add', 'id' => $product->id]) ?>" ex-icon="fas fa-tasks fa-lg" uk-tooltip="<?= Yii::t('shop', 'Add to compare') ?>" data-product="<?= $product->id ?>"></a>
                        <?php endif; ?>
                    </div>
                    <div>
                        <?php
                        if (empty($product->modifications)) {
                            $ex = [
                                'ajax' => true,
                                'success' => "cart.update(data);",
                            ];
                            $url = 'api/cart/add';
                        } else {
                            $ex = ['modal' => true];
                            $url = 'cart/add';
                        }
                        ?>
                        <?= Html::a(Yii::t('shop', 'To cart'), [$url, 'id' => $product->id], [
                            'class' => 'uk-button uk-button-primary cart-add',
                            'ex' => $ex,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
$add = Yii::t('shop', 'Add to favorites');
$delete = Yii::t('shop', 'Remove from favorites');
$js = <<<JS
var wishlist_tooltip = {add: '$add', delete: '$delete'}
JS;
$this->registerJs($js, \yii\web\View::POS_HEAD);
