<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $searchForm \shop\forms\Shop\Search\SearchForm */

use yii\helpers\Html;

$this->title = Yii::t('shop', 'Search');

$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php
echo $this->render('_search/form', [
    'searchForm' => $searchForm
]);
?>

<?= $this->render('_category/products', [
    'dataProvider' => $dataProvider
]) ?>
