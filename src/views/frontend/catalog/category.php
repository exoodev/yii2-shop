<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $category shop\entities\Shop\Category */

$this->title = $category->getSeoTitle();
$this->registerMetaTag(['name' => 'description', 'content' => $category->meta->description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $category->meta->keywords]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Catalog'), 'url' => ['index']];

foreach ($category->parents()->all() as $parent) {
    if (!$parent->isRoot()) {
        $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'id' => $parent->id]];
    }
}
$this->params['breadcrumbs'][] = $category->name;
$this->params['active_category'] = $category;
?>

<h1 class="category-heading"><?= Html::encode($category->name) ?></h1>
<div class="catalog-product">
    <?= $this->render('_category/subcategories', [
        'category' => $category
    ]) ?>

    <?php if ($category->description) : ?>
        <div class="uk-card uk-card-default uk-margin">
            <div class="uk-card-body">
                <?= Yii::$app->formatter->asHtml($category->description, $this->context->module->purifierOptions) ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($dataProvider->getCount()) : ?>
        <?= $this->render('_category/products', [
            'dataProvider' => $dataProvider
        ]) ?>
    <?php endif; ?>
</div>