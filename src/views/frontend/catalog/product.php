<?php
use yii\helpers\Url;
use yii\helpers\Html;
use exoo\shop\helpers\PriceHelper;
use yii\widgets\Pjax;

$this->title = $product->name;
if ($product->meta) {
    $this->registerMetaTag(['name' => 'description', 'content' => $product->meta->description]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $product->meta->keywords]);
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop', 'Catalog'), 'url' => ['index']];

if ($product->category) {
    foreach ($product->category->parents()->all() as $parent) {
        if (!$parent->isRoot()) {
            $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['category', 'id' => $parent->id]];
        }
    }
    $this->params['breadcrumbs'][] = ['label' => $product->category->name, 'url' => ['category', 'id' => $product->category->id]];
    $this->params['active_category'] = $product->category;
}
$this->params['breadcrumbs'][] = $product->name;

?>

<?php Pjax::begin([
    'id' => 'productPjax',
    'scrollTo' => false,
]); ?>
<div id="shop-product-view" data-product="<?= Html::encode($product->slug) ?>">
    <div uk-grid>

        <div class="uk-width-1-2@m">
            <?= $this->render('_product/slideshow', [
                'product' => $product,
                'productView' => $productView,
            ]) ?>
        </div>

        <div class="uk-width-expand@m">
            <h1 class="uk-h2"><?= Html::encode($this->title) ?></h1>

            <?php if (Yii::$app->settings->get('shop', 'siteShowSku')) : ?>
                <div class="uk-margin uk-text-muted"><?= Yii::t('shop', 'Product Code') ?>: <?= Html::encode($productView->sku) ?></div>
            <?php endif; ?>

            <?php if (Yii::$app->settings->get('shop', 'reviews')) : ?>
                <p>
                    <div><?= Yii::t('shop', 'Rating') ?>: <?= round($product->rating, 1) ?> <?= Yii::t('shop', 'from') ?> <?= $reviewForm->maxRating ?></div>
                    <div><?= Yii::t('shop', 'Votes') ?>: <?= $reviewDataProvider->getCount() ?></div>
                </p>
            <?php endif; ?>

            <?php if ($product->brand) : ?>
                <div class="uk-margin">
                    <?= HtmL::a(Html::encode($product->brand->name), ['brand', 'id' => $product->brand->id], [
                        'data-pjax' => 0
                    ]) ?>
                </div>
            <?php endif; ?>

            <?php if ($product->tags) : ?>
                <h3><?= Yii::t('shop', 'Tags') ?></h3>
                <?php foreach ($product->tags as $tag) : ?>
                    <?= HtmL::a(Html::encode($tag->name), ['tag', 'id' => $tag->id], [
                        'class' => 'uk-button uk-button-default uk-button-small',
                        'data-pjax' => 0,
                    ]) ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="uk-margin shop-product-price uk-flex uk-flex-middle">
                <span class="shop-product-price-new"><?= PriceHelper::format($productView->priceNew) ?></span>
                <?php if ($productView->priceOld) : ?>
                    <span class="shop-product-price-old uk-margin-left uk-text-muted">
                        <s><?= PriceHelper::format($productView->priceOld) ?></s>
                    </span>
                    <?php if ($percent = PriceHelper::percentDifference($productView->priceNew, $productView->priceOld)) : ?>
                        <span class="uk-label uk-label-danger uk-margin-left uk-animation-scale-down"><?= $percent ?></span>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <?= $this->render('_product/cart', [
                'product' => $product,
                'cartForm' => $cartForm,
                'productView' => $productView,
            ]) ?>
        </div>
    </div>

    <ul class="uk-margin" uk-tab>
        <?php if ($product->description) : ?>
            <li><a href="#"><?= Yii::t('shop', 'Description') ?></a></li>
        <?php endif; ?>
        <?php if ($productView->values) : ?>
            <li><a href="#"><?= Yii::t('shop', 'Characteristics') ?></a></li>
        <?php endif; ?>
        <?php if (Yii::$app->settings->get('shop', 'reviews')) : ?>
            <li><a href="#"><?= Yii::t('shop', 'Reviews') ?></a></li>
        <?php endif; ?>
    </ul>

    <ul class="uk-switcher">
        <?php if ($product->description) : ?>
            <li><?= Yii::$app->formatter->asHtml($product->description, $this->context->module->purifierOptions) ?></li>
        <?php endif; ?>
        <?php if ($productView->values) : ?>
            <li><?= $this->render('_product/characteristics', [
                    'product' => $product,
                    'characteristics' => $productView->values,
                ]) ?></li>
        <?php endif; ?>
        <?php if (Yii::$app->settings->get('shop', 'reviews')) : ?>
            <li><?= $this->render('_product/reviews', [
                    'product' => $product,
                    'reviewForm' => $reviewForm,
                    'reviewDataProvider' => $reviewDataProvider,
                ]) ?></li>
        <?php endif; ?>
    </ul>
</div>
<?php Pjax::end(); ?>

<?php
$url = Url::to(['product', 'slug' => $product->slug]);
$js = <<<JS
var modification = '.js-modification',
    quantity = '.js-quantity';

if ($(modification).length) {
    sessionStorage.removeItem('ex_q');
}
$(document).on('pjax:send', function() {
    var value = $(quantity).val();
    if (value) {
        sessionStorage.setItem('ex_q', value);
    }
});
$(document).on('pjax:complete', function() {
    $(quantity).val(sessionStorage.getItem('ex_q'));
});
$(document).on('change', modification, function() {
    var modId = $(this).val();
    if (modId) {
        $.pjax({
            url: "$url" + '&mId=' + modId,
            container: '#productPjax',
            scrollTo: false,
        });
    }
});
// $.get('/shop/api/product/index').done(function(res) {
//     console.log(res);
// });
JS;
$this->registerJs($js);

$css = <<<CSS
.shop-product-price-new {
    font-size: 1.8rem;
    line-height: 1.8;
}
CSS;
$this->registerCss($css);
