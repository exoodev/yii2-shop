<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$groups = ArrayHelper::index($characteristics, null, 'characteristic.group.name');
?>

<table id="shop-product-characteristics" class="uk-table uk-table-small uk-table-divider@m uk-table-middle uk-table-justify uk-table-responsive">
    <tbody>
        <?php foreach ($groups as $groupName => $characteristics): ?>
            <?php if ($groupName): ?>
                <tr>
                    <td class="uk-text-bold" colspan="2"><?= Html::encode($groupName) ?></td>
                </tr>
            <?php endif; ?>
            <?php foreach ($characteristics as $nc): ?>
            <tr>
                <th class="uk-width-1-2@m" uk-leader="media: @m">
                    <span><?= Html::encode($nc->characteristic->name) ?></span>
                    <?php if ($nc->characteristic->description): ?>
                        <span ratio="0.5" class="far fa-question-circle" uk-tooltip="<?= Html::encode($nc->characteristic->description) ?>"></span>
                    <?php endif; ?>
                </th>
                <td><?= Html::encode($nc->value) ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>

<?php
$css = <<<CSS
.uk-leader-fill {
    color: #ccc;
    font-weight: 100;
}
.uk-leader-fill span{
    color: #999;
    font-weight: 400;
}
.uk-table-small td, .uk-table-small th {
    padding: 5px 12px;
}
CSS;
$this->registerCss($css);
$js = <<<JS
$(window).on('resize', function () {
    var media = window.matchMedia('(max-width: 960px)').matches;
    $('#shop-product-characteristics').toggleClass('uk-table-divider', media);
});
JS;
$this->registerJs($js);
