<?php
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

$options = array_merge([
    'animation' => 'pull',
], ArrayHelper::getValue($this->context->module->params, 'product.images.slideshowOptions'));
?>

<div class="uk-animation-fade" uk-slideshow=<?= Json::htmlEncode($options) ?>>

    <div class="uk-position-relative uk-visible-toggle">

        <div class="uk-slideshow-items" uk-lightbox>
            <?php foreach ($productView->images as $index => $image): ?>
                <a href="<?= $image->url('large') ?>" data-caption="<?= Html::encode($product->name) ?>">
                    <img src="<?= $image->url('large') ?>" alt="<?= Html::encode($product->name) ?>" uk-cover>
                </a>
            <?php endforeach; ?>
        </div>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

    </div>

    <?php if (count($productView->images) > 1): ?>
        <div uk-slider="finite: true">
            <div class="uk-position-relative">

                <div class="uk-slider-container" style="padding:20px 0">
                    <ul class="uk-slider-items uk-thumbnav uk-flex uk-grid uk-grid-small">
                        <?php foreach ($productView->images as $index => $image): ?>
                            <li uk-slideshow-item="<?= $index ?>">
                                <a href="#">
                                    <img class="uk-card uk-card-default uk-box-shadow-small uk-box-shadow-hover-medium" src="<?= $image->url('small') ?>" alt="<?= Html::encode($product->name) ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

                <a class="uk-position-center-left-out" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                <a class="uk-position-center-right-out" href="#" uk-slidenav-next uk-slider-item="next"></a>

            </div>
        </div>
    <?php endif; ?>

</div>
