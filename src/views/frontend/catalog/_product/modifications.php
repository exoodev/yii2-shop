<?php
use yii\helpers\Html;
?>

<?php if ($modifications = $product->modifications): ?>
    <div class="uk-margin">
        <?php foreach ($modifications as $modification): ?>
            <?php $class = ($modificationId == $modification->id) ? 'primary' : 'default' ?>
            <?= Html::a(
                Html::encode($modification->name),
                ['product', 'slug' => $product->slug, 'modification_id' => $modification->id],
                ['class' => 'uk-button uk-button-small uk-button-' . $class]) ?>
        <?php endforeach; ?>
        <?php // Html::activeHiddenInput($form, 'modification'); ?>
    </div>
<?php endif; ?>
