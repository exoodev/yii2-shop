<?php
use exoo\widgets\Rating;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="uk-card uk-card-default uk-card-body uk-margin">
    <div class="uk-grid-divider" uk-grid>
        <div class="uk-width-1-4@m">
            <a href="#"><?= Html::encode($model->createdBy->username) ?></a>
            <p class="uk-text-nowrap"><?= Rating::widget([
                'name' => 'vote',
                'value'  => $model->vote,
                'totalStars' => $maxRating
            ]) ?></p>
            <p class="uk-text-small uk-text-muted"><?= Yii::$app->formatter->asDateTime($model->created_at) ?></p>
            <?php if ($model->created_by == Yii::$app->user->id): ?>
                <p>
                    <a class="uk-button uk-button-danger uk-button-small" href="<?= Url::to(['profile/review/delete', 'id' => $model->id]) ?>" data-method="post" data-confirm="<?= Yii::t('shop', 'Are you sure you want to delete the review?') ?>"><?= Yii::t('shop', 'Delete') ?></a>
                </p>
            <?php endif; ?>
        </div>
        <div class="uk-width-expand@m">
            <dl class="uk-description-list">
            <?php if (Yii::$app->settings->get('shop', 'plusMinusReview')): ?>
                <?php if ($model->plus): ?>
                    <dt><?= $model->getAttributeLabel('plus') ?></dt>
                    <dd><?= Html::encode($model->plus) ?></dd>
                <?php endif; ?>
                <?php if ($model->minus): ?>
                    <dt><?= $model->getAttributeLabel('minus') ?></dt>
                    <dd><?= Html::encode($model->minus) ?></dd>
                <?php endif; ?>
            <?php endif; ?>
                <dt><?= $model->getAttributeLabel('comment') ?></dt>
                <dd><?= Html::encode($model->comment) ?></dd>
            </dl>
        </div>
    </div>
</div>
