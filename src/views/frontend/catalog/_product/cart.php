<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use exoo\uikit\ActiveForm;
use exoo\shop\widgets\frontend\Modifications;

?>
<?php $form = ActiveForm::begin([
    'id' => 'addToCartForm',
    'action' => ['api/cart/add', 'id' => $product->id],
    'enableAjaxSubmit' => true,
    'enableAjaxValidation' => true,
    'validationUrl' => ['api/cart/validation', 'id' => $product->id],
]); ?>

    <?php if ($modifications = $cartForm->getModifications()): ?>
        <?php if (Yii::$app->settings->get('shop', 'modificationsInProductCard') == 'links'): ?>

            <?= $form->field($cartForm, 'modificationId')->widget(Modifications::class, [
                'items' => $modifications,
                'value' => $productView->modification ? $productView->modification->id : null,
                'productSlug' => $product->slug
            ])->label(Html::encode(StringHelper::mb_ucfirst($product->modification_name))) ?>

        <?php elseif (Yii::$app->settings->get('shop', 'modificationsInProductCard') == 'list'): ?>

            <?= $form->field($cartForm, 'modificationId')->dropDownList(ArrayHelper::map($modifications, 'id', 'name'), [
                'prompt' => Yii::t('shop', 'Select') . ' ' . Html::encode(mb_strtolower($product->modification_name)),
                'class' => 'js-modification',
            ])->label(Html::encode(StringHelper::mb_ucfirst($product->modification_name))) ?>

        <?php endif; ?>
    <?php endif; ?>

    <?php if (!Yii::$app->settings->get('shop', 'allowOrder') && Yii::$app->settings->get('shop', 'allowStock') && $productView->quantity == 0): ?>
        <div class="uk-alert uk-alert-danger"><?= Html::encode(Yii::t('shop', 'Are not available')) ?></div>
    <?php else: ?>
        <?= $form
            ->field($cartForm, 'quantity')->textInput([
                'type' => 'number',
                'min' => 1,
                // 'max' => $cartForm->maxQuantity,
                'class' => 'uk-width-1-5@s ex-unselectable js-quantity'
            ])
            ->group(Yii::$app->settings->get('shop', 'siteShowStock') ? Html::tag('div', Yii::t('shop', 'Stock') . ': ' . $productView->quantity . ' ' . Html::encode($product->unit->short_name), ['class' => 'uk-text-nowrap uk-margin-small-left']) : null) ?>

        <?php if (Yii::$app->settings->get('shop', 'allowOrder')): ?>
            <div class="uk-alert uk-alert-danger"><?= Html::encode(Yii::t('shop', 'Under the order')) ?></div>
        <?php endif; ?>

        <?=  Html::submitButton(Yii::t('shop', 'Add to Cart'), [
            'class' => 'uk-button uk-button-primary uk-button-large shop-add-cart',
        ]) ?>
    <?php endif; ?>

<?php ActiveForm::end(); ?>
