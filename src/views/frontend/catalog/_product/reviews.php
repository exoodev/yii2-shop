<?php
use yii\helpers\Url;
use yii\helpers\Html;
use exoo\uikit\ActiveForm;
use exoo\rating\Rating;
use exoo\uikit\ListView;

?>
<button class="uk-button uk-button-primary js-add-review" type="button"><?= Yii::t('shop', 'Write a review') ?></button>
<div class="uk-card uk-card-default uk-card-body js-review-form" style="display:none">
    <h3><?= Yii::t('shop', 'Review of the {name}', ['name' => $product->name]) ?></h3>
    <div class="uk-child-width-1-2@m" uk-grid>
        <div>
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="panel-panel-info">
                    <div class="panel-body">
                        <?= Yii::t('shop', 'Please {login} for writing a review.', [
                            'login' => Html::a(Yii::t('shop', 'log in'), ['/user/auth/login'])
                        ]) ?>
                    </div>
                </div>
            <?php elseif (Yii::$app->user->identity->canAddReview($product->id)): ?>

                <?php $form = ActiveForm::begin([
                    'id' => 'reviewForm',
                    'action' => Url::to(['/shop/profile/review/add', 'id' => $product->id]),
                ]) ?>
                    <?= $form->field($reviewForm, 'vote')->widget(Rating::class, [
                        'totalStars' => $reviewForm->maxRating
                    ]) ?>
                    <?php if (Yii::$app->settings->get('shop', 'plusMinusReview')): ?>
                        <?= $form->field($reviewForm, 'plus')->textarea(['rows' => 5]) ?>
                        <?= $form->field($reviewForm, 'minus')->textarea(['rows' => 5]) ?>
                    <?php endif; ?>
                    <?= $form->field($reviewForm, 'comment')->textarea(['rows' => 5]) ?>
                    <div class="uk-margin-top">
                        <?= Html::submitButton(Yii::t('shop', 'Send'), ['class' => 'uk-button uk-button-primary']) ?>
                        <button class="uk-button uk-button-default js-cancel-review" type="button"><?= Yii::t('shop', 'Cancel') ?></button>
                    </div>
                <?php ActiveForm::end() ?>

            <?php endif; ?>
        </div>
        <div>
            <?php if (Yii::$app->settings->get('shop', 'reviewsNote')): ?>
                <div class="uk-alert"><?= Yii::$app->settings->get('shop', 'reviewsNote') ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?= ListView::widget([
    'dataProvider' => $reviewDataProvider,
    'itemView' => '_reviewItem',
    'viewParams' => [
        'product' => $product,
        'maxRating' => $reviewForm->maxRating
    ],
    'summary' => false,
    'options' => ['class' => 'uk-margin']
]) ?>

<?php
$js = <<<JS
$('.js-add-review').on('click', function() {
    $('.js-review-form').slideDown('slow');
});
$('.js-cancel-review').on('click', function() {
    $('.js-review-form').slideUp('slow');
    $('.js-add-review').slideDown('slow');
});
JS;
$this->registerJs($js);
