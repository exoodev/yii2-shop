<?php
use exoo\uikit\ActiveForm;
use yii\helpers\Html;
?>

<div class="uk-card uk-card-default uk-card-small">
    <div class="uk-card-body">
        <?php $form = ActiveForm::begin(['action' => [''], 'method' => 'get']) ?>

        <div class="uk-child-width-1-3@m uk-grid-small" uk-grid>
            <div>
                <?= $form->field($searchForm, 'text')->textInput() ?>
            </div>
            <div>
                <?= $form->field($searchForm, 'category')->dropDownList($searchForm->categoriesList(), ['prompt' => '']) ?>
            </div>
            <div>
                <?= $form->field($searchForm, 'brand')->dropDownList($searchForm->brandsList(), ['prompt' => '']) ?>
            </div>
        </div>

        <?php foreach ($searchForm->values as $i => $value): ?>
            <?php if ($variants = $value->variantsList()): ?>
                <?= $form
                    ->field($value, '[' . $i . ']equal')
                    ->dropDownList($variants, ['prompt' => ''])
                    ->label(Html::encode($value->getCharacteristicName()))
                ?>
            <?php elseif ($value->isAttributeSafe('from') && $value->isAttributeSafe('to')): ?>
                <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                    <div>
                        <?= $form
                            ->field($value, '[' . $i . ']from')->textInput()
                            ->label(Html::encode($value->getCharacteristicName()))
                        ?>
                    </div>
                    <div>
                        <?= $form->field($value, '[' . $i . ']to')->textInput() ?>
                    </div>
                </div>
            <?php endif ?>
        <?php endforeach; ?>

        <div class="uk-margin-top">
            <?= Html::submitButton(Yii::t('shop', 'Search'), ['class' => 'uk-button uk-button-primary']) ?>
            <?= Html::a(Yii::t('shop', 'Clear'), [''], ['class' => 'uk-button uk-button-default']) ?>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>
