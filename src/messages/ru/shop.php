<?php
return [
    // common
    'Votes' => 'Голосов',
    '{model} is not found.' => '{model} не найден.',
    'All' => 'Все',
    'and' => 'и',
    'Basket' => 'Корзина',
    'Brand' => 'Производитель',
    'Brands' => 'Производители',
    'Buy' => 'Купить',
    'Cancel' => 'Отмена',
    'Cancelled by customer' => 'Отменен покупателем',
    'Cancelled' => 'Отменен',
    'Categories' => 'Категории',
    'Category' => 'Категория',
    'Choose a delivery method' => 'Выберите способ доставки',
    'Close' => 'Закрыть',
    'Completed' => 'Выполнен',
    'Cost' => 'Стоимость',
    'Currencies' => 'Валюты',
    'Currency' => 'Валюта',
    'Delivery method' => 'Способ доставки',
    'Delivery methods' => 'Способы доставки',
    'Delivery' => 'Доставка',
    'Description' => 'Описание',
    'Dimensions (LxWxH) in cm.' => 'Габариты (ДхШхВ) в см.',
    'Discount' => 'Скидка',
    'Discounts' => 'Скидки',
    'File' => 'Файл',
    'Files' => 'Файлы',
    'Group' => 'Группа',
    'group' => 'Категория',
    'Groups' => 'Группы',
    'Height in cm.' => 'Высота в см.',
    'HIT' => 'ХИТ',
    'In basket' => 'В корзине',
    'Length in cm.' => 'Длина в см.',
    'Max weight' => 'Максимальный вес',
    'Min weight' => 'Минимальный вес',
    'Name' => 'Название',
    'NEW' => 'НОВИНКА',
    'Note' => 'Примечание',
    'or' => 'или',
    'Order' => 'Заказ',
    'Orders' => 'Заказы',
    'Paid' => 'Оплачен',
    'Payment method' => 'Способ оплаты',
    'Payment methods' => 'Способы оплаты',
    'Percent' => 'Процент',
    'Price old' => 'Старая цена',
    'Price' => 'Цена',
    'Product Code' => 'Код товара',
    'Product' => 'Товар',
    'Products' => 'Товары',
    'Quantity' => 'Количество',
    'Rating' => 'Рейтинг',
    'Remaining' => 'Остаток',
    'Remove' => 'Удалить',
    'Review of the {name}' => 'Отзыв о {name}',
    'Review' => 'Отзыв',
    'Reviews' => 'Отзывы',
    'Save' => 'Сохранить',
    'Saved' => 'Сохранено',
    'Search' => 'Поиск',
    'Select a Payment Method' => 'Выберите способ оплаты',
    'Choose' => 'Выберите',
    'Select' => 'Выбрать',
    'Select products' => 'Выберите товары',
    'Send' => 'Отправить',
    'Sent' => 'Отправлен',
    'Shop' => 'Магазин',
    'Short description' => 'Краткое описание',
    'SKU' => 'Артикул',
    'Statistics' => 'Статистика',
    'Status' => 'Статус',
    'Stock' => 'Остаток',
    'To order' => 'Заказать',
    'Under the order' => 'Под заказ',
    'Width in cm.' => 'Ширина в см.',
    'Write a review' => 'Написать отзыв',
    'Tags' => 'Метки',
    'To cart' => 'В корзину',
    'The selected quantity exceeds the stock' => 'Выбранное количество превышает остаток',
    'Shopping Cart' => 'Корзина',
    'Continue Shopping' => 'Продолжить покупки',
    'Checkout' => 'Оформить заказ',
    'Total' => 'Итого',
    'Total without discount' => 'Итого без скидки',
    'Total for payment' => 'Итого к оплате',
    'Catalog' => 'Каталог',
    'Product catalog' => 'Каталог товаров',
    'Product Name' => 'Наименование товара',
    'Model' => 'Модель',
    'Unit Price' => 'Цена за ед.',
    'Customer' => 'Покупатель',
    'First Name' => 'Имя',
    'Last Name' => 'Фамилия',
    'Phone' => 'Телефон',
    'Method' => 'Метод',
    'Post code' => 'Индекс',
    'Address' => 'Адрес',
    'Wish List' => 'Список пожеланий',
    'Order details' => 'Детали заказа',
    'Profile' => 'Профиль',
    'Message' => 'Сообщение',
    'Vote' => 'Оценка',
    'Are you sure you want to delete?' => 'Вы действительно хотите удалить?',
    'Are you sure you want to delete the review?' => 'Вы действительно хотите удалить отзыв?',
    'from' => 'из',
    'Advantages' => 'Достоинства',
    'Disadvantages' => 'Недостатки',
    'Comment' => 'Комментарий',
    'Related products' => 'Сопутствующие товары',
    'Qty' => 'Кол-во',
    'Compare products' => 'Сравнение товаров',
    'No products' => 'Нет товаров',


    // backend
    '{name} already exists.' => '{name} уже существует.',
    '{name} is not found.' => '{name} не найдена.',
    'Сharacteristics View Type' => 'Вид представления характеристик',
    'Active' => 'Активен',
    'Add product' => 'Добавить товар',
    'Add subcategory' => 'Добавить подкатегорию',
    'Add subgroup' => 'Добавить подгруппу',
    'Add' => 'Добавить',
    'Adding' => 'Добавление',
    'Additional categories' => 'Дополнительные категории',
    'Admin note' => 'Примечание администратора',
    'All list items from a new line' => 'Все пункты списка с новой строки',
    'Allow product order with zero remaining' => 'Разрешить заказ товара с нулевым остатком',
    'Allow stock' => 'Учет остатков',
    'Are not available' => 'Нет в наличии',
    'Are you sure want to delete?' => 'Вы действительно хотите удалить?',
    'Are you sure you want to delete the {item}?' => 'Вы уверены, что хотите удалить {item}?',
    'Assignment' => 'Назначение',
    'Bind the characteristics to the category' => 'Привязать характеристики к категории',
    'Bind the characteristics to the group' => 'Привязать характеристики к группе',
    'Buttons' => 'Кнопки',
    'category' => 'категории',
    'Characteristic' => 'Характеристика',
    'Characteristics are tied to a {types}.' => 'Характеристики привязаны к {types}.',
    'Characteristics group' => 'Группа характиристики',
    'Characteristics groups' => 'Группы характеристик',
    'Characteristics' => 'Характеристики',
    'Clear' => 'Очистить',
    'Code' => 'Код',
    'Comma' => 'Запятая',
    'Copy' => 'Копировать',
    'Create' => 'Создать',
    'Created at' => 'Создано',
    'Created by' => 'Создал',
    'Decimal separator' => 'Разделитель десятичных',
    'Default value' => 'Значение по умолчанию',
    'Delete' => 'Удалить',
    'Deleted' => 'Удалено',
    'Directories' => 'Справочники',
    'Does not appear on the site' => 'Не отображается на сайте',
    'Dot' => 'Точка',
    'Draft' => 'Черновик',
    'From date' => 'Дата начала',
    'From time' => 'Время начала',
    'group' => 'группе',
    'Hits' => 'Хиты',
    'Icon' => 'Иконка',
    'Images' => 'Изображения',
    'Inactive' => 'Неактивен',
    'Insert Item ID in SKU' => 'Вставлять ID номенклатуры в артикул',
    'Integer' => 'Целое число',
    'List' => 'Список',
    'Main category' => 'Основания категория',
    'Main' => 'Основное',
    'Manage shop' => 'Управление магазином',
    'Maximum fraction digits' => 'Максимальное число цифр после запятой',
    'Meta' => 'Мета',
    'Minimum fraction digits' => 'Минимальное число цифр после запятой',
    'Moderation' => 'На модерации',
    'Modification name' => 'Название модификации',
    'Modification' => 'Модификация',
    'Modifications View Type' => 'Вид представления модификаций',
    'Modifications' => 'Модификации',
    'New tags' => 'Новые теги',
    'New' => 'Новый',
    'Next' => 'Далее',
    'Non integer' => 'Нецелое число',
    'Note in reviews' => 'Примечание в отзывах',
    'Number of leading zeros' => 'Количество ведущих нулей',
    'Parent category' => 'Родительская категория',
    'Parent group' => 'Родительская группа',
    'Preview' => 'Предпросмотр',
    'Required' => 'Обязательный',
    'Reset' => 'Сброс',
    'Save and add' => 'Сохранить и дополнить',
    'Save and close' => 'Сохранить и закрыть',
    'Save and create' => 'Сохранить и создать',
    'Service' => 'Услуга',
    'Settings' => 'Настройки',
    'Short name' => 'Краткое название',
    'Show characteristics' => 'Показывать характеристики',
    'Show modifications' => 'Показывать модификации',
    'Show SKU' => 'Показывать артикул',
    'Show stock' => 'Показывать остаток',
    'SKU prefix' => 'Префикс артикула',
    'Slug' => 'Псевдоним',
    'Sorting' => 'Сортировка',
    'Space' => 'Пробел',
    'Specify the necessary characteristics in the {types}.' => 'Укажите необходимые характеристики в {types}.',
    'String' => 'Строка',
    'Tag' => 'Тег',
    'Thousands separator' => 'Разделитель тысяч',
    'To date' => 'Дата окончания',
    'To time' => 'Время окончания',
    'Transaction limit' => 'Лимит транзакции',
    'Type' => 'Тип',
    'Unit' => 'Единица измерения',
    'Units' => 'Единицы измерения',
    'Update' => 'Изменить',
    'Updated at' => 'Изменено',
    'Updated by' => 'Изменил',
    'Use SKU prefix' => 'Использовать префикс артикула',
    'Values list' => 'Список значений',
    'View type' => 'Вид представления',
    'Warehouse' => 'Склад',
    'Warehouses' => 'Склады',
    'Weight in kg.' => 'Вес в кг.',
    'Hide products that are not in stock' => 'Скрывать товары отсутствующие на складе',
    'Failed Payment' => 'Неудачный платеж',
    'Sort By' => 'Сортировать по',
    'By name (A - Z)' => 'По названию (A - Z)',
    'By name (Z - A)' => 'По названию (Z - A)',
    'Ascending price' => 'По возрастанию цены',
    'Descending price' => 'По убыванию цены',
    'Ascending rating' => 'По возрастанию рейтинга',
    'Descending rating' => 'По убыванию рейтинга',
    'Author' => 'Автор',
    'Add a review can only bought the product' => 'Добавить отзыв можно только купив товар',
    'Maximum rating' => 'Максимальная оценка',
    'Advantages and disadvantages of the product in the review' => 'Преимущества и недостатки товара в отзыве',
    'Activate' => 'Активировано',
    'Compare' => 'Сравнение',
    'Title catalog' => 'Заголовок каталога',
    'Show catalog title' => 'Показать заголовок каталога',
    'Color' => 'Цвет',
    'Show category widget' => 'Показать виджет категории',
    'Category widget position' => 'Позиция виджета категории',
    'Unable to remove category with products.' => 'Невозможно удалить категорию с товарами.',
    'Suppliers' => 'Поставщики',
    'Supplier' => 'Поставщик',
    'INN' => 'ИНН',
    'Representative' => 'Представитель',
    'Website' => 'Вебсайт',
    'All characteristics' => 'Все характеристики',

    // frontend
    'Review successfully added and is waiting for moderation' => 'Отзыв успешно добавлен и ожидает модерации',
    'log in' => 'Авторизуйтесь',
    'Please {login} for writing a review.' => '{login}, чтобы оставить отзыв.',
    'Product added to cart' => 'Товар добавлен в корзину',
    'This goods is only available in {quantity} {unit}' => 'Данный товар доступен только в количестве {quantity} {unit}',
    'Ordering' => 'Оформление заказа',
    'Order list' => 'Состав заказа',
    'Contact Information' => 'Контактная информация',
    'Order successfully paid' => 'Заказ успешно оплачен',
    'Product added to favorites' => 'Товар добавлен в избранное',
    'Product removed from favorites' => 'Товар удален из избранного',
    'Add to favorites' => 'Добавить в избранное',
    'Remove from favorites' => 'Удалить из избранного',
    'Add to Cart' => 'Добавить в корзину',
    'Remove from cart' => 'Удалить из корзины',
    'Add to compare' => 'Добавить в сравнение',
    'Remove from compare' => 'Удалить из сравнения',
    'Products with tag' => 'Товары с тегом',
    'To add products to the cart, {link}' => 'Для добавления товаров в корзину, {link}',
    'go to the catalog' => 'перейдите в каталог',
];
