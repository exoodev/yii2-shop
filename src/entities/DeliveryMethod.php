<?php

namespace exoo\shop\entities;

use Yii;
use exoo\shop\entities\queries\DeliveryMethodQuery;
use yii\db\ActiveRecord;
use exoo\status\StatusTrait;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property int $id
 * @property string $name
 * @property int $cost
 * @property int $min_weight
 * @property int $max_weight
 * @property int status
 * @property int $position
 */
class DeliveryMethod extends ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function create($name, $cost, $minWeight, $maxWeight, $status): self
    {
        $method = new static();
        $method->name = $name;
        $method->cost = $cost;
        $method->min_weight = $minWeight;
        $method->max_weight = $maxWeight;
        $method->status = $status;
        return $method;
    }

    public function edit($name, $cost, $minWeight, $maxWeight, $status): void
    {
        $this->name = $name;
        $this->cost = $cost;
        $this->min_weight = $minWeight;
        $this->max_weight = $maxWeight;
        $this->status = $status;
    }

    public function isAvailableForWeight($weight): bool
    {
        return (!$this->min_weight || $this->min_weight <= $weight) && (!$this->max_weight || $weight <= $this->max_weight);
    }

    public static function tableName(): string
    {
        return '{{%shop_delivery_method}}';
    }

    public static function find(): DeliveryMethodQuery
    {
        return new DeliveryMethodQuery(static::class);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'min_weight' => Yii::t('shop', 'Min weight'),
            'max_weight' => Yii::t('shop', 'Max weight'),
            'cost' => Yii::t('shop', 'Cost'),
            'status' => Yii::t('shop', 'Status'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }

    public static function getStatuses()
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('shop', 'Inactive'),
                'class' => 'danger',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('shop', 'Active'),
                'class' => 'success',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
            ],
        ];
    }
}
