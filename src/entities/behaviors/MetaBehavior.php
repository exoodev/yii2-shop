<?php

namespace exoo\shop\entities\behaviors;

use exoo\shop\entities\Meta;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class MetaBehavior extends Behavior
{
    public $attribute = 'meta';

    public function events(): array
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'onAfterFind',
        ];
    }

    public function onAfterFind(): void
    {
        $meta = $this->owner->{$this->attribute};
        $this->owner->{$this->attribute} = new Meta(
            ArrayHelper::getValue($meta, 'title'),
            ArrayHelper::getValue($meta, 'description'),
            ArrayHelper::getValue($meta, 'keywords')
        );
    }
}
