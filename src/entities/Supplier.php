<?php

namespace exoo\shop\entities;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%shop_supplier}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $rep
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $website
 * @property int $inn
 * @property string $note
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_supplier}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'address', 'website'], 'required'],
            [['description', 'note'], 'string'],
            [['inn', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'rep', 'address', 'email', 'website'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'rep' => Yii::t('shop', 'Representative'),
            'phone' => Yii::t('shop', 'Phone'),
            'address' => Yii::t('shop', 'Address'),
            'email' => Yii::t('shop', 'Email'),
            'website' => Yii::t('shop', 'Website'),
            'inn' => Yii::t('shop', 'INN'),
            'note' => Yii::t('shop', 'Note'),
            'created_at' => Yii::t('shop', 'Created at'),
            'updated_at' => Yii::t('shop', 'Updated at'),
            'created_by' => Yii::t('shop', 'Created by'),
            'updated_by' => Yii::t('shop', 'Updated by'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }
}
