<?php

namespace exoo\shop\entities;

use Yii;
use yii\db\ActiveRecord;
use exoo\shop\entities\Product\Product;
use yii2tech\ar\position\PositionBehavior;

/**
 * This is the model class for table "{{%shop_unit}}".
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property int $position
 *
 * @property Product[] $products
 */
class Unit extends ActiveRecord
{
    public static function create($name, $shortName): self
    {
        $model = new static();
        $model->name = $name;
        $model->short_name = $shortName;
        return $model;
    }

    public function edit($name, $shortName): void
    {
        $this->name = $name;
        $this->short_name = $shortName;
    }

    ##########################

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%shop_unit}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'short_name'], 'required'],
            [['position'], 'integer'],
            [['name', 'short_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'short_name' => Yii::t('shop', 'Short name'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
            ],
        ];
    }

    public function getProducts(): ActiveQuery
    {
        return $this->hasMany(Product::className(), ['unit_id' => 'id']);
    }
}
