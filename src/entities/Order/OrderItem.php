<?php

namespace exoo\shop\entities\Order;

use exoo\shop\entities\Product\Product;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $modification_id
 * @property string $product_name
 * @property string $product_sku
 * @property string $modification_name
 * @property string $modification_code
 * @property int $price
 * @property int $quantity
 * @property int $created_at
 * @property int $created_by
 */
class OrderItem extends ActiveRecord
{
    public static function create(Product $product, $modificationId, $price, $quantity)
    {
        $item = new static();
        $item->product_id = $product->id;
        $item->product_name = $product->name;
        $item->product_sku = $product->sku;
        if ($modificationId) {
            $modification = $product->getModification($modificationId);
            $item->modification_id = $modification->id;
            $item->modification_name = $modification->name;
            $item->modification_sku = $modification->sku;
        }
        $item->price = $price;
        $item->quantity = $quantity;
        $item->created_at = time();
        $item->created_by = Yii::$app->user->id;
        return $item;
    }

    public function getCost(): int
    {
        return $this->price * $this->quantity;
    }

    public static function tableName(): string
    {
        return '{{%shop_order_item}}';
    }
}
