<?php

namespace exoo\shop\entities;

use Yii;
use yii\db\ActiveRecord;
use exoo\shop\entities\Product\Product;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;

/**
 * This is the model class for table "{{%shop_warehouse}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $data
 *
 * @property Product[] $products
 */
class Warehouse extends ActiveRecord
{
    public static function create($name, $description): self
    {
        $warehouse = new static();
        $warehouse->name = $name;
        $warehouse->description = $description;
        return $warehouse;
    }

    public function edit($name, $description): void
    {
        $this->name = $name;
        $this->description = $description;
    }

    ##########################

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%shop_warehouse}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'dynamicAttributeBehavior' => [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [],
            ],
        ];
    }

    public function getProducts(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['warehouse_id' => 'id']);
    }
}
