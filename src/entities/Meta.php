<?php

namespace exoo\shop\entities;

class Meta
{
    public $title;
    public $description;
    public $keywords;

    public function __construct($title, $description, $keywords)
    {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
    }

    public function toArray(): array
    {
        $values = [];
        foreach (['title', 'description', 'keywords'] as $attribute) {
            if (!empty($this->{$attribute})) {
                $values[$attribute] = $this->{$attribute};
            }
        }
        return $values;
    }
}
