<?php

namespace exoo\shop\entities\Product\Modification;

use Yii;
use yii\db\ActiveRecord;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property integer $id
 * @property int $modification_id
 * @property string $filename
 * @property integer $position
 *
 */
class Image extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%shop_modification_image}}';
    }

    public function behaviors(): array
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
                'groupAttributes' => [
                    'modification_id'
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return parent::find()->orderBy([
            'position' => SORT_ASC,
            'modification_id' => SORT_ASC,
        ]);
    }

    public function url($size = null)
    {
        return Yii::$app->fileStorage->getFileUrl('productImages', $this->filename, $size);
    }
}
