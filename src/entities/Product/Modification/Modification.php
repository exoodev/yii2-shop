<?php

namespace exoo\shop\entities\Product\Modification;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii2tech\ar\position\PositionBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\entities\Product\Modification\Image;
use exoo\shop\entities\Product\Modification\Value;
use exoo\storage\behaviors\FileBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use exoo\shop\Module;

/**
 * @property int $id
 * @property string $sku
 * @property string $name
 * @property string $price_new
 * @property string $price_old
 * @property int $weight
 * @property int $length
 * @property int $width
 * @property int $height
 * @property int $color
 * @property int $quantity
 * @property Value[] $values
 * @property Image[] $images
 */
class Modification extends ActiveRecord
{
    public static function create($sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity): self
    {
        $modification = new static();
        $modification->sku = $sku;
        $modification->name = $name;
        $modification->price_new = $priceNew;
        $modification->price_old = $priceOld;
        $modification->weight = $weight;
        $modification->length = $length;
        $modification->width = $width;
        $modification->height = $height;
        $modification->color = $color;
        $modification->quantity = $quantity;
        return $modification;
    }

    public function edit($sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity): void
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price_new = $priceNew;
        $this->price_old = $priceOld;
        $this->weight = $weight;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->color = $color;
        $this->quantity = $quantity;
    }

    public function checkout($quantity): void
    {
        if ($quantity > $this->quantity) {
            throw new \DomainException('Only ' . $this->quantity . ' items are available.');
        }
        $this->quantity -= $quantity;
    }

    public function isIdEqualTo($id)
    {
        return $this->id == $id;
    }

    public function isSkuEqualTo($sku)
    {
        return $this->sku === $sku;
    }

    public static function tableName(): string
    {
        return '{{%shop_modification}}';
    }

    public function behaviors(): array
    {
        $module = Module::getInstance();
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
            ],
            'dynamicAttribute' => [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [],
            ],
            'images' => [
                'class' => FileBehavior::class,
                'relation' => 'images',
                'relationAttribute' => 'modification_id',
                'bucketName' => 'productImages',
                'bucketData' => $module->buckets['productImages'],
                'transformImage' => ArrayHelper::getValue($module->params, 'product.images.transform'),
            ],
            'relations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => ['values', 'images'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'sku' => Yii::t('shop', 'SKU'),
            'price_new' => Yii::t('shop', 'Price'),
            'price_old' => Yii::t('shop', 'Price old'),
            'quantity' => Yii::t('shop', 'Quantity'),
            'weight' => Yii::t('shop', 'Weight in kg.'),
            'length' => Yii::t('shop', 'Length'),
            'width' => Yii::t('shop', 'Width'),
            'height' => Yii::t('shop', 'Height'),
            'color' => Yii::t('shop', 'Color'),
            'dimensions' => Yii::t('shop', 'Dimensions (LxWxH) in cm.'),
        ];
    }

    public function setValue($id, $value): void
    {
        if ($value) {
            $values = $this->values;
            foreach ($values as $i => $val) {
                if ($val->isForCharacteristic($id)) {
                    $val->change($value);
                    $this->values = $values;
                    return;
                }
            }
            $values[] = Value::create($id, $value);
            $this->values = $values;
        }
    }

    public function getValue($id): Value
    {
        $values = $this->values;
        foreach ($values as $val) {
            if ($val->isForCharacteristic($id)) {
                return $val;
            }
        }
        return Value::blank($id);
    }

    public function revokeValues(): void
    {
        $this->values = [];
    }

    public function getValues(): ActiveQuery
    {
        return $this->hasMany(Value::class, ['modification_id' => 'id']);
    }

    public function getImages(): ActiveQuery
    {
        return $this->hasMany(Image::class, ['modification_id' => 'id']);
    }

    public function getMainImage()
    {
        if ($this->images) {
            return $this->images[0];
        }
    }
}
