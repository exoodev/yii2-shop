<?php

namespace exoo\shop\entities\Product\events;

use exoo\shop\entities\Product\Product;

class ProductAppearedInStock
{
    public $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }
}
