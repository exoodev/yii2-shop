<?php

namespace exoo\shop\entities\Product\queries;

use Yii;
use exoo\shop\entities\Product\Review;
use yii\db\ActiveQuery;

class ReviewQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            'status' => Review::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param null $alias
     * @return $this
     */
    public function moderation($alias = null)
    {
        return $this->andWhere([
            'status' => Review::STATUS_MODERATION,
        ]);
    }
}
