<?php

namespace exoo\shop\entities\Product\queries;

use Yii;
use exoo\shop\entities\Product\Product;
use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => Product::STATUS_ACTIVE,
        ]);
    }

    public function inStock()
    {
        if (Yii::$app->settings->get('shop', 'hideProductsAreNotInStock')) {
            return $this
                ->joinWith('modifications m')
                ->andWhere(['and', ['or', ['>', 'p.quantity', 0], ['>', 'm.quantity', 0]]]);
        }
        return $this;
    }
}
