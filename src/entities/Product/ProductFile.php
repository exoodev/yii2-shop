<?php

namespace exoo\shop\entities\Product;

use Yii;
use yii\db\ActiveRecord;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property integer $id
 * @property int $product_id
 * @property string $filename
 * @property integer $position
 *
 */
class ProductFile extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%shop_product_file}}';
    }

    public function behaviors(): array
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::className(),
                'groupAttributes' => [
                    'product_id'
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return parent::find()->orderBy([
            'position' => SORT_ASC,
            'product_id' => SORT_ASC,
        ]);
    }

    public function url()
    {
        return Yii::$app->fileStorage->getFileUrl('productFiles', $this->filename);
    }
}
