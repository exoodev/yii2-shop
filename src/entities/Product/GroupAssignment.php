<?php

namespace exoo\shop\entities\Product;

use yii\db\ActiveRecord;

/**
 * @property integer $product_id;
 * @property integer $group_id;
 */
class GroupAssignment extends ActiveRecord
{
    public static function create($groupId): self
    {
        $assignment = new static();
        $assignment->group_id = $groupId;
        return $assignment;
    }

    public function isForGroup($id): bool
    {
        return $this->group_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%shop_product_group}}';
    }
}
