<?php

namespace exoo\shop\entities\Product;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use exoo\status\StatusTrait;
use exoo\shop\entities\Product\queries\ReviewQuery;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\User;

/**
 * @property int $id
 * @property int $product_id
 * @property string $plus
 * @property string $minus
 * @property string $comment
 * @property int $vote
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property bool $status
 */
class Review extends ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_MODERATION = 2;

    public static function create(int $productId, int $vote, string $plus, string $minus, string $comment): self
    {
        $review = new static();
        $review->product_id = $productId;
        $review->vote = $vote;
        $review->plus = $plus;
        $review->minus = $minus;
        $review->comment = $comment;
        $review->status = self::STATUS_MODERATION;
        return $review;
    }

    public function edit(int $vote, string $plus, string $minus, string $comment, int $status): void
    {
        $this->vote = $vote;
        $this->plus = $plus;
        $this->minus = $minus;
        $this->comment = $comment;
        $this->status = $status;
    }

    public function activate(): void
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function inactivate(): void
    {
        $this->status = self::STATUS_INACTIVE;
    }

    public function moderation(): void
    {
        $this->status = self::STATUS_MODERATION;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function getRating(): int
    {
        return $this->vote;
    }

    public function isIdEqualTo($id): bool
    {
        return $this->id == $id;
    }

    public static function tableName(): string
    {
        return '{{%shop_review}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'product_id' => Yii::t('shop', 'Product'),
            'vote' => Yii::t('shop', 'Vote'),
            'plus' => Yii::t('shop', 'Advantages'),
            'minus' => Yii::t('shop', 'Disadvantages'),
            'comment' => Yii::t('shop', 'Comment'),
            'status' => Yii::t('shop', 'Status'),
            'created_at' => Yii::t('shop', 'Created at'),
            'updated_at' => Yii::t('shop', 'Updated at'),
            'created_by' => Yii::t('shop', 'Author'),
            'updated_by' => Yii::t('shop', 'Updated by'),
        ];
    }

    public function behaviors(): array
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('shop', 'Inactive'),
                'class' => 'danger',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('shop', 'Active'),
                'class' => 'success',
            ],
            [
                'id' => self::STATUS_MODERATION,
                'name' => Yii::t('shop', 'Moderation'),
                'class' => 'warning',
            ],
        ];
    }

    public function getProduct(): ActiveQuery
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    public static function find(): ReviewQuery
    {
        return new ReviewQuery(static::class);
    }
}
