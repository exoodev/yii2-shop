<?php

namespace exoo\shop\entities\Product;

use exoo\status\StatusTrait;
use exoo\shop\entities\EventTrait;
use exoo\shop\entities\AggregateRoot;
use exoo\shop\entities\Meta;
use exoo\shop\entities\Brand;
use exoo\shop\entities\Category\Category;
use exoo\shop\entities\Warehouse;
use exoo\shop\entities\Unit;
use exoo\shop\entities\Product\events\ProductAppearedInStock;
use exoo\shop\entities\Product\queries\ProductQuery;
use exoo\shop\entities\Product\ProductImage;
use exoo\shop\entities\Tag;
use exoo\shop\entities\WishlistItem;
use exoo\shop\entities\User;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use yii2tech\ar\position\PositionBehavior;
use exoo\shop\entities\Product\Modification\Modification;

/**
 * @property integer $id
 * @property integer $created_at
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property integer $category_id
 * @property integer $warehouse_id
 * @property integer $brand_id
 * @property integer $unit_id
 * @property integer $price_old
 * @property integer $price_new
 * @property integer $rating
 * @property integer $status
 * @property integer $weight
 * @property integer $quantity
 *
 * @property Meta $meta
 * @property Brand $brand
 * @property Category $category
 * @property CategoryAssignment[] $categoryAssignments
 * @property GroupAssignment[] $groupAssignments
 * @property Category[] $categories
 * @property Group[] $groups
 * @property TagAssignment[] $tagAssignments
 * @property Tag[] $tags
 * @property RelatedAssignment[] $relatedAssignments
 * @property Modification[] $modifications
 * @property Value[] $values
 * @property ProductImage[] $imagess
 * @property ProductImage $mainImage
 * @property Review[] $reviews
 */
class Product extends ActiveRecord implements AggregateRoot
{
    use EventTrait, StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 2;

    public $meta;

    public static function create($name): self
    {
        $product = new static();
        $product->name = $name;
        $product->status = self::STATUS_DRAFT;
        return $product;
    }

    public function setPrice($new, $old): void
    {
        $this->price_new = $new;
        $this->price_old = $old;
    }

    public function changeQuantity($quantity): void
    {
        if ($this->modifications) {
            throw new \DomainException('Change modifications quantity.');
        }
        $this->setQuantity($quantity);
    }
    public function edit(
        $brandId,
        $warehouseId,
        $unitId,
        $sku,
        $name,
        $slug,
        $shortDescription,
        $description,
        $modificationName,
        $weight,
        $length,
        $width,
        $height,
        $quantity,
        $adminNote,
        $status,
        Meta $meta
    ): void
    {
        $this->brand_id = $brandId;
        $this->warehouse_id = $warehouseId;
        $this->unit_id = $unitId;
        $this->sku = $sku;
        $this->name = $name;
        $this->slug = $slug;
        $this->short_description = $shortDescription;
        $this->description = $description;
        $this->modification_name = $modificationName;
        $this->weight = $weight;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->quantity = $quantity;
        $this->admin_note = $adminNote;
        $this->status = $status;
    }

    public function changeMainCategory($categoryId): void
    {
        $this->category_id = $categoryId;
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new \DomainException('Product is already active.');
        }
        $this->status = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        if ($this->isDraft()) {
            throw new \DomainException('Product is already draft.');
        }
        $this->status = self::STATUS_DRAFT;
    }

    public function isActive(): bool
    {
        return $this->status == self::STATUS_ACTIVE;
    }


    public function isDraft(): bool
    {
        return $this->status == self::STATUS_DRAFT;
    }

    public function isAvailable(): bool
    {
        return $this->quantity > 0 || Yii::$app->settings->get('shop', 'allowStock');
    }

    public function canChangeQuantity(): bool
    {
        return !$this->modifications;
    }

    public function isAvailableInStock($modificationId, $quantity): bool
    {
        return $quantity <= $this->getQuantity($modificationId);
    }

    public function getQuantity($modificationId = null): int
    {
        if ($modificationId) {
            return $this->getModification($modificationId)->quantity;
        }
        return $this->quantity;
    }

    public function checkout($modificationId, $quantity): void
    {
        if ($modificationId) {
            $modifications = $this->modifications;
            foreach ($modifications as $i => $modification) {
                if ($modification->isIdEqualTo($modificationId)) {
                    $modification->checkout($quantity);
                    $this->updateModifications($modifications);
                    return;
                }
            }
        }
        if ($quantity > $this->quantity) {
            throw new \DomainException('Only ' . $this->quantity . ' items are available.');
        }
        $this->setQuantity($this->quantity - $quantity);
    }

    private function setQuantity($quantity): void
    {
        if ($this->quantity == 0 && $quantity > 0) {
            $this->recordEvent(new ProductAppearedInStock($this));
        }
        $this->quantity = $quantity;
    }

    public function getSeoTitle(): string
    {
        return $this->meta->title ?: $this->name;
    }

    // Values

    public function setValue($id, $value): void
    {
        if ($value) {
            $values = $this->values;
            foreach ($values as $i => $val) {
                if ($val->isForCharacteristic($id)) {
                    $val->change($value);
                    $this->values = $values;
                    return;
                }
            }
            $values[] = Value::create($id, $value);
            $this->values = $values;
        }
    }

    public function getValue($id): Value
    {
        $values = $this->values;
        foreach ($values as $val) {
            if ($val->isForCharacteristic($id)) {
                return $val;
            }
        }
        return Value::blank($id);
    }

    public function revokeValues(): void
    {
        $this->values = [];
    }

    // Modification

    public function getModification($id): Modification
    {
        foreach ($this->modifications as $modification) {
            if ($modification->isIdEqualTo($id)) {
                return $modification;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Modification')]));
    }

    public function getModificationPrice($id): int
    {
        foreach ($this->modifications as $modification) {
            if ($modification->isIdEqualTo($id)) {
                return $modification->price_new ?: $this->price_new;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Modification')]));
    }

    public function addModification($sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $modification) {
            $sku = $this->modificationSku($sku);
            if ($modification->isSkuEqualTo($sku)) {
                throw new \DomainException(Yii::t('shop', '{name} already exists.', ['name' => Yii::t('shop', 'Modification')]));
            }
        }
        $modifications[] = Modification::create($sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity);
        $this->updateModifications($modifications);
    }

    public function editModification($id, $sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $i => $modification) {
            if ($modification->isIdEqualTo($id)) {
                $sku = $this->modificationSku($sku);
                $modification->edit($sku, $name, $priceNew, $priceOld, $weight, $length, $width, $height, $color, $quantity);
                $this->updateModifications($modifications);
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Modification')]));
    }

    public function modificationSku($sku)
    {
        if (!$sku) {
            if (Yii::$app->settings->get('shop', 'useSkuPrefix')) {
                $max = Modification::find()->max('id');
                return $this->sku . '-' . $max;
            }
        }
        return $sku;
    }

    public function removeModification($id): void
    {
        $modifications = $this->modifications;
        foreach ($modifications as $i => $modification) {
            if ($modification->isIdEqualTo($id)) {
                unset($modifications[$i]);
                $this->updateModifications($modifications);
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Modification')]));
    }

    private function updateModifications(array $modifications): void
    {
        $this->modifications = $modifications;
        $this->setQuantity(array_sum(array_map(function (Modification $modification) {
            return $modification->quantity;
        }, $this->modifications)));
    }

    // Categories

    public function assignCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForCategory($id)) {
                return;
            }
        }
        $assignments[] = CategoryAssignment::create($id);
        $this->categoryAssignments = $assignments;
    }

    public function revokeCategory($id): void
    {
        $assignments = $this->categoryAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForCategory($id)) {
                unset($assignments[$i]);
                $this->categoryAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Assignment')]));
    }

    public function revokeCategories(): void
    {
        $this->categoryAssignments = [];
    }

    // Groups

    public function assignGroup($id): void
    {
        $assignments = $this->groupAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForGroup($id)) {
                return;
            }
        }
        $assignments[] = GroupAssignment::create($id);
        $this->groupAssignments = $assignments;
    }

    public function revokeGroup($id): void
    {
        $assignments = $this->groupAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForGroup($id)) {
                unset($assignments[$i]);
                $this->groupAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Assignment')]));
    }

    public function revokeGroups(): void
    {
        $this->groupAssignments = [];
    }

    // Tags

    public function assignTag($id): void
    {
        $assignments = $this->tagAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForTag($id)) {
                return;
            }
        }
        $assignments[] = TagAssignment::create($id);
        $this->tagAssignments = $assignments;
    }

    public function revokeTag($id): void
    {
        $assignments = $this->tagAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForTag($id)) {
                unset($assignments[$i]);
                $this->tagAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Assignment')]));
    }

    public function revokeTags(): void
    {
        $this->tagAssignments = [];
    }

    // Related products

    public function assignRelatedProduct($id): void
    {
        $assignments = $this->relatedAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForProduct($id)) {
                return;
            }
        }
        $assignments[] = RelatedAssignment::create($id);
        $this->relatedAssignments = $assignments;
    }

    public function revokeRelatedProduct($id): void
    {
        $assignments = $this->relatedAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForProduct($id)) {
                unset($assignments[$i]);
                $this->relatedAssignments = $assignments;
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Assignment')]));
    }

    // Reviews

    public function addReview($userId, $vote, $plus, $minus, $comment): void
    {
        $reviews = $this->reviews;
        $reviews[] = Review::create($userId, $vote, $plus, $minus, $comment);
        $this->updateReviews($reviews);
    }

    public function editReview($id, $vote, $plus, $minus, $comment): void
    {
        $this->doWithReview($id, function (Review $review) use ($vote, $plus, $minus, $comment) {
            $review->edit($vote, $plus, $minus, $comment);
        });
    }

    public function activateReview($id): void
    {
        $this->doWithReview($id, function (Review $review) {
            $review->activate();
        });
    }

    public function inactivateReview($id): void
    {
        $this->doWithReview($id, function (Review $review) {
            $review->inactivate();
        });
    }

    public function moderationReview($id): void
    {
        $this->doWithReview($id, function (Review $review) {
            $review->moderation();
        });
    }

    private function doWithReview($id, callable $callback): void
    {
        $reviews = $this->reviews;
        foreach ($reviews as $review) {
            if ($review->isIdEqualTo($id)) {
                $callback($review);
                $this->updateReviews($reviews);
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Review')]));
    }

    public function removeReview($id): void
    {
        $reviews = $this->reviews;
        foreach ($reviews as $i => $review) {
            if ($review->isIdEqualTo($id)) {
                unset($reviews[$i]);
                $this->updateReviews($reviews);
                return;
            }
        }
        throw new \DomainException(Yii::t('shop', '{name} is not found.', ['name' => Yii::t('shop', 'Review')]));
    }

    private function updateReviews(array $reviews): void
    {
        $amount = 0;
        $total = 0;

        foreach ($reviews as $review) {
            if ($review->isActive()) {
                $amount++;
                $total += $review->getRating();
            }
        }

        $this->reviews = $reviews;
        $this->rating = $amount ? $total / $amount : null;
    }

    public static function getStatuses(): array
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('shop', 'Inactive'),
                'class' => 'danger',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('shop', 'Active'),
                'class' => 'success',
            ],
            [
                'id' => self::STATUS_DRAFT,
                'name' => Yii::t('shop', 'Draft'),
                'class' => 'muted',
            ],
        ];
    }

    ##########################

    public function getBrand(): ActiveQuery
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id']);
    }

    public function getWarehouse(): ActiveQuery
    {
        return $this->hasOne(Warehouse::class, ['id' => 'warehouse_id']);
    }

    public function getUnit(): ActiveQuery
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getCategoryAssignments(): ActiveQuery
    {
        return $this->hasMany(CategoryAssignment::class, ['product_id' => 'id']);
    }

    public function getCategories(): ActiveQuery
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->via('categoryAssignments');
    }

    public function getGroupAssignments(): ActiveQuery
    {
        return $this->hasMany(GroupAssignment::class, ['product_id' => 'id']);
    }

    public function getGroups(): ActiveQuery
    {
        return $this->hasMany(Group::class, ['id' => 'group_id'])->via('groupAssignments');
    }

    public function getTagAssignments(): ActiveQuery
    {
        return $this->hasMany(TagAssignment::class, ['product_id' => 'id']);
    }

    public function getTags(): ActiveQuery
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('tagAssignments');
    }

    public function getModifications(): ActiveQuery
    {
        return $this->hasMany(Modification::class, ['product_id' => 'id']);
    }

    public function getValues(): ActiveQuery
    {
        return $this->hasMany(Value::class, ['product_id' => 'id']);
    }

    public function getImages(): ActiveQuery
    {
        return $this->hasMany(ProductImage::class, ['product_id' => 'id']);
    }

    public function getFiles(): ActiveQuery
    {
        return $this->hasMany(ProductFile::class, ['product_id' => 'id']);
    }

    public function getMainImage(): ActiveQuery
    {
        return $this->hasOne(ProductImage::class, ['product_id' => 'id']);
    }

    public function getRelatedAssignments(): ActiveQuery
    {
        return $this->hasMany(RelatedAssignment::class, ['product_id' => 'id']);
    }

    public function getRelateds(): ActiveQuery
    {
        return $this->hasMany(Product::class, ['id' => 'related_id'])->via('relatedAssignments');
    }

    public function getReviews(): ActiveQuery
    {
        return $this->hasMany(Review::class, ['product_id' => 'id']);
    }

    public function getWishlistItems(): ActiveQuery
    {
        return $this->hasMany(WishlistItem::class, ['product_id' => 'id']);
    }

    public function getWishlist(): ActiveQuery
    {
        return $this->hasMany(WishlistItem::class, ['product_id' => 'id'])
            ->andWhere(['user_id' => Yii::$app->user->id]);
    }

    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_product}}';
    }

    public function behaviors(): array
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            'relations' => [
                'class' => SaveRelationsBehavior::class,
                'relations' => [
                    'categoryAssignments',
                    'groupAssignments',
                    'tagAssignments',
                    'relatedAssignments',
                    'modifications',
                    'values',
                    'reviews'
                ],
            ],
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            'dynamicAttribute' => [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [
                    'meta' => [],
                ],
            ],
            'position' => [
                'class' => PositionBehavior::class,
                'groupAttributes' => [
                    'category_id'
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Name'),
            'short_description' => Yii::t('shop', 'Short description'),
            'description' => Yii::t('shop', 'Description'),
            'modification_name' => Yii::t('shop', 'Modification name'),
            'category_id' => Yii::t('shop', 'Category'),
            'warehouse_id' => Yii::t('shop', 'Warehouse'),
            'brand_id' => Yii::t('shop', 'Brand'),
            'unit_id' => Yii::t('shop', 'Unit'),
            'slug' => Yii::t('shop', 'Slug'),
            'sku' => Yii::t('shop', 'SKU'),
            'quantity' => Yii::t('shop', 'Qty'),
            'weight' => Yii::t('shop', 'Weight in kg.'),
            'length' => Yii::t('shop', 'Length in cm.'),
            'width' => Yii::t('shop', 'Width in cm.'),
            'height' => Yii::t('shop', 'Height in cm.'),
            'dimensions' => Yii::t('shop', 'Dimensions (LxWxH) in cm.'),
            'price_old' => Yii::t('shop', 'Price old'),
            'price_new' => Yii::t('shop', 'Price'),
            'rating' => Yii::t('shop', 'Rating'),
            'hits' => Yii::t('shop', 'Hits'),
            'created_at' => Yii::t('shop', 'Created at'),
            'updated_at' => Yii::t('shop', 'Updated at'),
            'created_by' => Yii::t('shop', 'Created by'),
            'updated_by' => Yii::t('shop', 'Updated by'),
            'status' => Yii::t('shop', 'Status'),
            'position' => Yii::t('shop', 'Sorting'),
            'admin_note' => Yii::t('shop', 'Admin note'),
            'image' => Yii::t('shop', 'Photo'),
        ];
    }

    public function rules(): array
    {
        return [
            [['weight', 'quantity'], 'default', 'value' => 0],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterSave($insert, $changedAttributes): void
    {
        // $related = $this->getRelatedRecords();
        parent::afterSave($insert, $changedAttributes);
        // if (array_key_exists('mainPhoto', $related)) {
        //     $this->updateAttributes(['main_photo_id' => $related['mainPhoto'] ? $related['mainPhoto']->id : null]);
        // }
    }

    public static function find(): ProductQuery
    {
        return new ProductQuery(static::class);
    }
}
