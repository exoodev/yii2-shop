<?php

namespace exoo\shop\entities\Product;

use yii\db\ActiveRecord;

/**
 * @property integer $product_id;
 * @property integer $discount_id;
 */
class DiscountAssignment extends ActiveRecord
{
    public static function create($discountId): self
    {
        $assignment = new static();
        $assignment->discount_id = $discountId;
        return $assignment;
    }

    public function isForDiscount($id): bool
    {
        return $this->discount_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%shop_product_discount}}';
    }
}
