<?php

namespace exoo\shop\entities\queries;

use yii\db\ActiveQuery;
use exoo\shop\entities\PaymentMethod;

class PaymentMethodQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => PaymentMethod::STATUS_ACTIVE]);
    }
}
