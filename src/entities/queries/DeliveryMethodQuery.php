<?php

namespace exoo\shop\entities\queries;

use yii\db\ActiveQuery;
use exoo\shop\entities\DeliveryMethod;

class DeliveryMethodQuery extends ActiveQuery
{
    public function availableForWeight($weight)
    {
        return $this->andWhere(['and',
            ['or', ['min_weight' => null], ['<=', 'min_weight', $weight]],
            ['or', ['max_weight' => null], ['>=', 'max_weight', $weight]],
        ]);
    }

    public function active()
    {
        return $this->andWhere(['status' => DeliveryMethod::STATUS_ACTIVE]);
    }
}
