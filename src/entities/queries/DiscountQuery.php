<?php

namespace exoo\shop\entities\queries;

use yii\db\ActiveQuery;
use exoo\shop\entities\Discount;

class DiscountQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => Discount::STATUS_ACTIVE]);
    }
}
