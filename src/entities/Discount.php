<?php

namespace exoo\shop\entities;

use exoo\status\StatusTrait;
use exoo\shop\entities\queries\DiscountQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property integer $percent
 * @property string $name
 * @property string $from_date
 * @property string $to_date
 * @property bool $status
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Discount extends ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 2;

    public static function create($percent, $name, $fromDate, $toDate, $description, $status): self
    {
        $discount = new static();
        $discount->percent = $percent;
        $discount->name = $name;
        $discount->from_date = $fromDate;
        $discount->to_date = $toDate;
        $discount->description = $description;
        $discount->status = $status;
        return $discount;
    }

    public function edit($percent, $name, $fromDate, $toDate, $description, $status): void
    {
        $this->percent = $percent;
        $this->name = $name;
        $this->from_date = $fromDate;
        $this->to_date = $toDate;
        $this->description = $description;
        $this->status = $status;
    }

    public function activate(): void
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        $this->status = self::STATUS_DRAFT;
    }

    public function isEnabled(): bool
    {
        return true;
    }

    public static function tableName(): string
    {
        return '{{%shop_discount}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'percent' => Yii::t('shop', 'Percent'),
            'from_date' => Yii::t('shop', 'From date'),
            'to_date' => Yii::t('shop', 'To date'),
            'position' => Yii::t('shop', 'Sorting'),
            'status' => Yii::t('shop', 'Status'),
            'created_at' => Yii::t('shop', 'Created at'),
            'updated_at' => Yii::t('shop', 'Updated at'),
            'created_by' => Yii::t('shop', 'Created by'),
            'updated_by' => Yii::t('shop', 'Updated by'),
        ];
    }

    public static function find(): DiscountQuery
    {
        return new DiscountQuery(static::class);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            BlameableBehavior::class,
            TimestampBehavior::class,
            'position' => [
                'class' => PositionBehavior::class,
            ],
        ];
    }

    public static function getStatuses()
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('shop', 'Inactive'),
                'class' => 'danger',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('shop', 'Active'),
                'class' => 'success',
            ],
            [
                'id' => self::STATUS_DRAFT,
                'name' => Yii::t('shop', 'Draft'),
                'class' => 'muted',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Yii::$app->user->identityClass, ['id' => 'updated_by']);
    }
}
