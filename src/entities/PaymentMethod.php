<?php

namespace exoo\shop\entities;

use Yii;
use exoo\shop\entities\queries\PaymentMethodQuery;
use yii\db\ActiveRecord;
use exoo\status\StatusTrait;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property string $short_description
 * @property string $description
 * @property int transaction_limit
 * @property int status
 * @property int $position
 */
class PaymentMethod extends ActiveRecord
{
    use StatusTrait;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public static function create($name, $icon, $shortDescription, $description, $transactionLimit, $status): self
    {
        $method = new static();
        $method->name = $name;
        $method->icon = $icon;
        $method->short_description = $shortDescription;
        $method->description = $description;
        $method->transaction_limit = $transactionLimit;
        $method->status = $status;
        return $method;
    }

    public function edit($name, $icon, $shortDescription, $description, $transactionLimit, $status): void
    {
        $this->name = $name;
        $this->icon = $icon;
        $this->short_description = $shortDescription;
        $this->description = $description;
        $this->transaction_limit = $transactionLimit;
        $this->status = $status;
    }

    public static function tableName(): string
    {
        return '{{%shop_payment_method}}';
    }

    public static function find(): PaymentMethodQuery
    {
        return new PaymentMethodQuery(static::class);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'icon' => Yii::t('shop', 'Icon'),
            'short_description' => Yii::t('shop', 'Short description'),
            'description' => Yii::t('shop', 'Description'),
            'transaction_limit' => Yii::t('shop', 'Transaction limit'),
            'status' => Yii::t('shop', 'Status'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }

    public static function getStatuses()
    {
        return [
            [
                'id' => self::STATUS_INACTIVE,
                'name' => Yii::t('shop', 'Inactive'),
                'class' => 'danger',
            ],
            [
                'id' => self::STATUS_ACTIVE,
                'name' => Yii::t('shop', 'Active'),
                'class' => 'success',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'positionBehavior' => [
                'class' => PositionBehavior::class,
            ],
        ];
    }
}
