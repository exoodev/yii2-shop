<?php

namespace exoo\shop\entities\Group;

use exoo\shop\entities\Group\GroupCharacteristic;
use exoo\shop\entities\Meta;
use exoo\shop\entities\queries\GroupQuery;
use exoo\shop\entities\Characteristic\Characteristic;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use exoo\shop\entities\behaviors\MetaBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property Meta $meta
 *
 * @property Group $parent
 * @property Group[] $parents
 * @property Group[] $children
 * @property Group $prev
 * @property Group $next
 * @mixin NestedSetsBehavior
 */
class Group extends ActiveRecord
{
    public static function create($name, $slug, $title, $description, $characteristicIds, Meta $meta): self
    {
        $group = new static();
        $group->name = $name;
        $group->slug = $slug;
        $group->title = $title;
        $group->description = $description;
        $group->characteristicIds = $characteristicIds;
        $group->meta = $meta->toArray();
        return $group;
    }

    public function edit($name, $slug, $title, $description, $characteristicIds, Meta $meta): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->title = $title;
        $this->description = $description;
        $this->characteristicIds = $characteristicIds;
        $this->meta = $meta->toArray();
    }

    public function getSeoTitle(): string
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile(): string
    {
        return $this->title ?: $this->name;
    }

    public static function tableName(): string
    {
        return '{{%shop_group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['characteristicIds', 'safe'],
        ];
    }

    public function behaviors(): array
    {
        $module = Yii::$app->controller->module;
        return [
            MetaBehavior::class,
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [
                    'meta' => []
                ],
            ],
            [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'characteristics',
                'relationReferenceAttribute' => 'characteristicIds',
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find(): GroupQuery
    {
        return new GroupQuery(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupCharacteristics()
    {
        return $this->hasMany(GroupCharacteristic::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this
            ->hasMany(Characteristic::className(), ['id' => 'characteristic_id'])
            ->via('groupCharacteristics');
    }

    /**
     * To get the first parent of a node
     * @return integer|null
     */
    public function getParent()
    {
        return $this->parents(1)->one();
    }
}
