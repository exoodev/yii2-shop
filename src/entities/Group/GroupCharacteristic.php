<?php

namespace exoo\shop\entities\Group;

use Yii;
use exoo\shop\entities\Group\Group;
use exoo\shop\entities\Characteristic\Characteristic;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shop_group_characteristic}}".
 *
 * @property int $group_id
 * @property int $characteristic_id
 *
 * @property Group $group
 * @property Characteristic $characteristic
 */
class GroupCharacteristic extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_group_characteristic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'characteristic_id'], 'required'],
            [['group_id', 'characteristic_id'], 'integer'],
            [['group_id', 'characteristic_id'], 'unique', 'targetAttribute' => ['group_id', 'characteristic_id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['characteristic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristic::className(), 'targetAttribute' => ['characteristic_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'group_id' => Yii::t('shop', 'Group'),
            'characteristic_id' => Yii::t('shop', 'Characteristic'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristic()
    {
        return $this->hasOne(Characteristic::className(), ['id' => 'characteristic_id']);
    }
}
