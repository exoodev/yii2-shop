<?php

namespace exoo\shop\entities\Category;

use exoo\shop\entities\Category\CategoryImage;
use exoo\shop\entities\Category\CategoryCharacteristic;
use exoo\shop\entities\Meta;
use exoo\shop\entities\queries\CategoryQuery;
use exoo\shop\entities\Characteristic\Characteristic;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsBehavior;
use yii2tech\ar\linkmany\LinkManyBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use exoo\storage\behaviors\FileBehavior;
use exoo\shop\entities\behaviors\MetaBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property Meta $meta
 *
 * @property Category $parent
 * @property Category[] $parents
 * @property Category[] $children
 * @property Category $prev
 * @property Category $next
 * @mixin NestedSetsBehavior
 */
class Category extends ActiveRecord
{
    public static function create($name, $slug, $title, $description, $characteristicIds, Meta $meta): self
    {
        $category = new static();
        $category->name = $name;
        $category->slug = $slug;
        $category->title = $title;
        $category->description = $description;
        $category->characteristicIds = $characteristicIds;
        $category->meta = $meta->toArray();
        return $category;
    }

    public function edit($name, $slug, $title, $description, $characteristicIds, Meta $meta): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->title = $title;
        $this->description = $description;
        $this->characteristicIds = $characteristicIds;
        $this->meta = $meta->toArray();
    }

    public function getSeoTitle(): string
    {
        return $this->meta->title ?: $this->getHeadingTile();
    }

    public function getHeadingTile(): string
    {
        return $this->title ?: $this->name;
    }

    public static function tableName(): string
    {
        return '{{%shop_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['characteristicIds', 'safe'],
        ];
    }

    public function behaviors(): array
    {
        $module = Yii::$app->getModule('shop');
        return [
            MetaBehavior::class,
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [
                    'meta' => []
                ],
            ],
            [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            [
                'class' => LinkManyBehavior::class,
                'relation' => 'characteristics',
                'relationReferenceAttribute' => 'characteristicIds',
            ],
            [
                'class' => FileBehavior::class,
                'relation' => 'images',
                'relationAttribute' => 'category_id',
                'bucketName' => 'categoryImages',
                'bucketData' => $module->buckets['categoryImages'],
                'transformImage' => ArrayHelper::getValue($module->params, 'category.images.transform'),
            ]
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find(): CategoryQuery
    {
        return new CategoryQuery(static::class);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(CategoryImage::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainImage()
    {
        return $this->hasOne(CategoryImage::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryCharacteristics()
    {
        return $this->hasMany(CategoryCharacteristic::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this
            ->hasMany(Characteristic::className(), ['id' => 'characteristic_id'])
            ->via('categoryCharacteristics');
    }


    /**
     * To get the first parent of a node
     * @return integer|null
     */
    public function getParent()
    {
        return $this->parents(1)->one();
    }

    public function getParents()
    {
        return $this->parents()->all();
    }

    /**
     * Whether the model is root category.
     * @return boolean
     */
    public function isRoot()
    {
        return $this->lft == 1;
    }
}
