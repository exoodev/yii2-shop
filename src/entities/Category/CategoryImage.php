<?php

namespace exoo\shop\entities\Category;

use Yii;
use yii\db\ActiveRecord;
use exoo\shop\entities\Category\Category;

/**
 * This is the model class for table "{{%shop_category_image}}".
 *
 * @property int $id
 * @property int $category_id
 * @property string $filename
 * @property int $position
 *
 * @property Category $category
 */
class CategoryImage extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_category_image}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'filename'], 'required'],
            [['category_id', 'position'], 'integer'],
            [['filename'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'category_id' => Yii::t('shop', 'Category'),
            'filename' => Yii::t('shop', 'Images'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return parent::find()->orderBy([
            'position' => SORT_ASC,
            'category_id' => SORT_ASC,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function url($size = null)
    {
        return Yii::$app->fileStorage->getFileUrl('categoryImages', $this->filename, $size);
    }
}
