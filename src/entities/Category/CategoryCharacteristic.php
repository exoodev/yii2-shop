<?php

namespace exoo\shop\entities\Category;

use Yii;
use exoo\shop\entities\Category\Category;
use exoo\shop\entities\Characteristic\Characteristic;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%shop_category_characteristic}}".
 *
 * @property int $category_id
 * @property int $characteristic_id
 *
 * @property Category $category
 * @property Characteristic $characteristic
 */
class CategoryCharacteristic extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_category_characteristic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'characteristic_id'], 'required'],
            [['category_id', 'characteristic_id'], 'integer'],
            [['category_id', 'characteristic_id'], 'unique', 'targetAttribute' => ['category_id', 'characteristic_id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['characteristic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristic::className(), 'targetAttribute' => ['characteristic_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('shop', 'Category'),
            'characteristic_id' => Yii::t('shop', 'Characteristic'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristic()
    {
        return $this->hasOne(Characteristic::className(), ['id' => 'characteristic_id']);
    }
}
