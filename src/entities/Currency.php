<?php

namespace exoo\shop\entities;

use Yii;

/**
 * This is the model class for table "{{%shop_currency}}".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $icon
 *
 * @property Order[] $Orders
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'icon'], 'required'],
            [['name', 'icon'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Name'),
            'code' => Yii::t('shop', 'Code'),
            'icon' => Yii::t('shop', 'Icon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['currency_id' => 'id']);
    }
}
