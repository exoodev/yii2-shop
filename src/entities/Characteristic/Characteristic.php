<?php

namespace exoo\shop\entities\Characteristic;

use Yii;
use exoo\shop\entities\Product\Value as ProductValue;
use exoo\shop\entities\Product\Modification\Value as ModificationValue;
use exoo\shop\entities\Category\CategoryCharacteristic;
use exoo\shop\entities\Characteristic\CharacteristicGroup;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\behaviors\SluggableBehavior;
use yii2tech\ar\dynattribute\DynamicAttributeBehavior;
use yii2tech\ar\position\PositionBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $group_id
 * @property string $type
 * @property string $required
 * @property string $default
 * @property array $variants
 * @property integer $position
 */
class Characteristic extends ActiveRecord
{
    const TYPE_STRING = 'string';
    const TYPE_INTEGER = 'integer';
    const TYPE_FLOAT = 'float';

    public static function create($name, $slug, $description, $group_id, $type, $required, $default, array $variants): self
    {
        $object = new static();
        $object->name = $name;
        $object->slug = $slug;
        $object->description = $description;
        $object->group_id = $group_id;
        $object->type = $type;
        $object->required = $required;
        $object->default = $default;
        $object->variants = $variants;
        return $object;
    }

    public function edit($name, $slug, $description, $group_id, $type, $required, $default, array $variants): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->description = $description;
        $this->group_id = $group_id;
        $this->type = $type;
        $this->required = $required;
        $this->default = $default;
        $this->variants = $variants;
    }

    public function isString(): bool
    {
        return $this->type === self::TYPE_STRING;
    }

    public function isInteger(): bool
    {
        return $this->type === self::TYPE_INTEGER;
    }

    public function isFloat(): bool
    {
        return $this->type === self::TYPE_FLOAT;
    }

    public function isSelect(): bool
    {
        return count($this->variants) > 0;
    }

    public static function tableName(): string
    {
        return '{{%shop_characteristic}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'name' => Yii::t('shop', 'Name'),
            'description' => Yii::t('shop', 'Description'),
            'group_id' => Yii::t('shop', 'Group'),
            'slug' => Yii::t('shop', 'Slug'),
            'type' => Yii::t('shop', 'Type'),
            'variants' => Yii::t('shop', 'Values list'),
            'default' => Yii::t('shop', 'Default value'),
            'required' => Yii::t('shop', 'Required'),
            'position' => Yii::t('shop', 'Sorting'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            'dynamicAttribute' => [
                'class' => DynamicAttributeBehavior::class,
                'saveDynamicAttributeDefaults' => false,
                'dynamicAttributeDefaults' => [
                    'variants' => []
                ],
            ],
            'position' => [
                'class' => PositionBehavior::class,
                'groupAttributes' => [
                    'group_id'
                ],
            ],
        ];
    }

    public function getGroup(): ActiveQuery
    {
        return $this->hasOne(CharacteristicGroup::class, ['id' => 'group_id']);
    }

    public function getProductValues(): ActiveQuery
    {
        return $this->hasOne(ProductValue::className(), ['characteristic_id' => 'id']);
    }

    public function getModificationValues(): ActiveQuery
    {
        return $this->hasOne(ModificationValue::className(), ['characteristic_id' => 'id']);
    }

    public function getCategoryCharacteristics(): ActiveQuery
    {
        return $this->hasMany(CategoryCharacteristic::className(), ['characteristic_id' => 'id']);
    }
}
