<?php

namespace exoo\shop\entities\Characteristic;

use Yii;
use yii\db\ActiveRecord;
use yii2tech\ar\position\PositionBehavior;
use exoo\shop\entities\Characteristic\Characteristic;

/**
 * @property integer $id
 * @property string $name
 * @property string $position
 */
class CharacteristicGroup extends ActiveRecord
{
    public static function create($name): self
    {
        $characteristicGroup = new static();
        $characteristicGroup->name = $name;
        return $characteristicGroup;
    }

    public function edit($name): void
    {
        $this->name = $name;
    }

    public static function tableName(): string
    {
        return '{{%shop_characteristic_group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'name' => Yii::t('shop', 'Name'),
            'position' => Yii::t('shop', 'Position'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'position' => [
                'class' => PositionBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(Characteristic::className(), ['group_id' => 'id']);
    }
}
