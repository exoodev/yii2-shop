<?php

namespace exoo\shop\entities;

use Yii;
use exoo\shop\entities\Order\Order;
use exoo\shop\entities\Order\OrderItem;
use exoo\shop\entities\Product\Review;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * User model
 * @property WishlistItem[] $wishlistItems
 */
class User extends \exoo\user\models\User
{
    private $_wishlistCount;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['wishlistItems'],
            ],
        ]);
    }

    public function setWishlistItemsCount($count)
    {
        $this->_wishlistCount = (int) $count;
    }

    public function getWishlistItemsCount()
    {
        if ($this->isNewRecord) {
            return null;
        }

        if ($this->_wishlistCount === null) {
            $this->setWishlistItemsCount($this->getWishlistItems()->count());
        }

        return $this->_wishlistCount;
    }

    public function getWishlistItems(): ActiveQuery
    {
        return $this->hasMany(WishlistItem::class, ['user_id' => 'id']);
    }

    public function addToWishList($productId): void
    {
        $items = $this->wishlistItems;
        foreach ($items as $item) {
            if ($item->isForProduct($productId)) {
                throw new \DomainException('Item is already added.');
            }
        }
        $items[] = WishlistItem::create($productId);
        $this->wishlistItems = $items;
    }

    public function removeFromWishList($productId): void
    {
        $items = $this->wishlistItems;
        foreach ($items as $i => $item) {
            if ($item->isForProduct($productId)) {
                unset($items[$i]);
                $this->wishlistItems = $items;
                return;
            }
        }
        throw new \DomainException('Item is not found.');
    }

    public function getReviews(): ActiveQuery
    {
        return $this->hasMany(Review::class, ['created_by' => 'id']);
    }

    public function canAddReview($productId)
    {
        if (Yii::$app->settings->get('shop', 'canAddReviewOnlyBuyer') && !$this->getOrderItemExists($productId)) {
            return false;
        }

        return true;
    }

    public function getOrderItemExists($productId): bool
    {
        $items = $this->ordersItems;
        foreach ($items as $i => $item) {
            if ($item->product_id == $productId) {
                return true;
            }
        }
        return false;
    }

    public function getOrders(): ActiveQuery
    {
        return $this->hasMany(Order::class, ['created_by' => 'id']);
    }

    public function getOrdersItems(): ActiveQuery
    {
        return $this->hasMany(OrderItem::class, ['created_by' => 'id']);
    }

    /**
     * @return array
     */
    public function releaseEvents(): array
    {
        return [];
    }
}
