<?php

namespace exoo\shop\entities;

use exoo\shop\entities\Meta;
use exoo\shop\entities\behaviors\MetaBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property Meta $meta
 */
class Brand extends ActiveRecord
{
    public $meta;

    public static function create($name, $slug, $description, Meta $meta): self
    {
        $brand = new static();
        $brand->name = $name;
        $brand->slug = $slug;
        $brand->description = $description;
        $brand->meta = $meta;
        return $brand;
    }

    public function edit($name, $slug, $description, Meta $meta): void
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->description = $description;
        $this->meta = $meta;
    }

    public function getSeoTitle(): string
    {
        return isset($this->meta->title) ?: $this->name;
    }

    ##########################

    public static function tableName(): string
    {
        return '{{%shop_brand}}';
    }

    public function behaviors(): array
    {
        return [
            'sluggable' => [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            MetaBehavior::className(),
        ];
    }
}
