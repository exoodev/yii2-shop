<?php

namespace exoo\shop\components\cart;

use Yii;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\shop\entities\Product\Product;

class CartItem
{
    private $product;
    private $modificationId;
    private $quantity;
    private $maxQuantity;

    public function __construct(Product $product, $modificationId, $quantity)
    {
        if (Yii::$app->settings->get('shop', 'allowStock')) {
            $this->maxQuantity = $product->getQuantity($modificationId);
            if (!$product->isAvailableInStock($modificationId, $quantity)) {
                // throw new \DomainException(Yii::t('shop', 'This goods is only available in {quantity} {unit}', [
                //     'quantity' => $this->maxQuantity,
                //     'unit' => $product->unit->short_name,
                // ]));
                // $quantity = $this->maxQuantity;
            }
        }
        $this->product = $product;
        $this->modificationId = $modificationId;
        $this->quantity = $quantity;
    }

    public function getMaxQuantity(): ?int
    {
        return $this->maxQuantity;
    }

    public function getId(): string
    {
        return md5(serialize([$this->product->id, $this->modificationId]));
    }

    public function getProductId(): int
    {
        return $this->product->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getModificationId(): ?int
    {
        return $this->modificationId;
    }

    public function getModification(): ?Modification
    {
        if ($this->modificationId) {
            return $this->product->getModification($this->modificationId);
        }
        return null;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getPrice(): int
    {
        if ($this->modificationId) {
            return $this->product->getModificationPrice($this->modificationId);
        }
        return $this->product->price_new;
    }

    public function getWeight(): int
    {
        return $this->product->weight * $this->quantity;
    }

    public function getCost(): int
    {
        return $this->getPrice() * $this->quantity;
    }

    public function plus($quantity)
    {
        return new static($this->product, $this->modificationId, $this->quantity + $quantity);
    }

    public function changeQuantity($quantity)
    {
        return new static($this->product, $this->modificationId, $quantity);
    }
}
