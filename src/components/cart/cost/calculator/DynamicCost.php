<?php

namespace exoo\shop\components\cart\cost\calculator;

use exoo\shop\components\cart\cost\Cost;
use exoo\shop\components\cart\cost\Discount as CartDiscount;
use exoo\shop\entities\Discount as DiscountEntity;

class DynamicCost implements CalculatorInterface
{
    private $next;

    public function __construct(CalculatorInterface $next)
    {
        $this->next = $next;
    }

    public function getCost(array $items): Cost
    {
        /** @var DiscountEntity[] $discounts */
        $discounts = DiscountEntity::find()->active()->orderBy('position')->all();

        $cost = $this->next->getCost($items);

        foreach ($discounts as $discount) {
            if ($discount->isEnabled()) {
                $new = new CartDiscount($cost->getOrigin() * $discount->percent / 100, $discount->name, $discount->description);
                $cost = $cost->withDiscount($new);
            }
        }

        return $cost;
    }
}
