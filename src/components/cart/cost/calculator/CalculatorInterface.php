<?php

namespace exoo\shop\components\cart\cost\calculator;

use exoo\shop\components\cart\CartItem;
use exoo\shop\components\cart\cost\Cost;

interface CalculatorInterface
{
    /**
     * @param CartItem[] $items
     * @return Cost
     */
    public function  getCost(array $items): Cost;
} 