<?php

namespace exoo\shop\components\cart\cost;

final class Discount
{
    private $value;
    private $name;
    private $description;

    public function __construct(float $value, string $name, string $description)
    {
        $this->value = $value;
        $this->name = $name;
        $this->description = $description;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
