<?php

namespace exoo\shop\components\listeners\Category;

use exoo\shop\entities\Category\Category;
use exoo\shop\repositories\events\EntityPersisted;
use yii\caching\Cache;
use yii\caching\TagDependency;

class CategoryPersistenceListener
{
    private $cache;

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    public function handle(EntityPersisted $event): void
    {
        if ($event->entity instanceof Category) {
            $this->cache->cachePath = \Yii::getAlias('@frontend') . '/runtime/cache';
            TagDependency::invalidate($this->cache, ['categories']);
        }
    }
}