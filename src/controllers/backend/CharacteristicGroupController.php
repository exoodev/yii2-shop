<?php

namespace exoo\shop\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use exoo\position\PositionAction;
use exoo\shop\entities\Characteristic\CharacteristicGroup;
use exoo\shop\models\backend\forms\CharacteristicGroupForm;
use exoo\shop\models\backend\search\CharacteristicGroupSearch;
use exoo\shop\services\manage\CharacteristicGroupManageService;

class CharacteristicGroupController extends Controller
{
    private $service;

    public function __construct($id, $module, CharacteristicGroupManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => CharacteristicGroup::class,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CharacteristicGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $form = new CharacteristicGroupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $characteristicGroup = $this->service->create($form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->redirect(['update', 'id' => $characteristicGroup->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $characteristicGroup = $this->findModel($id);

        $form = new CharacteristicGroupForm($characteristicGroup);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($characteristicGroup->id, $form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'characteristicGroup' => $characteristicGroup,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->remove($id);
                }
                return $this->redirect(['index']);
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.danger', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @return CharacteristicGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): CharacteristicGroup
    {
        if (($model = CharacteristicGroup::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
