<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\DeliveryMethodForm;
use exoo\shop\services\manage\DeliveryMethodManageService;
use Yii;
use exoo\shop\entities\DeliveryMethod;
use exoo\shop\models\backend\search\DeliveryMethodSearch;
use exoo\position\PositionAction;
use exoo\status\StatusAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DeliveryController extends Controller
{
    private $service;

    public function __construct($id, $module, DeliveryMethodManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => DeliveryMethod::class,
            ],
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => DeliveryMethod::class,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliveryMethodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $method = new DeliveryMethod();
        $form = new DeliveryMethodForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $method = $this->service->create($form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->redirect(['update', 'id' => $method->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'method' => $method,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $method = $this->findModel($id);

        $form = new DeliveryMethodForm($method);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($method->id, $form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'method' => $method,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->remove($id);
                }
                return $this->redirect(['index']);
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.danger', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @return DeliveryMethod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): DeliveryMethod
    {
        if (($model = DeliveryMethod::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
