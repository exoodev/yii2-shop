<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\CategoryForm;
use exoo\shop\services\manage\CategoryManageService;
use Yii;
use exoo\shop\entities\Category\Category;
use exoo\nested\MoveAction;
use exoo\storage\actions\FileAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class CategoryController extends Controller
{
    private $service;

    public function __construct($id, $module, CategoryManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'move' => [
                'class' => MoveAction::class,
                'modelClass' => Category::class,
            ],
            'images' => [
                'class' => FileAction::class,
                'modelClass' => Category::class,
                'relation' => 'images',
                'relationAttribute' => 'category_id',
                'sortable' => true,
                'bucketName' => 'categoryImages',
                'bucketData' => $this->module->buckets['categoryImages'],
                'transformImage' => ArrayHelper::getValue($this->module->params, 'category.images.transform'),
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'modelClass' => Category::class,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate($parent = null)
    {
        $form = new CategoryForm();
        $form->parentId = $parent;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $category = $this->service->create($form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->redirect(['update', 'id' => $category->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', Yii::t('shop', $e->getMessage()));
            }
        }
        return $this->render('form', [
            'model' => $form,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $category = $this->findModel($id);

        $form = new CategoryForm($category);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($category->id, $form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', Yii::t('shop', $e->getMessage()));
            }
        }
        return $this->render('form', [
            'model' => $form,
            'category' => $category,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', Yii::t('shop', $e->getMessage()));
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Category
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
