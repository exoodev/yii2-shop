<?php

namespace exoo\shop\controllers\backend;

use yii\web\Controller;
use exoo\shop\models\backend\Settings;

/**
 * DefaultController class.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'settings' => [
                'class' => 'exoo\settings\actions\SettingsAction',
                'modelClass' => Settings::className(),
            ]
        ];
    }

    public function actionDirectories()
    {
        return $this->render('directories');
    }
}
