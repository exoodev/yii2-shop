<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\Product\QuantityForm;
use exoo\shop\models\backend\forms\Product\PriceForm;
use exoo\shop\models\backend\forms\Product\ProductCreateForm;
use exoo\shop\models\backend\forms\Product\ProductEditForm;
use exoo\shop\services\manage\ProductManageService;
use exoo\shop\entities\Product\Product;
use exoo\shop\models\backend\search\ProductSearch;
use exoo\shop\repositories\ProductRepository;
use Yii;
use exoo\status\StatusAction;
use exoo\position\PositionAction;
use exoo\storage\actions\FileAction;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class ProductController extends Controller
{
    private $service;
    private $products;

    public function __construct($id, $module, ProductManageService $service, ProductRepository $products, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->products = $products;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'activate' => ['post'],
                    'draft' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => Product::class,
            ],
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => Product::class,
            ],
            'images' => [
                'class' => FileAction::class,
                'modelClass' => Product::class,
                'bucketName' => 'productImages',
                'bucketData' => $this->module->buckets['productImages'],
                'relation' => 'images',
                'relationAttribute' => 'product_id',
                'sortable' => true,
                'transformImage' => ArrayHelper::getValue($this->module->params, 'product.images.transform'),
            ],
            'files' => [
                'class' => FileAction::class,
                'modelClass' => Product::class,
                'bucketName' => 'productFiles',
                'bucketData' => $this->module->buckets['productFiles'],
                'relation' => 'files',
                'relationAttribute' => 'product_id',
                'sortable' => true,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $product = $this->findModel($id);
        $modificationsProvider = new ActiveDataProvider([
            'query' => $product->getModifications()->orderBy('position'),
            'pagination' => false,
        ]);

        return $this->render('view', [
            'product' => $product,
            'modificationsProvider' => $modificationsProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        try {
            $product = $this->service->create(new ProductCreateForm());
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
            return $this->goBack();
        }
        return $this->redirect(['update', 'id' => $product->id]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $product = $this->findModel($id);
        $relatedProvider = new ActiveDataProvider([
            'query' => $product->getRelatedAssignments()
                ->with('product.category')
                ->orderBy('position'),
        ]);
        $form = new ProductEditForm($product);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($product->id, $form);
                if (Yii::$app->request->post('action') == 'close') {
                    return $this->redirect(Yii::$app->user->returnUrl);
                }
                if (Yii::$app->request->isAjax) {
                    return $this->asJson(['message' => Yii::t('shop', 'Saved'), 'result' => true]);
                } else {
                    Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                    return $this->refresh();
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }
        if (Yii::$app->request->isPjax) {
            return $this->renderAjax('update', [
                'model' => $form,
                'product' => $product,
                'relatedProvider' => $relatedProvider,
            ]);
        } else {
            return $this->render('update', [
                'model' => $form,
                'product' => $product,
                'relatedProvider' => $relatedProvider,
            ]);
        }

    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionPrice($id)
    {
        $product = $this->findModel($id);
        $form = new PriceForm($product);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->changePrice($product->id, $form);
                return $this->redirect(['view', 'id' => $product->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }

        return $this->render('price', [
            'model' => $form,
            'product' => $product,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionQuantity($id)
    {
        $product = $this->findModel($id);
        $form = new QuantityForm($product);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->changeQuantity($product->id, $form);
                return $this->redirect(['view', 'id' => $product->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }
        
        return $this->render('quantity', [
            'model' => $form,
            'product' => $product,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->remove($id);
                }
                return $this->redirect(['index']);
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
    }

    public function actionRemoveValue($id, $characteristic_id)
    {
        try {
            $this->service->removeValue($id, $characteristic_id);
        } catch (\DomainException $e) {
            if (Yii::$app->request->isAjax) {
                return $this->asJson([
                    'message' => $e->getMessage(),
                    'status' => 'error'
                ]);
            }
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
            return $this->redirect(['update', 'id' => $id]);
        }
    }

    /**
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Product
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
