<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\Product\ModificationCreateForm;
use exoo\shop\models\backend\forms\Product\ModificationEditForm;
use exoo\shop\services\manage\ProductManageService;
use Yii;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Product\Modification\Modification;
use exoo\position\PositionAction;
use exoo\storage\actions\FileAction;
use exoo\uikit\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AjaxFilter;
use yii\helpers\ArrayHelper;

class ModificationController extends Controller
{
    private $service;

    public function __construct($id, $module, ProductManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'batch-delete' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => ['create', 'batch-delete'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => Modification::class,
            ],
            'images' => [
                'class' => FileAction::class,
                'modelClass' => Modification::class,
                'bucketName' => 'productImages',
                'bucketData' => $this->module->buckets['productImages'],
                'relation' => 'images',
                'relationAttribute' => 'modification_id',
                'sortable' => true,
                'transformImage' => ArrayHelper::getValue($this->module->params, 'productImages.transform'),
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect('shop/product');
    }

    /**
     * @param $product_id
     * @return mixed
     */
    public function actionCreate($product_id)
    {
        $product = $this->findModel($product_id);
        $form = new ModificationCreateForm();

        if ($form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                try {
                    $this->service->addModification($product->id, $form);
                } catch (\DomainException $e) {
                    Yii::$app->errorHandler->logException($e);
                    Yii::$app->session->setFlash('notify.error', $e->getMessage());
                }
            } else {
                return $this->asJson(['validation' => ActiveForm::validate($form)]);
            }
        }

        return $this->renderAjax('create', [
            'product' => $product,
            'model' => $form,
        ]);
    }

    /**
     * @param integer $product_id
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($product_id, $id)
    {
        $product = $this->findModel($product_id);
        $modification = $product->getModification($id);
        $form = new ModificationEditForm($product, $modification);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->editModification($product->id, $modification->id, $form);
                return $this->redirect(['product/view', 'id' => $product->id, '#' => 'modifications']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'product' => $product,
            'model' => $form,
            'modification' => $modification,
        ]);
    }

    /**
     * @param $product_id
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($product_id, $id)
    {
        $product = $this->findModel($product_id);
        try {
            $this->service->removeModification($product->id, $id);

            if (Yii::$app->request->isAjax) {
                return $this->asJson(['result' => true]);
            }
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
        return $this->redirect(['product/view', 'id' => $product->id, '#' => 'modifications']);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete($product_id)
    {
        $product = $this->findModel($product_id);
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->removeModification($product->id, $id);
                }
                return $this->asJson(['result' => true]);
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Product
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
