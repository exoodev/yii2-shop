<?php

namespace exoo\shop\controllers\backend;

use Yii;
use exoo\shop\entities\Product\Product;
use exoo\shop\entities\Product\RelatedAssignment;
use exoo\shop\services\manage\ProductManageService;
use exoo\shop\models\backend\search\ProductSearch;
use exoo\position\PositionAction;
use exoo\uikit\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class RelatedController extends Controller
{
    private $service;

    public function __construct($id, $module, ProductManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'add' => ['post'],
                    'delete' => ['post'],
                    'batch-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => RelatedAssignment::class,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionProducts($id)
    {
        $product = $this->findModel($id);
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('products', [
            'product' => $product,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $product_id
     * @return mixed
     */
    public function actionAdd($product_id)
    {
        if ($relatedIds = Yii::$app->request->post('ids')) {
            try {
                foreach ($relatedIds as $related_id) {
                    $this->service->addRelatedProduct($product_id, $related_id);
                }
                return $this->asJson(['result' => true]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }
    }

    /**
     * @param integer $product_id
     * @param integer $related_id
     * @return mixed
     */
    public function actionDelete($product_id, $related_id)
    {
        try {
            $this->service->removeRelatedProduct($product_id, $related_id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
        return $this->asJson(['result' => true]);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete($product_id)
    {
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->removeRelatedProduct($product_id, $id);
                }
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Product
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
