<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\GroupForm;
use exoo\shop\services\manage\GroupManageService;
use Yii;
use exoo\shop\entities\Group\Group;
use exoo\nested\MoveAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class GroupController extends Controller
{
    private $service;

    public function __construct($id, $module, GroupManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['get'],
                    'create' => ['get', 'post'],
                    'update' => ['get', 'post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'move' => [
                'class' => MoveAction::class,
                'modelClass' => Group::class,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'modelClass' => Group::class,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate($parent = null)
    {
        $form = new GroupForm();
        $form->parentId = $parent;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $group = $this->service->create($form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->redirect(['update', 'id' => $group->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $group = $this->findModel($id);

        $form = new GroupForm($group);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($group->id, $form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.error', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'group' => $group,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Group
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
