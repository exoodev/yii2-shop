<?php

namespace exoo\shop\controllers\backend;

use exoo\shop\models\backend\forms\DiscountForm;
use exoo\shop\services\manage\DiscountManageService;
use Yii;
use exoo\shop\entities\Discount;
use exoo\shop\models\backend\search\DiscountSearch;
use exoo\status\StatusAction;
use exoo\position\PositionAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;

class DiscountController extends Controller
{
    private $service;

    public function __construct($id, $module, DiscountManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'position' => [
                'class' => PositionAction::class,
                'modelClass' => Discount::class,
            ],
            'status' => [
                'class' => StatusAction::class,
                'modelClass' => Discount::class,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $discount = new Discount();
        $form = new DiscountForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $discount = $this->service->create($form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->redirect(['update', 'id' => $discount->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.danger', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'discount' => $discount,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $discount = $this->findModel($id);
        $form = new DiscountForm($discount);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($discount->id, $form);
                Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Saved'));
                return $this->refresh();
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('alert.danger', $e->getMessage());
            }
        }
        return $this->render('form', [
            'model' => $form,
            'discount' => $discount,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.danger', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes multiple an existing models.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionBatchDelete()
    {
        try {
            if (($ids = Yii::$app->request->post('ids')) !== null) {
                foreach ($ids as $id) {
                    $this->service->remove($id);
                }
                return $this->redirect(['index']);
            } else {
                throw new BadRequestHttpException(400);
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('alert.danger', $e->getMessage());
        }
    }

    /**
     * @param integer $id
     * @return Discount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id): Discount
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
