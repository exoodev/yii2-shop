<?php

namespace exoo\shop\controllers\frontend;

use exoo\shop\models\frontend\forms\AddToCartForm;
use exoo\shop\models\frontend\forms\ReviewForm;
use exoo\shop\models\frontend\forms\Search\SearchForm;
use exoo\shop\models\frontend\views\ProductView;
use exoo\shop\models\frontend\search\ReviewSearch;
use exoo\shop\repositories\read\BrandReadRepository;
use exoo\shop\repositories\read\CategoryReadRepository;
use exoo\shop\repositories\read\ProductReadRepository;
use exoo\shop\repositories\read\TagReadRepository;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller
{
    public $layout = 'catalog';

    private $products;
    private $categories;
    private $brands;
    private $tags;

    public function __construct(
        $id,
        $module,
        ProductReadRepository $products,
        CategoryReadRepository $categories,
        BrandReadRepository $brands,
        TagReadRepository $tags,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->products = $products;
        $this->categories = $categories;
        $this->brands = $brands;
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->products->getAll();
        $category = $this->categories->getRoot();

        return $this->render('index', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $slug
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCategory($id)
    {
        if (!$category = $this->categories->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        
        

        // if ($mode) {
        //     Yii::$app->response->cookies->add(new \yii\web\Cookie([
        //         'name' => 'view-mode',
        //         'value' => $mode,
        //     ]));
        // }
        $dataProvider = $this->products->getAllByCategory($category);

        return $this->render('category', [
            'category' => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionBrand($id)
    {
        if (!$brand = $this->brands->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $dataProvider = $this->products->getAllByBrand($brand);

        return $this->render('brand', [
            'brand' => $brand,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionTag($id)
    {
        if (!$tag = $this->tags->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $dataProvider = $this->products->getAllByTag($tag);

        return $this->render('tag', [
            'tag' => $tag,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionSearch()
    {
        $form = new SearchForm();
        $form->load(Yii::$app->request->queryParams);
        $form->validate();

        $dataProvider = $this->products->search($form);

        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'searchForm' => $form,
        ]);
    }

    /**
     * @param $slug
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionProduct($slug, $mId = null)
    {
        if (!$product = $this->products->findBySlug($slug)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $searchModel = new ReviewSearch();
        $reviewDataProvider = $searchModel->search($product->id, Yii::$app->request->queryParams);

        $productView = new ProductView($product, $mId);
        $cartForm = new AddToCartForm($product, $mId);
        $reviewForm = new ReviewForm();

        return $this->render('product', [
            'product' => $product,
            'cartForm' => $cartForm,
            'reviewForm' => $reviewForm,
            'productView' => $productView,
            'reviewDataProvider' => $reviewDataProvider,
        ]);
    }

    public function actionModeView()
    {
        if ($mode = Yii::$app->request->post('mode')) {
            try {
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => 'grid',
                    'value' => $mode,
                ]));
                return Yii::$app->response;
            } catch (\DomainException $e) {
                throw new BadRequestHttpException($e->getMessage(), null, $e);
            }
        }
    }
}
