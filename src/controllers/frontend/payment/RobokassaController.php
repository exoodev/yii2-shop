<?php

namespace exoo\shop\controllers\frontend\payment;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use exoo\shop\entities\Order\Order;
use exoo\shop\repositories\read\OrderReadRepository;
use exoo\shop\services\OrderService;
use robokassa\Merchant;
use robokassa\ResultAction;
use robokassa\SuccessAction;
use robokassa\FailAction;

class RobokassaController extends Controller
{
    public $enableCsrfValidation = false;

    private $orders;
    private $service;

    public function __construct($id, $module, OrderReadRepository $orders, OrderService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->orders = $orders;
        $this->service = $service;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'result' => ['post'],
                    'success' => ['post'],
                    'fail' => ['post'],
                ],
            ]
        ];
    }

    public function actionInvoice($id)
    {
        $order = $this->loadModel($id);
        return $this->getMerchant()->payment($order->cost, $order->id, 'Payment', null, Yii::$app->user->identity->email);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'result' => [
                'class' => ResultAction::class,
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => SuccessAction::class,
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => FailAction::class,
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }

    public function successCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        Yii::$app->session->setFlash('notify.success', Yii::t('shop', 'Order successfully paid'));
        return $this->redirect(['/shop/profile/order/view', 'id' => $nInvId]);
    }

    public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $order = $this->loadModel($nInvId);
        try {
            $this->service->pay($order->id, 'robokassa');
            return 'OK' . $nInvId;
        } catch (\DomainException $e) {
            return $e->getMessage();
        }
    }

    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $order = $this->loadModel($nInvId);
        try {
            $this->service->failPay($order->id, 'robokassa');
            return 'OK';
        } catch (\DomainException $e) {
            return $e->getMessage();
        }
    }

    private function loadModel($id): Order
    {
        if (!$order = $this->orders->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $order;
    }

    private function getMerchant(): Merchant
    {
        return Yii::$app->get('robokassa');
    }
}
