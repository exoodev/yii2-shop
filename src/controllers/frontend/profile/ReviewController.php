<?php

namespace exoo\shop\controllers\frontend\profile;

use exoo\shop\repositories\read\ProductReadRepository;
use exoo\shop\services\ReviewService;
use exoo\shop\models\frontend\forms\ReviewForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ReviewController extends Controller
{
    public $layout = 'profile';
    private $service;
    private $products;

    public function __construct($id, $module, ReviewService $service, ProductReadRepository $products, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->products = $products;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'add' => ['post'],
                    'update' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionAdd($id)
    {
        try {
            $form = new ReviewForm();
            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                $this->service->add(Yii::$app->user->id, $id, $form);
                Yii::$app->session->setFlash('alert.success', Yii::t('shop', 'Review successfully added and is waiting for moderation'));
            }
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * @param $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     try {
    //         $this->service->update(Yii::$app->user->id, $id);
    //         Yii::$app->session->setFlash('success', 'Success!');
    //     } catch (\DomainException $e) {
    //         Yii::$app->errorHandler->logException($e);
    //         Yii::$app->session->setFlash('error', $e->getMessage());
    //     }
    //     return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    // }

    /**
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->service->remove(Yii::$app->user->id, $id);
            Yii::$app->session->setFlash('alert.success', Yii::t('shop', 'Review deleted'));
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->goBack();
    }
}
