<?php

$directories = [
    [
        'label' => Yii::t('shop', 'Brands'),
        'url' => ['/shop/brand/index'],
        'routeActive' => 'shop/brand',
    ],
    // [
    //     'label' => Yii::t('shop', 'Currency'),
    //     'url' => ['/shop/currency/index'],
    //     'routeActive' => 'shop/currency',
    // ],
    [
        'label' => Yii::t('shop', 'Discounts'),
        'url' => ['/shop/discount/index'],
        'routeActive' => 'shop/discount',
    ],
    [
        'label' => Yii::t('shop', 'Characteristics'),
        'url' => ['/shop/characteristic/index'],
        'routeActive' => 'shop/characteristic',
    ],
    [
        'label' => Yii::t('shop', 'Characteristics groups'),
        'url' => ['/shop/characteristic-group/index'],
        'routeActive' => 'shop/characteristic-group',
    ],
    [
        'label' => Yii::t('shop', 'Units'),
        'url' => ['/shop/unit/index'],
        'routeActive' => 'shop/unit',
    ],
    [
        'label' => Yii::t('shop', 'Tags'),
        'url' => ['/shop/tag/index'],
        'routeActive' => 'shop/tag',
    ],
    [
        'label' => Yii::t('shop', 'Suppliers'),
        'url' => ['/shop/supplier/index'],
        'routeActive' => 'shop/supplier',
    ],
    [
        'label' => Yii::t('shop', 'Warehouses'),
        'url' => ['/shop/warehouse/index'],
        'routeActive' => 'shop/warehouse',
    ],
    [
        'label' => Yii::t('shop', 'Delivery methods'),
        'url' => ['/shop/delivery/index'],
        'routeActive' => 'shop/delivery',
    ],
    [
        'label' => Yii::t('shop', 'Payment methods'),
        'url' => ['/shop/payment/index'],
        'routeActive' => 'shop/payment',
    ],
];

return [
    'navside' => [
        'icon' => '<i class="fas fa-shopping-basket fa-lg fa-fw uk-margin-small-right"></i>',
        'label' => Yii::t('shop', 'Shop'),
        'items' => [
            [
                'label' => Yii::t('shop', 'Products'),
                'url' => ['/shop/product/index'],
                'routeActive' => ['shop/product', 'shop/modification'],
            ],
            [
                'label' => Yii::t('shop', 'Categories'),
                'url' => ['/shop/category/index'],
                'routeActive' => 'shop/category',
            ],
            [
                'label' => Yii::t('shop', 'Groups'),
                'url' => ['/shop/group/index'],
                'routeActive' => 'shop/group',
            ],
            [
                'label' => Yii::t('shop', 'Orders'),
                'url' => ['/shop/order/index'],
                'routeActive' => ['shop/order'],
            ],
            [
                'label' => Yii::t('shop', 'Reviews'),
                'url' => ['/shop/review/index'],
                'routeActive' => ['shop/review'],
            ],
            [
                'label' => Yii::t('shop', 'Directories'),
                'url' => ['/shop/default/directories'],
            ],
            [
                'label' => Yii::t('shop', 'Settings'),
                'url' => ['/shop/default/settings'],
                'routeActive' => ['/shop/default/settings'],
            ],
        ]
    ],
    'subnav' => [
        [
            'label' => Yii::t('shop', 'Products'),
            'url' => ['/shop/product/index'],
            'routeActive' => ['shop/product', 'shop/modification'],
        ],
        [
            'label' => Yii::t('shop', 'Categories'),
            'url' => ['/shop/category/index'],
            'routeActive' => 'shop/category',
        ],
        [
            'label' => Yii::t('shop', 'Groups'),
            'url' => ['/shop/group/index'],
            'routeActive' => 'shop/group',
        ],
        [
            'label' => Yii::t('shop', 'Orders'),
            'url' => ['/shop/order/index'],
            'routeActive' => ['shop/order'],
        ],
        [
            'label' => Yii::t('shop', 'Reviews'),
            'url' => ['/shop/review/index'],
            'routeActive' => ['shop/review'],
        ],
        [
            'label' => Yii::t('shop', 'Directories'),
            'url' => ['/shop/default/directories'],
            'items' => $directories,
        ],
        [
            'label' => Yii::t('shop', 'Settings'),
            'url' => ['/shop/default/settings'],
            'routeActive' => 'shop/default',
        ],
    ],
    'directories' => $directories
];
