<?php
return [
    'components' => [
        'robokassa' => [
            'class' => '\robokassa\Merchant',
            'baseUrl' => 'https://auth.robokassa.ru/Merchant/Index.aspx',
            'sMerchantLogin' => Yii::$app->params['robokassa.sMerchantLogin'],
            'sMerchantPass1' => Yii::$app->params['robokassa.sMerchantPass1'],
            'sMerchantPass2' => Yii::$app->params['robokassa.sMerchantPass2'],
            'isTest' => !YII_ENV_PROD,
        ],
    ],
    'buckets' => [
        'categoryImages' => [
            'baseSubPath' => 'shop/category/images',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
        'productImages' => [
            'baseSubPath' => 'shop/product/images',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
        'productFiles' => [
            'baseSubPath' => 'shop/product/files',
            'fileSubDirTemplate' => '{^name}/{^^name}/{^^^name}',
        ],
    ],
    'params' => [
        'category' => [
            'columns' => ['xl' => 4, 'l' => 3, 'm' => 2],
            'images' => [
                'transform' => [
                    'thumbnail' => [
                        'large' => [
                            'width' => 600,
                            'height' => 400,
                        ],
                        'medium' => [
                            'width' => 300,
                            'height' => 200,
                        ],
                        'small' => [
                            'width' => 70,
                            'height' => 70,
                        ],
                    ],
                ],
                'slideshowOptions' => [
                    'ratio' => '300:200',
                ],
                'widgetOptions' => [
                    'mime' => 'image/*',
                    'extensions' => false,
                    'cropperOptions' => [
                        'aspectRatio' => 300/200,
                    ],
                ],
            ],
        ],
        'product' => [
            'columns' => ['xl' => 4, 'l' => 3, 'm' => 2],
            'images' => [
                'transform' => [
                    'thumbnail' => [
                        'large' => [
                            'width' => 1000,
                            'height' => 1000,
                        ],
                        'medium' => [
                            'width' => 400,
                            'height' => 400,
                        ],
                        'small' => [
                            'width' => 70,
                            'height' => 70,
                        ],
                    ],
                ],
                'slideshowOptions' => [
                    'ratio' => '200:200',
                ],
                'widgetOptions' => [
                    'mime' => 'image/*',
                    'extensions' => false,
                    'cropperOptions' => [
                        'aspectRatio' => 200/200,
                        'viewMode' => 0,
                    ],
                ],
            ],
            'files' => [
                'widgetOptions' => [
                    'mime' => false,
                    'extensions' => '*.(zip|rar|txt|xls|doc|xlsx|docx|pdf)',
                ],
            ],
        ],
    ],
    'purifierOptions' => [
        'Attr.AllowedRel' => array('nofollow'),
        'HTML.SafeObject' => true,
        'Output.FlashCompat' => true,
        'HTML.SafeIframe' => true,
        'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
    ]
];
