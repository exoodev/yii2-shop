<?php
return [
    'user' => [
        [
            'label' => Yii::t('shop', 'Orders'),
            'url' => ['/shop/profile/order/index'],
            'routeActive' => ['shop/profile/order'],
        ],
    ],
];
