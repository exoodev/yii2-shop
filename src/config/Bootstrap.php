<?php

namespace exoo\shop\config;

use exoo\shop\components\cart\Cart;
use exoo\shop\components\cart\cost\calculator\DynamicCost;
use exoo\shop\components\cart\cost\calculator\SimpleCost;
use exoo\shop\components\cart\storage\HybridStorage;
use exoo\shop\components\dispatchers\AsyncEventDispatcher;
use exoo\shop\components\dispatchers\DeferredEventDispatcher;
use exoo\shop\components\dispatchers\EventDispatcher;
use exoo\shop\components\dispatchers\SimpleEventDispatcher;
use exoo\shop\entities\Product\events\ProductAppearedInStock;
use exoo\shop\components\jobs\AsyncEventJobHandler;
use exoo\shop\components\listeners\Category\CategoryPersistenceListener;
use exoo\shop\components\listeners\Product\ProductAppearedInStockListener;
use exoo\shop\components\listeners\Product\ProductSearchPersistListener;
use exoo\shop\components\listeners\Product\ProductSearchRemoveListener;
use exoo\shop\repositories\events\EntityPersisted;
use exoo\shop\repositories\events\EntityRemoved;
use exoo\shop\services\newsletter\MailChimp;
use exoo\shop\services\newsletter\Newsletter;
use exoo\shop\services\sms\LoggedSender;
use exoo\shop\services\sms\SmsRu;
use exoo\shop\services\sms\SmsSender;
use exoo\shop\services\yandex\ShopInfo;
use exoo\shop\services\yandex\YandexMarket;
use yii\base\BootstrapInterface;
use yii\base\ErrorHandler;
use yii\caching\Cache;
use yii\di\Container;
use yii\di\Instance;
use yii\mail\MailerInterface;
use yii\queue\Queue;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Client;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;

        $container->setSingleton(Client::class, function () {
            return ClientBuilder::create()->build();
        });

        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });

        $container->setSingleton(ErrorHandler::class, function () use ($app) {
            return $app->errorHandler;
        });

        $container->setSingleton(Queue::class, function () use ($app) {
            return $app->get('queue');
        });

        $container->setSingleton(Cache::class, function () use ($app) {
            return $app->cache;
        });

        $container->setSingleton(Cart::class, function () use ($app) {
            return new Cart(
                new HybridStorage($app->get('user'), 'cart', 3600 * 24, $app->db),
                new DynamicCost(new SimpleCost())
            );
        });

        // $container->setSingleton(YandexMarket::class, [], [
        //     new ShopInfo($app->name, $app->name, $app->params['frontendHostInfo']),
        // ]);

        $container->setSingleton(Newsletter::class, function () use ($app) {
            return new MailChimp(
                new \DrewM\MailChimp\MailChimp($app->params['mailChimpKey']),
                $app->params['mailChimpListId']
            );
        });

        $container->setSingleton(SmsSender::class, function () use ($app) {
            return new LoggedSender(
                new SmsRu($app->params['smsRuKey']),
                \Yii::getLogger()
            );
        });

        $container->setSingleton(EventDispatcher::class, SimpleEventDispatcher::class);

        $container->setSingleton(DeferredEventDispatcher::class, function (Container $container) {
            return new DeferredEventDispatcher(new AsyncEventDispatcher($container->get(Queue::class)));
        });

        $container->setSingleton(SimpleEventDispatcher::class, function (Container $container) {
            return new SimpleEventDispatcher($container, [
                ProductAppearedInStock::class => [ProductAppearedInStockListener::class],
                EntityPersisted::class => [
                    ProductSearchPersistListener::class,
                    CategoryPersistenceListener::class,
                ],
                EntityRemoved::class => [
                    ProductSearchRemoveListener::class,
                    CategoryPersistenceListener::class,
                ],
            ]);
        });

        $container->setSingleton(AsyncEventJobHandler::class, [], [
            Instance::of(SimpleEventDispatcher::class)
        ]);
    }
}
