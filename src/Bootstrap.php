<?php

namespace exoo\shop;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Shop extension bootstrap class.
 */
class Bootstrap implements BootstrapInterface
{
	/**
     * @inheritdoc
     */
	public function bootstrap($app)
	{
		Yii::setAlias('@shop', dirname(__DIR__) . '/src');
        
		$app->i18n->translations['shop'] = [
            'class' => 'yii\i18n\PhpMessageSource',
			'basePath' => '@exoo/shop/messages',
		];
        $module = $app->getModule('shop');

		if ($app instanceof \yii\console\Application) {
			$app->setModules([
                'shop' => [
		            'class' => 'exoo\shop\Module',
		            'controllerNamespace' => 'exoo\shop\controllers\console'
		        ],
            ]);
        } else {
			// if (!$module->isBackend) {
				// Yii::$app->getUrlManager()->addRules([
				// 	// '<_m:shop>/nom/modification/<nom_id:[a-zA-Z0-9_-]{1,100}+>' => '<_m>/modification/index',
				// ]);
            // }

            Yii::createObject('exoo\shop\config\Bootstrap')->bootstrap($app);
            
            // if ($appName = $module->getAppName()) {
            //     $component = Yii::createObject('exoo\shop\config\\' . $appName . '\Bootstrap');
            //     $component->bootstrap($app);
            // }

            $app->setComponents([
                'user' => array_merge($app->components['user'], [
                    'identityClass' => 'exoo\shop\entities\User',
                ]),
                'queue' => [
                    'class' => 'yii\queue\redis\Queue',
                    'as log' => 'yii\queue\LogBehavior',
                ],
            ]);
        }
	}
}
