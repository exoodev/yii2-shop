<?php

namespace exoo\shop;

use Yii;
use yii\di\Instance;
use yii\helpers\ArrayHelper;

/**
 * Module class.
 */
class Module extends \yii\base\Module
{
    /**
     * @var string the name extension
     */
    public $name;
    /**
     * @var string name of the file storage application component.
     */
    public $fileStorage = 'fileStorage';
    /**
     * @var string|BucketInterface name of the file storage bucket, which stores the related files or
     * bucket instance itself.
     * If empty, bucket name will be generated automatically using owner class name and [[fileAttribute]].
     */
    public $fileStorageBucket;
    /**
     * @var array the bucketData params for images category
     */
    public $categoryImageBucketData;
    /**
     * @var array the transform params for images category
     */
    public $categoryImageTransform = [];
    /**
     * @var array the bucketData params for images nomenclature
     */
    public $nomImageBucketData;
    /**
     * @var array the transform params for images nomenclature
     */
    public $nomImageTransform = [];
    /**
     * @var array the bucketData params for files nomenclature
     */
    public $nomFileBucketData;
    /**
     * @var string the nomenclature params
     */
    public $nomenclature;
    /**
     * @var string Example: @app/config/shop.php
     */
    public $configPath;
    /**
     * @var array the file buckets
     */
    public $buckets;
    /**
     * @var string the configuration of clean up HTML from any harmful code
     */
    public $purifierOptions;
    /**
     * @var boolean
     */
    public $isBackend = false;
    /**
     * @var string the app name
     */
    private $_appName;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->name = Yii::t('shop', 'Shop');
        $this->configureModule();
        $this->setBuckets();

        $this->modules = [
            'api' => [
                'class' => 'exoo\shop\modules\api\Module',
                'basePath' => '@exoo/shop/modules/api',
                'controllerNamespace' => 'exoo\shop\modules\api\controllers'
            ]
        ];

        if ($this->isBackend === true) {
            $this->_appName = 'backend';
            $this->layout = 'main.php';
        } else {
            $this->_appName = 'frontend';
        }

        $this->setViewPath('@exoo/shop/views/' . $this->_appName);
        if ($this->controllerNamespace === null) {
            $this->controllerNamespace = 'exoo\shop\controllers\\' . $this->_appName;
        }

        parent::init();
    }

    public function configureModule()
    {
        $config = require __DIR__ . '/config/main.php';

        if ($this->configPath !== null) {
            $clientConfig = Yii::getAlias($this->configPath);

            if (file_exists($clientConfig)) {
                $config = ArrayHelper::merge($config, require $clientConfig);
            }
        }

        Yii::configure($this, $config);

        if ($this->components) {
            Yii::$app->setComponents($this->components);
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    public function setBuckets()
    {
        if (!$this->buckets) {
            return;
        }
        if (!is_object($this->fileStorageBucket)) {
            $this->fileStorageBucket = Instance::ensure($this->fileStorage, 'exoo\storage\FileStorage');
        }
        $this->fileStorageBucket->setBuckets($this->buckets);
    }

    /**
     * @param string $name
     * @return array
     */
    public function getMenu($name)
    {
        $menu = include Yii::getAlias('@shop') . '/config/' . $this->_appName . '/menu.php';
        return ArrayHelper::getValue($menu, $name);
    }

    public function getAppName()
    {
        return $this->_appName;
    }
}
