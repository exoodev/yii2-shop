<?php

namespace exoo\shop\helpers;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class ImageHelper
{
    public static function default($options = []): string
    {
        if (!isset($options['class'])) {
            $options['class'] = 'uk-flex uk-flex-middle uk-flex-center';
        }
        $options['style'] = [
            'border' => '1px dashed #a7a7a7',
            'min-height' => '40px',
            'min-width' => '40px',
        ];

        $icon = ArrayHelper::remove($options, 'icon', '<i class="uk-text-muted" uk-icon="camera"></i>');
        return Html::tag('div', $icon, $options);
    }
}
