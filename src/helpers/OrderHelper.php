<?php

namespace exoo\shop\helpers;

use Yii;
use exoo\shop\entities\Order\Status;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class OrderHelper
{
    public static function statusList(): array
    {
        return [
            Status::NEW => Yii::t('shop', 'New'),
            Status::PAID => Yii::t('shop', 'Paid'),
            Status::PAID_FAIL => Yii::t('shop', 'Failed Payment'),
            Status::SENT => Yii::t('shop', 'Sent'),
            Status::COMPLETED => Yii::t('shop', 'Completed'),
            Status::CANCELLED => Yii::t('shop', 'Cancelled'),
            Status::CANCELLED_BY_CUSTOMER => Yii::t('shop', 'Cancelled by customer'),
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case Status::PAID:
                $class = 'uk-label uk-label-success';
                break;
            case Status::SENT:
                $class = 'uk-label uk-label-warning';
                break;
            case Status::COMPLETED:
                $class = 'uk-label ex-text-green-600 ex-background-green-100';
                break;
            case Status::PAID_FAIL:
                $class = 'uk-label uk-label-danger';
                break;
            case Status::CANCELLED:
            case Status::CANCELLED_BY_CUSTOMER:
                $class = 'uk-label ex-text-grey-600 ex-background-grey-100';
                break;
            default:
                $class = 'uk-label';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}
