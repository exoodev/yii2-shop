<?php

namespace exoo\shop\helpers;

class ShopHelper
{
    public static function getGridClasses($columns): string
    {
        if (!$columns) {
            $columns = ['xl' => 4, 'l' => 3, 'm' => 2];
        }

        $result = '';

        foreach ($columns as $size => $column) {
            if (in_array($size, ['m','l','xl']) && in_array($column, range(1, 6))) {
                $result .= 'uk-child-width-1-' . $column . '@' . $size . ' ';
            }
        }
        
        return $result;
    }
}