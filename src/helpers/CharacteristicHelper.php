<?php

namespace exoo\shop\helpers;

use Yii;
use exoo\shop\entities\Characteristic\Characteristic;
use yii\helpers\ArrayHelper;

class CharacteristicHelper
{
    public static function typeList(): array
    {
        return [
            Characteristic::TYPE_STRING => Yii::t('shop', 'String'),
            Characteristic::TYPE_INTEGER => Yii::t('shop', 'Integer'),
            Characteristic::TYPE_FLOAT => Yii::t('shop', 'Non integer'),
        ];
    }

    public static function typeName($type): string
    {
        return ArrayHelper::getValue(self::typeList(), $type);
    }
}
