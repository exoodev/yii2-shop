<?php

namespace exoo\shop\helpers;

use Yii;
use exoo\shop\models\backend\Settings;

/**
 * PriceHelper class.
 */
class PriceHelper
{
    public static function format($price, $currnecy = true)
    {
        $value = number_format(
            $price,
            Yii::$app->settings->get('shop', 'maxFractionDigits'),
            Settings::getNumberSeparator('decimalSeparator'),
            Settings::getNumberSeparator('thousandSeparator')
        );

        if ($currnecy) {
            $value .= ' ' . self::currency();
        }

        return $value;
    }

    public static function currency()
    {
        $formatter = new \NumberFormatter(
            Yii::$app->language . '@currency=' . Yii::$app->settings->get('shop', 'currency'),
            \NumberFormatter::CURRENCY
        );
        return $formatter->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }

    public static function percentDifference($newprice, $oldprice)
    {
        $number = round((1 - $newprice / $oldprice) * 100);

        if ($number > 0) {
            return '-' . $number . '%';
        }
    }
}
