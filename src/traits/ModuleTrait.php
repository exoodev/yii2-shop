<?php

namespace exoo\shop\traits;

use Yii;
use exoo\shop\Module;

/**
 * Module trait
 */
trait ModuleTrait
{
    /**
     * @var Module|null Module instance
     */
    private $_module;

    /**
     * @return Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $module = Module::getInstance();

            if ($module instanceof Module) {
                $this->_module = $module;
            } else {
                $this->_module = Yii::$app->getModule('shop');
            }
        }

        return $this->_module;
    }
}