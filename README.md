# Shop

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist exoodev/yii2-shop
```

or add

```json
"exoodev/yii2-shop": "*"
```

to the require section of your composer.json.



Usage
-----

Example backend configuration:

```php
return [
    'modules' => [
        'shop' => [
            'class' => 'exoo\shop\Module',
            'isBackend' => true,
        ],
    ],
    'components' => [
        'menu' => [
            'class' => 'exoo\system\components\Menu',
            'items' => [
                'shop' => '@exoo/shop/config/backend/menu',
            ],
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
    ]
];
```

Example frontend configuration:

```php
return [
    'modules' => [
        'shop' => [
            'class' => 'exoo\shop\Module',
        ]
    ],
    'components' => [
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
    ]
];
```

## Migration

```
yii migrate --migrationPath=@exoo/shop/migrations
```
